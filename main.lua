CommRedux = RegisterMod("Community Redux", 1)
CommRedux.GAME = Game()
CommRedux.SFX = SFXManager()
CommRedux.RNG = RNG()

local thisversion = {0, 2, 1}

CommRedux.flavortext_array = {
	"Also try Fiend Folio!",
	"fartin on my roommates door",
	"holeh moleh",
	"hope you brought a couple of diapers",
	"whats the deal with airplane food",
	"do you kiss your mom with that mouth?",
	"Also try my butt!",
	"dogs cant look up"
}

local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG
local didflavor = false
CommRedux.sd = {}

local json = include("json")

CommRedux:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, p)
	local TotPlayers = #Isaac.FindByType(EntityType.ENTITY_PLAYER) -- Change to Isaac.CountEntities() when they fix it (without the #).
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	if TotPlayers == 0 then
		modrng:SetSeed(game:GetSeeds():GetStartSeed(), 35)
		--print("player seed " .. playerseed, "| game seed " .. game:GetSeeds():GetStartSeed())
		if CommRedux:HasData() then
			CommRedux.sd = json.decode(Isaac.LoadModData(CommRedux))
			CommRedux:SaveData(json.encode(CommRedux.sd))
			
			CBLib.RunCallback(CommRedux.ModCallbacks.MC_RUN_CONTINUE, CommRedux, p)
		end
		
		if game:GetFrameCount() == 0 then
			CommRedux.sd.items = {}
			CommRedux.sd.trinkets = {}
			CommRedux.sd.roomdata = {}
			
			CBLib.RunCallback(CommRedux.ModCallbacks.MC_RUN_START, CommRedux, p)
			
			if not didflavor then
				local currflavortext = CommRedux.flavortext_array[CommRedux.rand(1, #CommRedux.flavortext_array, modrng)]
				print(currflavortext)
				didflavor = true
			end
			
			--move to item files
			CommRedux.sd.applemode = {}
			CommRedux.sd.occamboost = {}
			CommRedux.sd.maxpawboost = {}
			CommRedux.sd.mysterymeat = {}
			CommRedux.sd.cherubcount = 0
			
			CommRedux.sd.initplayers = {}
			
			CommRedux:SaveData(json.encode(CommRedux.sd))
		end
	end
end)


function CommRedux.Save(died)
	CommRedux:SaveData(json.encode(CommRedux.sd))
end
CommRedux:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, CommRedux.Save)


CommRedux.sd.roomdata = CommRedux.sd.roomdata ~= nil and CommRedux.sd.roomdata or {} 
CommRedux.sd.cursedroomidx = CommRedux.sd.cursedroomidx ~= nil and CommRedux.sd.cursedroomidx or {} 

CommRedux.sd.pillcase = CommRedux.sd.pillcase ~= nil and CommRedux.sd.pillcase or {} 
CommRedux.sd.deckbuilder = CommRedux.sd.deckbuilder ~= nil and CommRedux.sd.deckbuilder or {} 

CommRedux.sd.items = CommRedux.sd.items ~= nil and CommRedux.sd.items or {} 
CommRedux.sd.trinkets = CommRedux.sd.trinkets ~= nil and CommRedux.sd.trinkets or {} 

CommRedux.sd.initplayers = CommRedux.sd.initplayers ~= nil and CommRedux.sd.initplayers or {} 

CommRedux.sd.maxpawboost = CommRedux.sd.maxpawboost ~= nil and CommRedux.sd.maxpawboost or {} 
CommRedux.sd.mysterymeat = CommRedux.sd.mysterymeat ~= nil and CommRedux.sd.mysterymeat or {} 
CommRedux.sd.applemode = CommRedux.sd.applemode ~= nil and CommRedux.sd.applemode or {} 
CommRedux.sd.occamboost = CommRedux.sd.occamboost ~= nil and CommRedux.sd.occamboost or {} 
CommRedux.sd.appleslot = CommRedux.sd.appleslot ~= nil and CommRedux.sd.appleslot or {}

CommRedux.sd.showstreak = CommRedux.sd.showstreak ~= nil and CommRedux.sd.showstreak or 2 
CommRedux.sd.showdps = CommRedux.sd.showdps ~= nil and CommRedux.sd.showdps or 1 
CommRedux.sd.losttarotannouncer = CommRedux.sd.losttarotannouncer ~= nil and CommRedux.sd.losttarotannouncer or 1 
CommRedux.sd.lambannouncer = CommRedux.sd.lambannouncer ~= nil and CommRedux.sd.lambannouncer or 1
CommRedux.sd.screenshake = CommRedux.sd.screenshake ~= nil and CommRedux.sd.screenshake or 2
CommRedux.sd.tearsfx = CommRedux.sd.tearsfx ~= nil and CommRedux.sd.tearsfx or 1

CommRedux.sd.unlocks = CommRedux.sd.unlocks ~= nil and CommRedux.sd.unlocks or {}

local scrpath = "resources/scripts/"

local req_scr = {
	"cblib",
	"excb",
	"commredux",
	"mini_achievements",
	"dss"
}

local function scr_inc(name)
	include(scrpath .. name .. "/main")
end

for i = 1, #req_scr do
	scr_inc(req_scr[i])
end

local levelTime = 0

-- track spawns 
local entitySpawnData = {}
CommRedux:AddCallback(ModCallbacks.MC_PRE_ENTITY_SPAWN, function(_, _Type, variant, subType, position, velocity, spawner, seed)
	entitySpawnData[seed] = {
		Type = _type,
		Variant = variant,
		SubType = subType,
		Position = position,
		Velocity = velocity,
		SpawnerEntity = spawner,
		InitSeed = seed
	}
end)

CBLib.AddToCallback(ExCB.ModCallbacks.MC_POST_ENTITY_INIT, function(entity)
	local seed = entity.InitSeed
	local data = entity:GetData()
	data.SpawnData = entitySpawnData[seed]
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, save)
	if save and CommRedux.sd.cherubcount and CommRedux.sd.cherubcount > 0 then
		for i = 1, CommRedux.sd.cherubcount do
			local cherub = Isaac.Spawn(EntityType.ENTITY_CHERUB, ENTITYVARIANT_CHERUB, 0, game:GetRoom():GetCenterPos(), Vector.Zero, nil)
			CommRedux.sd.cherubcount = CommRedux.sd.cherubcount - 1
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_ENTITY_REMOVE, function(_, entity)
	local data = entity:GetData()
	data.SpawnData = nil
end)

CommRedux:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_)
	if CommRedux.sd.tearsfx == 2 then
		if sfx:IsPlaying(SoundEffect.SOUND_SPLATTER) then
			sfx:Stop(SoundEffect.SOUND_SPLATTER)
			sfx:Play(SoundEffect.SOUND_CR_TEARSPLAT, 1, 0, false, 1, 0)
		end
		if sfx:IsPlaying(SoundEffect.SOUND_TEARS_FIRE) then
			sfx:Stop(SoundEffect.SOUND_TEARS_FIRE)
			sfx:Play(SoundEffect.SOUND_CR_TEARFIRE, 1, 0, false, 1, 0)
		end
		if sfx:IsPlaying(SoundEffect.SOUND_TEARIMPACTS) then
			sfx:Stop(SoundEffect.SOUND_TEARIMPACTS)
			sfx:Play(SoundEffect.SOUND_CR_TEARBLOCK, 1, 0, false, 1, 0)
		end
	end
	
	if CommRedux.sd.screenshake == 1 then
		if game:GetScreenShakeCountdown() > 0 then
			game:ShakeScreen(0)
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_ENTITY_KILL, function(_, e)
	if e.Type == 1 then
		local p = e:ToPlayer()
		local d = p:GetData()
		
		if d.CommRedux.IsImmortal then
			p:Revive()
			if p:GetMaxHearts() == 0 then
				p:AddSoulHearts(-1)
			else
				p:AddHearts(-1)
			end
			if sfx:IsPlaying(30) then
				sfx:Stop(30)
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function(self)
	CommRedux.sd.roomdata = {}
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_UPDATE, function(_)
	levelTime = levelTime + 1
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_ITEM_GET, function(Type, id, p)
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	local itemcfg = Isaac.GetItemConfig()
	
	for i, v in pairs(TRANSFORMATION_METADATA) do
		local t = {}
		t[i] = 0
		for i2, v2 in pairs(v) do
			if v2[2] == "collectible" then
				if CommRedux.checkItem(p, v2[1]) then
					t[i] = t[i] + 1
				end
			elseif v2[2] == "trinket" then
				
				if CommRedux.checkTrinket(p, v2[1]) then
					t[i] = t[i] + 1
				end
			end
		end
		if (not p:HasCollectible(i)) and ( t[i] >= 3 or (t[i] == #v and #v > 0 )) then
			local hud = game:GetHUD()
			local transcfg = itemcfg:GetCollectible(i)
			hud:ShowItemText(transcfg.Name, "", false)
			p:AddCollectible(i, 0, true, 0, 0)
			Isaac.Spawn(1000, 15, 0, p.Position, Vector.Zero, p)
			sfx:Play(SoundEffect.SOUND_POWERUP_SPEWER, 1, 0, false, 1, 0)
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, p)
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	local d = p:GetData()
	local level = game:GetLevel()
	local itemcfg = Isaac.GetItemConfig()
	local pfx = p:GetEffects()
	
	if d.CommRedux == nil then d.CommRedux = {} end
	
	d.CommRedux.MaxFireDelay = p.MaxFireDelay
	
	if not CommRedux.sd.initplayers[ps] then
		CBLib.RunCallback(CommRedux.ModCallbacks.MC_PLAYER_FIRST_INIT, CommRedux, p)
		CommRedux.sd.initplayers[ps] = true
	end
	
	if CommRedux.sd.items == nil then CommRedux.sd.items = {} end
	if CommRedux.sd.trinkets == nil then CommRedux.sd.trinkets = {} end
	if CommRedux.sd.items[ps] == nil then CommRedux.sd.items[ps] = {} end
	if CommRedux.sd.trinkets[ps] == nil then CommRedux.sd.trinkets[ps] = {} end
	
	if not p:IsCoopGhost() then
		for i, v in pairs(CommRedux.ItemTracker) do
			for i2, v2 in pairs(v) do
				if i == 1 then
					if p:HasCollectible(i2) and not CommRedux.checkItem(p, i2) then
						CommRedux.sd.items[ps][#CommRedux.sd.items[ps] + 1] = i2
						CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_ITEM_GET, CommRedux, "collectible", i2, p)
					end
				elseif i == 2 then
					if p:HasTrinket(i2) and not CommRedux.checkTrinket(p, i2) then
						CommRedux.sd.trinkets[ps][#CommRedux.sd.trinkets[ps] + 1] = i2
						CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_ITEM_GET, CommRedux, "trinket", i2, p)
					end
				end
			end
		end
	end
	
	if not d.CommRedux.Init then
		CBLib.RunCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, CommRedux, p)
		d.CommRedux.Init = true
	end
	
	CBLib.RunCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, CommRedux, p)
	
	
	
	
	
	
	
	
	
	
	if d.dmgcountdown and d.dmgcountdown > 0 then
		d.dmgcountdown = d.dmgcountdown - 1
	end
	
	if d.CommRedux.redappleboost and d.CommRedux.redappleboost > 1.5 then
		d.CommRedux.redappleboost = 1.5
		p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
		p:EvaluateItems()
	end
	
	if d.CommRedux.whiteappleboost and d.CommRedux.whiteappleboost > 1.5 then
		d.CommRedux.whiteappleboost = 1.5
		p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
		p:EvaluateItems()
	end
	
	--[[
	local hasapple = (
		p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED) or
		p:HasCollectible(AppleFrames.L) or
		p:HasCollectible(AppleFrames.ML) or
		p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE) or
		p:HasCollectible(AppleFrames.M) or
		p:HasCollectible(AppleFrames.MR) or
		p:HasCollectible(AppleFrames.R) or
		p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE)
	)
	
	local hasappleframes = (
		p:HasCollectible(AppleFrames.L) or
		p:HasCollectible(AppleFrames.ML) or
		p:HasCollectible(AppleFrames.M) or
		p:HasCollectible(AppleFrames.MR) or
		p:HasCollectible(AppleFrames.R)
	)]]
	
	if d.CommRedux.tempstatcountdown == nil then d.CommRedux.tempstatcountdown = 15 end
	if d.CommRedux.redappleboost == nil then d.CommRedux.redappleboost = 0 end
	if d.CommRedux.whiteappleboost == nil then d.CommRedux.whiteappleboost = 0 end
	if d.CommRedux.tempstatcountdown <= 0 then
		if d.CommRedux.redappleboost and d.CommRedux.redappleboost > 0 then
			d.CommRedux.redappleboost = d.CommRedux.redappleboost - 0.1
			if d.CommRedux.redappleboost < 0 then d.CommRedux.redappleboost = 0 end
			p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
			p:EvaluateItems()
		end
		if d.CommRedux.whiteappleboost and d.CommRedux.whiteappleboost > 0 then
			d.CommRedux.whiteappleboost = d.CommRedux.whiteappleboost - 0.1
			if d.CommRedux.whiteappleboost < 0 then d.CommRedux.whiteappleboost = 0 end
			p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
			p:EvaluateItems()
		end
		d.CommRedux.tempstatcountdown = 15
	end
	
	if d.CommRedux.tempstatcountdown > 0 then
		d.CommRedux.tempstatcountdown = d.CommRedux.tempstatcountdown - 1
	end
end)


function CommRedux.NewRoom()
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		if d.CommRedux == nil then d.CommRedux = {} end
		if d.CommRedux.shampooboost then
			d.CommRedux.shampooboost = nil
			p:AddCacheFlags(CacheFlag.CACHE_ALL)
			p:EvaluateItems()
		end
	end
	
	if CommRedux.sd.unlocks and not CommRedux.sd.unlocks.insanependant then
		game:GetItemPool():RemoveTrinket(TrinketType.TRINKET_CR_INSANEPENDANT)
	end
	if CommRedux.sd.unlocks and not CommRedux.sd.unlocks.occamsrazor then
		game:GetItemPool():RemoveCollectible(CollectibleType.COLLECTIBLE_CR_OCCAMSRAZOR)
	end
end
CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, CommRedux.NewRoom)
CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, CommRedux.NewRoom)

CommRedux:AddCallback(ModCallbacks.MC_POST_FIRE_TEAR, function(_, t)
	local p = CommRedux.getPlayerFromTear(t)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	
	CBLib.RunCallback(CommRedux.ModCallbacks.MC_PRE_PLAYER_FIRE_TEAR, CommRedux, p, t)
	
	CBLib.RunCallback(CommRedux.ModCallbacks.MC_PLAYER_FIRE_TEAR, CommRedux, p, t)
	
	if p:HasCollectible(Transformation.CR_LOKI) then
		CommRedux.forceBloodTear(t)
	end
	
	CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_PLAYER_FIRE_TEAR, CommRedux, p, t)
end)

CommRedux:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, p, flag)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	local numcarbattery = p:GetCollectibleNum(356, false)
	
	local tears_up = {}
	tears_up.flat = 0
	tears_up.div = 1
	
	if d.CommRedux == nil then d.CommRedux = {} end
	
	if d.CommRedux.redappleboost == nil then d.CommRedux.redappleboost = 0 end
	if d.CommRedux.whiteappleboost == nil then d.CommRedux.whiteappleboost = 0 end
	
	CBLib.RunCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, CommRedux, p, flag, tears_up)
	
	if flag == CacheFlag.CACHE_DAMAGE then
		p.Damage = p.Damage * (1 + d.CommRedux.redappleboost)
	end
	
	if flag == CacheFlag.CACHE_FIREDELAY then
		tears_up.div = tears_up.div / (1 + d.CommRedux.whiteappleboost)
	end
	
	
	
	
	
	if flag == CacheFlag.CACHE_DAMAGE and p:HasTrinket(TrinketType.TRINKET_CR_INSANEPENDANT) then
		p.Damage = p.Damage * (p:GetTrinketMultiplier(TrinketType.TRINKET_CR_INSANEPENDANT) + 1)
	end
	
	if flag == CacheFlag.CACHE_RANGE and p:HasTrinket(TrinketType.TRINKET_CR_PINWORM) then
		p.TearFallingAcceleration = p.TearFallingAcceleration + ((2 / math.abs((p.TearRange / 40) - 3.25)) * p:GetTrinketMultiplier(TrinketType.TRINKET_CR_PINWORM))
	end
	
	if p:HasCollectible(Transformation.CR_LOKI) then
		if flag == CacheFlag.CACHE_LUCK then
			p.Luck = p.Luck + 6.66
		end
		if flag == CacheFlag.CACHE_FLYING then
			if p.CanFly == false then p.CanFly = true end
		end
	end
	
	if p:HasCollectible(Transformation.CR_TAMMY) then
		if flag == CacheFlag.CACHE_FLYING then
			if p.CanFly == false then p.CanFly = true end
		end
	end
	
	
	
	if flag == CacheFlag.CACHE_FIREDELAY then
		local tmult = math.abs((p.MaxFireDelay * tears_up.div) - p.MaxFireDelay) + tears_up.flat
		while p.MaxFireDelay - tmult < 5 and tmult > 0.001 do
			tmult = tmult - 0.001
		end
		p.MaxFireDelay = p.MaxFireDelay - tmult
	end
	
	CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE, CommRedux, p, flag)
end)


CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
	local player = Isaac.GetPlayer()
	
	--print("min: " .. math.floor(math.floor(levelTime / 30) / 60), "sec: " .. (math.floor(levelTime / 30)) - (60 * math.floor(math.floor(levelTime / 30) / 60)))
	levelTime = 0
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_DEATH, function(_, e)
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_TAMMYSTAIL) then
			d.CommRedux.whiteappleboost = d.CommRedux.whiteappleboost + 0.5
			p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
			p:EvaluateItems()
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, e, amount, flags, source, countdown)
	if e.Type == EntityType.ENTITY_PLAYER then
		local p = e:ToPlayer()
		local d = p:GetData()
		
		if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_COUNTERFEITDOLLAR) and flags & DamageFlag.DAMAGE_NO_PENALTIES < 1 then
			local crng = p:GetCollectibleRNG(CollectibleType.COLLECTIBLE_CR_COUNTERFEITDOLLAR)
			local numcoins = CommRedux.rand(3, 4, crng)
			p:AddCoins(-numcoins)
			for i = 1, numcoins - 1 do
				local coin = Isaac.Spawn(5, 20, CoinSubType.COIN_PENNY, p.Position, Vector(CommRedux.rand_float(2.5, 5, crng), 0):Rotated(CommRedux.rand(0, 360, crng)), p)
			end
		end
		
		if p:HasTrinket(TrinketType.TRINKET_CR_INSANEPENDANT) and not d.TakingInsaneDamage then
			d.TakingInsaneDamage = true
			p:TakeDamage(amount * (p:GetTrinketMultiplier(TrinketType.TRINKET_CR_INSANEPENDANT) + 1), flags, source, countdown)
			d.TakingInsaneDamage = false
			return false
		end
		
		if p:HasTrinket(TrinketType.TRINKET_CR_FIGLEAF) then
			if flags & DamageFlag.DAMAGE_NO_PENALTIES < 1 then
				p:TakeDamage(amount, flags | DamageFlag.DAMAGE_NO_PENALTIES, source, countdown)
				return false
			end 
		end
		
		if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_VOODOOPIN) then
			local chance = CommRedux.rand(1, 2, modrng)
			if amount > 0 and chance == 1 then
				p:TakeDamage(0, flags | DamageFlag.DAMAGE_FAKE, source, countdown)
				p:SetMinDamageCooldown(countdown*2)
				return false
			end
		end
		
		if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_BLOODYFEATHER) then
			p:UseActiveItem(CollectibleType.COLLECTIBLE_CRACK_THE_SKY, false, false, false, false, 0)
		end
		
		if p:HasCollectible(Transformation.CR_TAMMY) then
			p:UseActiveItem(CollectibleType.COLLECTIBLE_TAMMYS_HEAD, false, false, false, false, 0)
			d.CommRedux.whiteappleboost = d.CommRedux.whiteappleboost + 2
			p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
			p:EvaluateItems()
		end
		
		d.dmgcountdown = countdown
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(_, f)
	local d = f:GetData()
	if f.SubType == CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE or f.SubType == CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED then
		if d.sprscale == nil then d.sprscale = 1 end
		f.Color = Color(f.Color.R, f.Color.G, f.Color.B, d.sprscale^3, f.Color.RO, f.Color.GO, f.Color.BO)
		d.sprscale = d.sprscale / 1.00325
		f.SpriteScale = Vector(d.sprscale, d.sprscale)
		if d.sprscale < 0.75 then f:Kill() end
	end
end, 206)


CommRedux:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
	if pickup.SubType == CollectibleType.COLLECTIBLE_HEAD_OF_KRAMPUS then
		if CommRedux.rand(1, 8, pickup:GetDropRNG()) == 1 then
			pickup:Morph(pickup.Type, pickup.Variant, CollectibleType.COLLECTIBLE_CR_COUNTERFEITDOLLAR, true, true, true)
		end
	end
	
	local level = game:GetLevel()
	local curse = level:GetCurses()
	if curse & LevelCurse.CURSE_OF_BLIND > 0 then
		
	end
end, PickupVariant.PICKUP_COLLECTIBLE)

CommRedux:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pickup)
	if pickup.SubType == CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE or pickup.SubType == CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED then
		pickup:Morph(pickup.Type, pickup.Variant, CollectibleType.COLLECTIBLE_CR_THEAPPLE, true, true, true)
	end
	if pickup.SubType == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2] or pickup.SubType == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3] or pickup.SubType == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4] then
		pickup:Morph(pickup.Type, pickup.Variant, CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1], true, true, true)
	end
end, PickupVariant.PICKUP_COLLECTIBLE)





CBLib.AddToCallback(ExCB.ModCallbacks.MC_POST_ROOM_CLEAR, function()
	local room = game:GetRoom()
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		
		if p:HasTrinket(TrinketType.TRINKET_CR_ICHOR) then
			if p:GetSoulHearts() % 2 == 1 then
				p:AddSoulHearts(1, false)
				sfx:Play(SoundEffect.SOUND_HOLY, 1, 0, false, 1)
			end
		end
		
		if room:GetType() == RoomType.ROOM_BOSS or room:GetType() == RoomType.ROOM_MINIBOSS then
			if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_CANTRIPBAG) then
				local ran = CommRedux.rand(1,4,p:GetCollectibleRNG(CollectibleType.COLLECTIBLE_CR_CANTRIPBAG))
				if ran == 1 then
					Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, Card.CARD_CR_TREASURE, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
				elseif ran == 2 then
					Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, Card.CARD_CR_LOOT, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
				elseif ran == 3 then
					Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, Card.CARD_CR_MONSTER, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
				elseif ran == 4 then
					Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TAROTCARD, Card.CARD_CR_SOUL, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
				end
			end
		end
	end
	
	if room:GetType() == RoomType.ROOM_BOSS then
		if CommRedux.sd.unlocks and room:GetFrameCount() <= 300 and not CommRedux.sd.unlocks.insanependant then
			CommRedux.sd.unlocks.insanependant = true
			miniAch.AddToQueue("insane.png")
		end
	end
end)

local playerCreepList = {
	EffectVariant.PLAYER_CREEP_LEMON_MISHAP,
	EffectVariant.PLAYER_CREEP_HOLYWATER,
	EffectVariant.PLAYER_CREEP_WHITE,
	EffectVariant.PLAYER_CREEP_BLACK,
	EffectVariant.PLAYER_CREEP_RED,
	EffectVariant.PLAYER_CREEP_GREEN,
	EffectVariant.PLAYER_CREEP_HOLYWATER_TRAIL,
	EffectVariant.PLAYER_CREEP_LEMON_PARTY,
	EffectVariant.PENTAGRAM_BLACKPOWDER
}
local function onPlayerCreepUpdate(_, effect)
	local p = CommRedux.getPlayerFromCreep(effect)
	if p then
		if p:HasTrinket(TrinketType.TRINKET_CR_PUTRIDGLAND) then
			local variant = effect.Variant
			for _, enemy in pairs(Isaac.FindInRadius(effect.Position, CommRedux.GetCreepScale(effect), EntityPartition.ENEMY)) do
				if enemy:IsVulnerableEnemy() and enemy:IsFlying() then
					local enemyData = enemy:GetData()
					
					if enemyData.tookFlyingCreepDamageFrame == nil then
						enemyData.tookFlyingCreepDamageFrame = -100
					end
					
					local delay = 20
					if variant == EffectVariant.PLAYER_CREEP_GREEN then
						delay = 4
					end
					
					local gameFrameCount = Isaac.GetFrameCount()
					if gameFrameCount >= (enemyData.tookFlyingCreepDamageFrame + delay) then
						enemyData.tookFlyingCreepDamageFrame = gameFrameCount
						if variant == EffectVariant.PLAYER_CREEP_WHITE or variant == EffectVariant.PLAYER_CREEP_BLACK then --slowing creep
							local slowPower = 2
							if p:GetTrinketMultiplier(TrinketType.TRINKET_CR_PUTRIDGLAND) > 1 then
								slowPower = 2.5
							end
							enemy:AddSlowing(EntityRef(effect), 11, slowPower, enemy:GetColor())
						else --damaging creep
							local damage = 2
							if variant == EffectVariant.PLAYER_CREEP_LEMON_MISHAP or variant == EffectVariant.PLAYER_CREEP_HOLYWATER or variant == EffectVariant.PLAYER_CREEP_LEMON_PARTY then
								damage = 8
							elseif variant == EffectVariant.PLAYER_CREEP_GREEN then
								damage = 1
							elseif variant == EffectVariant.PENTAGRAM_BLACKPOWDER then
								damage = 10
							end
							
							if p:GetTrinketMultiplier(TrinketType.TRINKET_CR_PUTRIDGLAND) > 1 then
								damage = math.ceil(damage * 1.5)
							end
							
							enemy:TakeDamage(damage, DamageFlag.DAMAGE_ACID, EntityRef(effect), 0)
						end
					end
				end
			end
		
			if variant ~= EffectVariant.PENTAGRAM_BLACKPOWDER then
				CommRedux.SpawnPutridBubbles(effect)
			end
		end
	end
end
for i = 1, #playerCreepList do
	CommRedux:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, onPlayerCreepUpdate, playerCreepList[i])
end


--player creep damages airborne enemies

CommRedux:AddCallback(ModCallbacks.MC_POST_BOMB_INIT, function(_, bomb)
	local player = CommRedux.getPlayerFromTear(bomb)
	if player then 
		if player:HasCollectible(CollectibleType.COLLECTIBLE_CR_CRYOBOMBS) then
			if (bomb.Variant > 4 or bomb.Variant < 3) then
				if bomb.Variant == 0 then
					bomb.Variant = BombVariant.BOMB_ICE
				end
			end
			bomb:GetData().isIceBomb = true
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_BOMB_UPDATE, function(_, bomb)
    local player = CommRedux.getPlayerFromTear(bomb)
	if bomb:GetData().isIceBomb == true and bomb:GetSprite():GetAnimation() == "Explode" then
		local chance = 1
		local luckchance = (player.Luck - 1)
		if luckchance < 1 then luckchance = 1 end
		if bomb.IsFetus then chance = CommRedux.rand(1, 10, bomb:GetDropRNG()) - luckchance end
		if chance < 1 then chance = 1 end
		if chance == 1 then
			local sx = CommRedux.rand(-1, 0, bomb:GetDropRNG())
			if sx == 0 then sx = 1 end
			local sy = CommRedux.rand(-1, 0, bomb:GetDropRNG())
			if sy == 0 then sy = 1 end
			
			sfx:Play(SoundEffect.SOUND_FREEZE_SHATTER, 1, 0, false, 0.75, 0)
			
			local poof = Isaac.Spawn(1000, 16, 5, bomb.Position, Vector.Zero, bomb):ToEffect()
			poof:GetSprite().Color = Color(0, 0, 0, 1, 0.75, 0.9, 1) 
			poof.Rotation = CommRedux.rand(0, 1, bomb:GetDropRNG()) * 180
			
			poof.SpriteScale = Vector(sx, sy) * ((CommRedux.getBombRadiusFromDamage(bomb.ExplosionDamage) * bomb.RadiusMultiplier) / 100)
			
			for i, e in ipairs(Isaac.FindInRadius(bomb.Position, CommRedux.getBombRadiusFromDamage(bomb.ExplosionDamage) * bomb.RadiusMultiplier, EntityPartition.ENEMY)) do
				if bomb.ExplosionDamage >= e.HitPoints and not e:IsBoss() and e:IsActiveEnemy() and e:IsVulnerableEnemy() and e:IsEnemy() then
					e:AddEntityFlags(EntityFlag.FLAG_ICE)
					e:AddEntityFlags(EntityFlag.FLAG_FREEZE)
					e:TakeDamage(e.HitPoints + 1, 0, EntityRef(bomb), 0)
				end
			end
		end
	end	
end)

CommRedux:AddCallback(ModCallbacks.MC_PRE_PLAYER_COLLISION, function(_, p, coll, low)
    if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_CORDYCEPS) then
		if coll:IsEnemy() and coll:IsActiveEnemy() and coll:IsVulnerableEnemy() then
			local npc = coll:ToNPC()
			npc:AddCharmed(EntityRef(p), 30 * 12)
			local d = npc:GetData()
			d.cordycepsCharmParent = p
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_COLLISION, function(_, e, coll, low)
    local d = e:GetData()
	if d.cordycepsCharmParent then
		local p = d.cordycepsCharmParent:ToPlayer()
		if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_CORDYCEPS) and e:HasEntityFlags(EntityFlag.FLAG_CHARM) then
			if coll:IsEnemy() and coll:IsActiveEnemy() and coll:IsVulnerableEnemy() then
				local npc = coll:ToNPC()
				npc:AddCharmed(EntityRef(p), 2)
				local npd = npc:GetData()
				npd.cordycepsCharmParent = d.cordycepsCharmParent
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
    local d = e:GetData()
	if d.cordycepsCharmParent then
		e:AddCharmed(EntityRef(d.cordycepsCharmParent), 2)
	end
end)

print("[Community Remix] V"..thisversion[1]..'.'..thisversion[2]..'.'..thisversion[3]..' loaded successfully!')
print('')
print('')
print('')