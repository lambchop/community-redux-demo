local thisversion = 1.1

local function loadscript()
	ExCB = RegisterMod("Extra Callbacks", 1)
	ExCB.Version = thisversion
	ExCB.Loaded = false
	print("[".. ExCB.Name .."]", "loading script V" .. ExCB.Version .. "...")
	ExCB.ModCallbacks = {}

	if CBLib then
		CBLib.InitializeMod(ExCB)
		
		local game = Game()
		
		ExCB.ModCallbacks.MC_POST_TEAR_FALL = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_TEAR_DEATH = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_TEAR_HIT = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_ENTITY_INIT = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_ENTITY_UPDATE = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_ENTITY_RENDER = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_ROOM_CLEAR = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_AMBUSH_DONE = CBLib.AddCustomCallback(ExCB)
		ExCB.ModCallbacks.MC_POST_GREED_WAVE = CBLib.AddCustomCallback(ExCB)

		
		CBLib.AddToCallback(ModCallbacks.MC_POST_TEAR_UPDATE, function(_, t)
			local d = t:GetData()
			if d.excb_tearfall == nil then d.excb_tearfall = false end
			if t.Height > -5 then
				if d.excb_tearfall == false then
					CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_TEAR_FALL, ExCB, t)
					d.excb_tearfall = true
				end
			elseif d.excb_tearfall == false and t.Height <= -5 then
				CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_TEAR_HIT, ExCB, t)
			end
			
			if t:IsDead() then
				CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_TEAR_DEATH, ExCB, t)
			end
		end, ExCB)
		
		CBLib.AddToCallback(ModCallbacks.MC_PRE_TEAR_COLLISION, function(_, t, coll, low)
			CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_TEAR_HIT, ExCB, t, coll)
			CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_TEAR_DEATH, ExCB, t)
		end, ExCB)
		
		local InitCallbacks =
		{
			ModCallbacks.MC_FAMILIAR_INIT,
			ModCallbacks.MC_POST_NPC_INIT,
			ModCallbacks.MC_POST_PICKUP_INIT,
			ModCallbacks.MC_POST_TEAR_INIT,
			ModCallbacks.MC_POST_PROJECTILE_INIT,
			ModCallbacks.MC_POST_LASER_INIT,
			ModCallbacks.MC_POST_EFFECT_INIT,
			ModCallbacks.MC_POST_BOMB_INIT,
			ModCallbacks.MC_POST_KNIFE_INIT,
			ModCallbacks.MC_POST_PLAYER_INIT
		}
		CBLib.AddToCallback(InitCallbacks, function(_, e)
			CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_ENTITY_INIT, ExCB, e)
		end, ExCB)
		
		local UpdateCallbacks =
		{
			ModCallbacks.MC_FAMILIAR_UPDATE,
			ModCallbacks.MC_NPC_UPDATE,
			ModCallbacks.MC_POST_PICKUP_UPDATE,
			ModCallbacks.MC_POST_TEAR_UPDATE,
			ModCallbacks.MC_POST_PROJECTILE_UPDATE,
			ModCallbacks.MC_POST_LASER_UPDATE,
			ModCallbacks.MC_POST_EFFECT_UPDATE,
			ModCallbacks.MC_POST_BOMB_UPDATE,
			ModCallbacks.MC_POST_KNIFE_UPDATE,
			ModCallbacks.MC_POST_PLAYER_UPDATE
		}
		CBLib.AddToCallback(UpdateCallbacks, function(_, e)
			CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_ENTITY_UPDATE, ExCB, e)
		end, ExCB)
		
		local RenderCallbacks =
		{
			ModCallbacks.MC_POST_FAMILIAR_RENDER,
			ModCallbacks.MC_POST_NPC_RENDER,
			ModCallbacks.MC_POST_PICKUP_RENDER,
			ModCallbacks.MC_POST_TEAR_RENDER,
			ModCallbacks.MC_POST_PROJECTILE_RENDER,
			ModCallbacks.MC_POST_LASER_RENDER,
			ModCallbacks.MC_POST_EFFECT_RENDER,
			ModCallbacks.MC_POST_BOMB_RENDER,
			ModCallbacks.MC_POST_KNIFE_RENDER,
			ModCallbacks.MC_POST_PLAYER_RENDER
		}
		CBLib.AddToCallback(RenderCallbacks, function(_, e)
			CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_ENTITY_RENDER, ExCB, e)
		end, ExCB)
		

		local roomWasCleared = true
		local roomWasJustCleared = false
		local roomAmbushWasDone = true
		local roomAmbushWasJustDone = false
		local lastGreedModeWave = nil
		local greedWaveWasCompleted = false
		CBLib.AddToCallback(ModCallbacks.MC_POST_UPDATE, function()
			local level = Game():GetLevel()
			local room = level:GetCurrentRoom()
			
			local roomIsCleared = room:IsClear()
			
			roomWasJustCleared = false
			if roomIsCleared and not roomWasCleared then
				roomWasJustCleared = true
				
				CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_ROOM_CLEAR, ExCB)
			end
			
			roomWasCleared = roomIsCleared
			
			
			local roomAmbushDone = room:IsAmbushDone()
			
			roomAmbushWasJustDone = false
			if roomAmbushDone and not roomAmbushWasDone then
				roomAmbushWasJustDone = true
				
				CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_AMBUSH_DONE, ExCB)
			end
			
			roomAmbushWasDone = roomAmbushDone
			
			
			if Game():IsGreedMode() then
				local greedModeWave = level.GreedModeWave
				if not lastGreedModeWave then
					lastGreedModeWave = greedModeWave
				end
				
				greedWaveWasCompleted = false
				if greedModeWave > lastGreedModeWave then
					greedWaveWasCompleted = true
					lastGreedModeWave = greedModeWave
					
					CBLib.RunCallback(ExCB.ModCallbacks.MC_POST_GREED_WAVE, ExCB)
				end
			end
		end, ExCB)
		CBLib.AddToCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
			roomWasJustCleared = false
			roomWasCleared = true
			
			roomAmbushWasJustDone = false
			roomAmbushWasDone = true

			if Game():IsGreedMode() then
				lastGreedModeWave = 0
			end
		end, ExCB)
		CBLib.AddToCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
			roomWasJustCleared = false
			roomWasCleared = true
			
			roomAmbushWasJustDone = false
			roomAmbushWasDone = true
		end, ExCB)
		
		--[[ examples
		CBLib.AddToCallback(ExCB.ModCallbacks.MC_TRUE_PLAYER_INIT, function(p)
		end)
		
		CBLib.AddToCallback(ExCB.ModCallbacks.MC_POST_TEAR_FALL, function(t)
		end)
		
		CBLib.AddToCallback(ExCB.ModCallbacks.MC_POST_TEAR_DEATH, function(t)
		end)
		
		CBLib.AddToCallback(ExCB.ModCallbacks.MC_POST_TEAR_HIT, function(t, coll)
			if coll then 
				-- do enemy things
			end
		end)
		]]
		
		ExCB.Loaded = true
		print("[".. ExCB.Name .."]", "loaded successfully")
	else
		error("[" .. ExCB.Name .. "]", ":", "[" .. ExCB.Name .. "]", "must be loaded after CBLib")
	end
end

if ExCB then
	if ExCB.Version < thisversion then
		print("[".. ExCB.Name .."]", "found old script V" .. ExCB.Version .. ", found new script V" .. thisversion .. ". replacing...")
		ExCB = nil
		loadscript()
		print("[".. ExCB.Name .."]", "replaced with V" .. ExCB.Version)
	end
elseif not ExCB then
	loadscript()
end