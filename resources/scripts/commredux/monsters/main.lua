local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == 1400 then
		local d = e:GetData()
		d.grid_cd = 0
		d.path_spd = 2
		d.atk_cd = 60
		d.creep_cd = 4
		d.sound_cd = 100
	end
end, 10)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == 1400 then
		local d = e:GetData()
		local s = e:GetSprite()
		local path = e.Pathfinder
		
		if d.grid_cd > 0 then d.grid_cd = d.grid_cd - 1 end
		if d.atk_cd > 0 then d.atk_cd = d.atk_cd - 1 end
		if d.creep_cd > 0 then d.creep_cd = d.creep_cd - 1 end
		if d.sound_cd > 0 then d.sound_cd = d.sound_cd - 1 end
		
		e.Target = e:GetPlayerTarget()
		e.TargetPosition = e.Target.Position
		
		if e.State == 0 then
			e.State = 4
		end
		
		if e:CollidesWithGrid() then
			d.grid_cd = 30
		end
		
		if e.State == 4 then
			if d.sound_cd == 0 then
				sfx:Play(SoundEffect.SOUND_ANGRY_GURGLE, 1, 0, false, 1, 0)
				d.sound_cd = CommRedux.rand(80, 120, modrng)
			end
			
			if e.Position.X > e.TargetPosition.X then
				e.FlipX = true
			else
				e.FlipX = false
			end
			e:MultiplyFriction(0.5)
			if d.creep_cd == 0 then
				local creep = Isaac.Spawn(1000, EffectVariant.CREEP_RED, 0, e.Position, Vector.Zero, e):ToEffect()
				creep:SetColor(Color(1, 1, 1, 0, 0, 0, 0), 1, 999, false, true)
				creep.Scale = 0.75
				creep:SetTimeout(30)
				d.creep_cd = 12
			end
			e:AnimWalkFrame('WalkHori', 'WalkVert', 0.1)
			if path:HasPathToPos(e.TargetPosition, true) then
				if d.grid_cd <= 0 then
					e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
				else
					path:FindGridPath(e.TargetPosition, d.path_spd * 0.25, 1, false)
				end
			end
			if (e.TargetPosition - e.Position):Length() <= 256 and d.atk_cd == 0 then
				e.State = 8
			end
		end
		
		if e.State == 8 then
			s:RemoveOverlay()
			if s:GetAnimation() ~= "Puke" and d.atk_cd == 0 then
				s:Play("Puke")
			end
			e:MultiplyFriction(0.75)
			
			if s:IsEventTriggered("Shoot") or s:IsEventTriggered("ShootMain") then
				--local creep = Isaac.Spawn(1000, EffectVariant.CREEP_RED, 0, e.Position + e.Velocity*3, Vector.Zero, e):ToEffect()
				--creep:SetColor(Color(1, 1, 1, 0, 0, 0, 0), 1, 999, false, true)
				--creep.SpriteScale = Vector(2, 2)
				--creep:SetTimeout(120)
				
				for i = 0, CommRedux.rand(0, 1, modrng) do
					local prj = Isaac.Spawn(9, 0, 0, e.Position, (((e.Target.Position + e.Target.Velocity*10) - e.Position):Normalized() * 7.5):Rotated(CommRedux.rand(-10, 10, modrng)), e):ToProjectile()
					prj.FallingAccel = 1 + CommRedux.rand_float(-0.5, 0.5, e:GetDropRNG())
					prj.FallingSpeed = -16 + CommRedux.rand_float(-2, 2, e:GetDropRNG())
					prj.Scale = 1.25 + CommRedux.rand_float(-0.25, 0.25, modrng)
					prj.CollisionDamage = 2
					if prj.Scale <= 1.25 then prj.CollisionDamage = 1 end
					prj:GetData().isCreepProjectile = true
				end
			end
			
			if s:IsEventTriggered("ShootMain") then
				sfx:Play(SoundEffect.SOUND_SHAKEY_KID_ROAR, 1, 0, false, 0.7, 0)
				sfx:Play(SoundEffect.SOUND_MEATHEADSHOOT, 1, 0, false, 1, 0)
			end
			
			if s:GetAnimation() == "Puke" and s:IsFinished() then
				d.atk_cd = CommRedux.rand(80, 100, modrng)
				e.State = 4
			end
		end
		return true
	end
end, 10)

CommRedux:AddCallback(ModCallbacks.MC_POST_PROJECTILE_UPDATE, function(_, proj)
	local d = proj:GetData()
	if d.isCreepProjectile then
		if proj:IsDead() then
			local creep = Isaac.Spawn(1000, EffectVariant.CREEP_RED, 0, proj.Position, Vector.Zero, proj):ToEffect()
			creep:SetColor(Color(1, 1, 1, 0, 0, 0, 0), 1, 999, false, true)
			creep.SpriteScale = Vector(proj.Scale, proj.Scale)
			creep:SetTimeout(60)
		end
	end
	
	if d.isPoopGridProjectile then
		if proj:IsDead() then
			local room = game:GetRoom()
			local clampedpos = room:GetClampedPosition(proj.Position, 0)
			local gent = room:GetGridEntityFromPos(clampedpos)
			if not gent or (gent.CollisionClass == GridCollisionClass.COLLISION_NONE and gent.Desc.Type ~= GridEntityType.GRID_POOP) then
				local g2 = Isaac.GridSpawn(GridEntityType.GRID_POOP, 0, clampedpos, true):ToPoop()
				g2:ReduceSpawnRate()
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == ENTITYVARIANT_FLYORGY then
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		if d.spitfly_cd == nil then d.spitfly_cd = 120 end
		if d.spitfly_cd > 0 then d.spitfly_cd = d.spitfly_cd - 1 end
		
		e:MultiplyFriction(0.8)
		
		if d.spitfly_cd <= 0 and (e.Target.Position - e.Position):Length() >= 160 then
			s:Play("Fire", true)
			d.spitfly_cd = CommRedux.rand(60, 70, e:GetDropRNG())
		end
		
		if s:IsEventTriggered("Fire") then
			local fly = Isaac.Spawn(18, 0, 0, e.Position + (e.Target.Position - e.Position):Normalized() * 20, Vector.Zero, e)
			fly:AddEntityFlags(e:GetEntityFlags() | EntityFlag.FLAG_KNOCKED_BACK)
			fly:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
			fly.Velocity = (e.Target.Position - e.Position):Resized(3)
			e.Velocity = - fly.Velocity
			
			
		end
		
		e.Velocity = e.Velocity + ((e.Target.Position - e.Position):Resized(1)):Rotated(CommRedux.rand(-90, 90, e:GetDropRNG()))
		
		if s:IsFinished() and (s:GetAnimation() == "Fire" or s:GetAnimation() == "Appear") then
			s:Play("Fly")
		end
		
		if e:IsDead() then
			for i = 1, 4 do
				local fly = Isaac.Spawn(18, 0, 0, e.Position, Vector.Zero, e)
				fly:AddEntityFlags(e:GetEntityFlags() | EntityFlag.FLAG_KNOCKED_BACK)
				fly:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
				fly.Velocity = ((e.Target.Position - e.Position):Resized(CommRedux.rand_float(1, 1.5, e:GetDropRNG()))):Rotated(CommRedux.rand(-360, 360, e:GetDropRNG()))
			end
		end
		return true
	end
end, 18)


CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == ENTITYVARIANT_MEGAPOOTER then
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		if s:IsEventTriggered("Shoot") then
			local params = ProjectileParams()
			params.HeightModifier = 8
			sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1, 0)
			e:FireProjectiles(e.Position, (e.Target.Position - e.Position):Normalized() * 7, 2, params)
			return true
		end
	end
end, 14)


CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == ENTITYVARIANT_BURROW then
		local d = e:GetData()
		d.isvuln = false
	end
end, 244)


CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == ENTITYVARIANT_BURROW then
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		e:MultiplyFriction(0)
		
		if s:IsEventTriggered("Shoot") then
			local params = ProjectileParams()
			params.HeightModifier = 8
			sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1, 0)
			e:FireProjectiles(e.Position, (e.Target.Position - e.Position):Normalized() * 7.5, 1, params)
			return true
		end
		
		if d.isvuln == nil then d.isvuln = false end
		
		if s:IsEventTriggered("open") then
			d.isvuln = true
		end
		
		if s:IsEventTriggered("close") then
			d.isvuln = false
		end
		
		if s:GetFrame() >= 16 and s:GetAnimation() == "DigOut" and (e.Target.Position - e.Position):Length() < 256 then
			if CommRedux.rand(1, 64, e:GetDropRNG()) == 1 then
				s:Play("Fire", true)
			end
			if s:IsFinished() then s:SetFrame(16) end
			return true
		end
		
		if s:GetAnimation() == "Fire" then
			if not s:IsFinished() then
				return true
			else
				if CommRedux.rand(1, 2, e:GetDropRNG()) == 1 then
					s:Play("DigIn", true)
					e.State = 6
				else
					s:Play("DigOut", true)
					s:SetFrame(16)
				end
			end
		end
		
		e:MultiplyFriction(0)
		e:AddEntityFlags(EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK | EntityFlag.FLAG_NO_KNOCKBACK)
	end
end, 244)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == 1401 then
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		if s:IsEventTriggered("Shoot") then
			local prj = Isaac.Spawn(9, ProjectileVariant.PROJECTILE_PUKE, 0, e.Position, (((e.Target.Position + e.Target.Velocity*10) - e.Position):Normalized() * 7.5):Rotated(CommRedux.rand(-10, 10, modrng)), e):ToProjectile()
			prj.FallingAccel = 1 + CommRedux.rand_float(-0.5, 0.5, e:GetDropRNG())
			prj.FallingSpeed = -16 + CommRedux.rand_float(-2, 2, e:GetDropRNG())
			prj.Scale = 1.75
			prj.CollisionDamage = 2
			prj:GetData().isPoopGridProjectile = true
			
			sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1, 0)
			e:MultiplyFriction(0)
			return true
		end
	end
end, 244)

CommRedux:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, e, amount, flags, source, countdown)
	if e.Type == 244 and e.Variant == ENTITYVARIANT_BURROW then
		local e = e:ToNPC()
		local d = e:GetData()
		return d.isvuln
	end
	
	if e.Type == 12 and e.Variant == ENTITYVARIANT_HURL then
		local e = e:ToNPC()
		local d = e:GetData()
		if (source.Type ~= 4 and source.Type ~= 1 and source.Type ~= 2) and flags & DamageFlag.DAMAGE_EXPLOSION > 0 then
			return false
		end
	end
	
	if e.Type == EntityType.ENTITY_CAST then
		return false
	end
	
	if e.Type == EntityType.ENTITY_CREDUX_ENEMY then
		local e = e:ToNPC()
		local d = e:GetData()
		if e.Variant == CREDUX_ENTITYVARIANT_SPIDERDEN and e.State == 3 and d.atk_cd and d.atk_cd <= 0 then
			e.State = 8
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == 1 and e.SubType == 0 then
		if CommRedux.rand(1, 6, e:GetDropRNG()) == 1 then
			e:Morph(e.Type, e.Variant, 64, e:GetChampionColorIdx())
		end
	end
end, 29)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == 1 and e.SubType == 0 then
		if CommRedux.rand(1, 6, e:GetDropRNG()) == 1 then
			e:Morph(e.Type, 1400, e.SubType, e:GetChampionColorIdx())
		end
	end
end, 14)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == 1 and e.SubType == 64 then
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		
		if s:IsEventTriggered("Land") then
			sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1, 0)
			local creep = Isaac.Spawn(1000, EffectVariant.CREEP_RED, 0, e.Position + e.Velocity*2, Vector.Zero, e):ToEffect()
			creep.SpriteScale = Vector(2, 2)
			creep:SetColor(Color(creep.Color.R, creep.Color.G, creep.Color.B, 0, creep.Color.RO, creep.Color.GO, creep.Color.BO), 1, 999, true, true)
			return true
		end
	end
end, 29)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == ENTITYVARIANT_HURL then
		e.SplatColor = Color(0, 0.4, 0.4, 1, 0, 0.4, 0.15)
	end
end, 12)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == ENTITYVARIANT_HURL then
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		
		if s:IsEventTriggered("Shoot") or s:WasEventTriggered("Shoot") then
			e:MultiplyFriction(0.75)
			if s:IsEventTriggered("Shoot") then
				sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 0.5, 0)
				sfx:Play(SoundEffect.SOUND_SHAKEY_KID_ROAR, 1, 0, false, 0.9, 0)
				
				local fx = Isaac.Spawn(1000, 2, 5, e.Position, Vector.Zero, e):ToEffect()
				fx.RenderZOffset = 2
				fx.SpriteOffset = Vector(0, - 12)
				fx.Color = e.SplatColor
				
				
				local prj = Isaac.Spawn(9, 4, 0, e.Position, (e.Target.Position - e.Position):Normalized() * 7.5, e):ToProjectile()
				prj:AddProjectileFlags(1 << 1)
				prj.Color = Color(0.3, 1, 0.3, 1, 0, 0, 0)
				prj.FallingAccel = 1 + CommRedux.rand_float(-0.25, 0.25, e:GetDropRNG())
				prj.FallingSpeed = -16 + CommRedux.rand_float(-1, 1, e:GetDropRNG())
				prj.Scale = 1.5
				prj.CollisionDamage = 2
				
				local prjs = prj:GetSprite()
				prjs:Load("gfx/bullets/bullet.anm2", true)
				prjs:Update()
			end
			if s:IsFinished() and s:GetAnimation() == "Attack" then
				s:Play("Shake")
			end
			return true
		end
	end
end, 12)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	local d = e:GetData()
	local s = e:GetSprite()
	d.maxcooldown = 12
	d.cooldown = (e.SubType*12)
	e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
	e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
	e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
	e:AddEntityFlags(
		EntityFlag.FLAG_NO_TARGET | EntityFlag.FLAG_NO_STATUS_EFFECTS |
		EntityFlag.FLAG_NO_BLOOD_SPLASH | EntityFlag.FLAG_DONT_OVERWRITE |
		EntityFlag.FLAG_NO_DEATH_TRIGGER
	)
	
	e.State = NpcState.STATE_IDLE
end, EntityType.ENTITY_CAST)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_HAZARD_REINIT, function(e)
	if e.Type == EntityType.ENTITY_CAST then
		local d = e:GetData()
		local s = e:GetSprite()
		d.maxcooldown = 12
		d.cooldown = (e.SubType*12)
		e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
		e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
		e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
		e:AddEntityFlags(
			EntityFlag.FLAG_NO_TARGET | EntityFlag.FLAG_NO_STATUS_EFFECTS |
			EntityFlag.FLAG_NO_BLOOD_SPLASH | EntityFlag.FLAG_DONT_OVERWRITE |
			EntityFlag.FLAG_NO_DEATH_TRIGGER
		)
		e.State = NpcState.STATE_IDLE
	end
end, CommRedux)


CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	e.Target = e:GetPlayerTarget()
	local s = e:GetSprite()
	local d = e:GetData()
	local nument = #Isaac.FindByType(e.Type, e.Variant, -1, false, false)
	d.maxcooldown = math.ceil(12 * nument) - 48
	
	
	local idx = tostring(Game():GetLevel():GetCurrentRoomDesc().SafeGridIndex)
    if not CommRedux.sd.roomdata[idx] then CommRedux.sd.roomdata[idx] = {} end
    CommRedux.sd.roomdata[idx][tostring(e.InitSeed)] = {
		Type = e.Type, 
		Variant = e.Variant, 
		SubType = e.SubType, 
		Data = d, 
		X = e.Position.X,
		Y = e.Position.Y
	}
	
	
	if e.Variant == ENTITYVARIANT_HOLLOWCAST then
		d.maxcooldown = 2
		if e.State == NpcState.STATE_IDLE then
			if d.cooldown > 0 then d.cooldown = d.cooldown - 1 end
			for i = 1, game:GetNumPlayers() do
				local p = Isaac.GetPlayer(i - 1)
				if d.cooldown <= 0 and (p.Position - e.Position):Length() < 24 then
					e.State = NpcState.STATE_STOMP
				else
					e.State = NpcState.STATE_IDLE
				end
			end
			return true
		end
		
		if s:IsEventTriggered("Wake") then
			sfx:Play(SoundEffect.SOUND_TOOTH_AND_NAIL_TICK, 0.5, 0, false, 1, 0)
		end
	end
	
end, EntityType.ENTITY_CAST)

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	e:AddEntityFlags(EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK | EntityFlag.FLAG_NO_KNOCKBACK)
	e:ClearEntityFlags(EntityFlag.FLAG_KNOCKED_BACK)
	e:MultiplyFriction(0)
	e.Target = e:GetPlayerTarget()
	local s = e:GetSprite()
	local d = e:GetData()
	
	if s:WasEventTriggered("Slam") and not s:WasEventTriggered("Jump") then
		e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
		e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
	else
		e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
		e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
	end
	
	if s:IsFinished() and e.State == NpcState.STATE_STOMP then
		e.State = NpcState.STATE_IDLE
	end
	
	if e.State == NpcState.STATE_STOMP then
		s:Play("Slam", false)
		d.cooldown = d.maxcooldown
	end
	
	if e.State == NpcState.STATE_IDLE then
		s:Play("Idle", true)
		
		if d.cooldown > 0 then d.cooldown = d.cooldown - 1 end
		if d.cooldown <= 0 then 
			e.State = NpcState.STATE_STOMP
		else
			e.State = NpcState.STATE_IDLE
		end
	end
	
	if s:IsEventTriggered("Slam") then
		CommRedux.blastdamage(24, 40, 10, e, false, false, false)
		for i = 1, game:GetNumPlayers() do
			local p = Isaac.GetPlayer(i - 1)
			if (p.Position - e.Position):Length() < 24 then
				p:TakeDamage(2, 0, EntityRef(e), 60)
				p.Velocity = p.Velocity + (p.Position - e.Position):Resized(8) / ((p.Position - e.Position):Length() / 2)
			end
		end
		for i = 0, 5 do
			local sprtrail = game:Spawn(1000, 59, e.Position, -e.Velocity/2, e, 0, 0):ToEffect()
			sprtrail.Parent = e
			sprtrail:SetTimeout(10)
			sprtrail.Color = Color(1, 1, 1, 0.05, 0, 0, 0)
		end
		game:ShakeScreen(5)
		sfx:Play(SoundEffect.SOUND_STONE_IMPACT, 0.5, 0, false, 1, 0)
	end
end, EntityType.ENTITY_CAST)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == CREDUX_ENTITYVARIANT_SLAMBONE then
		local d = e:GetData()
		d.atk_cd = 15
		if e.SubType == 1 then
			e.SplatColor = Color(0, 0, 0, 1, 0.25, 0.15, 0.1)
		end
	end
	if e.Variant == CREDUX_ENTITYVARIANT_STU then
		local d = e:GetData()
		d.grid_cd = 0
		d.path_spd = 2
		d.atk_cd = 0
		d.creep_cd = 4
		d.sound_cd = 100
	end
	if e.Variant == CREDUX_ENTITYVARIANT_SPIDERDEN then
		local d = e:GetData()
		d.atk_cd = 0
		e.SplatColor = Color(0, 0, 0, 1, 1, 1, 1)
	end
	
	if e.Variant == CREDUX_ENTITYVARIANT_CHEW then
		local s = e:GetSprite()
		e.State = 1
		s:Play("Appear" .. e.SubType + 1)
		e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
		local d = e:GetData()
		if not d.atk_cd then d.atk_cd = (e.SubType + 1) * 16 end
		if d.wasCombined then
			e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
		end
	end
end, EntityType.ENTITY_CREDUX_ENEMY)

local stuPoopWhitelist = {
	217,
	220
}

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	if e.Variant == CREDUX_ENTITYVARIANT_SLAMBONE then
		e:AddEntityFlags(EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK | EntityFlag.FLAG_NO_KNOCKBACK)
		e:ClearEntityFlags(EntityFlag.FLAG_KNOCKED_BACK)
		e:MultiplyFriction(0)
		e.Target = e:GetPlayerTarget()
		e.TargetPosition = e.Target.Position
		local room = game:GetRoom()
		local s = e:GetSprite()
		local d = e:GetData()
		if d.atk_cd == nil then d.atk_cd = 60 end
		
		if e.State == NpcState.STATE_INIT then
			e.State = NpcState.STATE_APPEAR
		end
		if e.State == NpcState.STATE_APPEAR then
			e.State = NpcState.STATE_IDLE
		end
		if e.State == NpcState.STATE_IDLE then
			if d.atk_cd > 0 then d.atk_cd = d.atk_cd - 1 end
			s:Play("Idle", false)
			local dist = 128
			if (e.Position - e.Target.Position):Length() < dist and d.atk_cd <= 0 then
				e.State = NpcState.STATE_ATTACK
			end
		end
		
		if e.State == NpcState.STATE_ATTACK then
			if e.Position.X > e.TargetPosition.X then
				e.FlipX = true
			else
				e.FlipX = false
			end
			s:Play("Slam", false)
			if s:IsEventTriggered("Slam") then
				if e.SubType == 0 then
					--game:ShakeScreen(8)
					--sfx:Play(SoundEffect.SOUND_FORESTBOSS_STOMPS, 1, 0, false, 1, 0)
					sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 0.5, 0)
					local creep = Isaac.Spawn(1000, EffectVariant.CREEP_RED, 0, e.Position + e.Velocity*3, Vector.Zero, e):ToEffect()
					creep:SetColor(Color(1, 1, 1, 0, 0, 0, 0), 1, 999, false, true)
					creep.SpriteScale = Vector(5, 5)
					creep:SetTimeout(120)
					
					local spreadDegree = 360
					local numproj = 5
					for i = 0, numproj - 1 do
						local prj = Isaac.Spawn(9, 0, 0, e.Position, ((e.Target.Position - e.Position):Normalized() * 9):Rotated((spreadDegree/numproj)*i), e):ToProjectile()
						local degbetween = spreadDegree/numproj
						prj.Velocity = prj.Velocity:Rotated((-degbetween/2) * (numproj-1))
						prj.CollisionDamage = 2
						prj.Scale = 1.75
						prj:GetData().isCreepProjectile = true
						prj.FallingAccel = 1.5
						prj.Height = -16
						prj.FallingSpeed = -6
					end
				end
				
				if e.SubType == 1 then
					sfx:Play(SoundEffect.SOUND_STONE_IMPACT, 1, 0, false, 1, 0)
					
					local shockwave = Isaac.Spawn(1000, EffectVariant.SHOCKWAVE, 0, e.Position, Vector(0, 0), e):ToEffect()
					shockwave:SetDamageSource(e.Type)
					shockwave.Parent = e
					shockwave:SetRadii(35, 65)
					shockwave:SetTimeout(5)
				end
			end
			if s:IsEventTriggered("Rise") then
				if e.SubType == 0 then
					sfx:Play(SoundEffect.SOUND_MEAT_JUMPS, 1, 0, false, 0.5, 0)
				end
			end
			if s:IsFinished() then
				d.atk_cd = 60
				e.State = NpcState.STATE_IDLE
			end
		end
		if e:IsDead() then
			if e.SubType == 1 then
				sfx:Play(SoundEffect.SOUND_STONE_IMPACT, 1, 0, false, 1, 0)
			end
		end
	end
	
	if e.Variant == CREDUX_ENTITYVARIANT_STU then
		local d = e:GetData()
		local s = e:GetSprite()
		local path = e.Pathfinder
		
		if d.grid_cd > 0 then d.grid_cd = d.grid_cd - 1 end
		if d.creep_cd > 0 then d.creep_cd = d.creep_cd - 1 end
		if d.sound_cd > 0 then d.sound_cd = d.sound_cd - 1 end
		
		if e.State == 0 then
			e.State = 4
		end
		
		if e:CollidesWithGrid() then
			d.grid_cd = 30
		end
		
		if e.State == 4 or e.State == 9 then
			if d.sound_cd == 0 then
				sfx:Play(SoundEffect.SOUND_ANGRY_GURGLE, 1, 0, false, 1, 0)
				d.sound_cd = CommRedux.rand(80, 120, modrng)
			end
			if s:GetAnimation() ~= "Eat" then
				if e.Position.X > e.Position.X + e.Velocity.X then
					e.FlipX = true
				else
					e.FlipX = false
				end
			end
			e:MultiplyFriction(0.75)
		end
		
		if e.State == 9 then
			e:MultiplyFriction(0.75)
			e.Target = e:GetPlayerTarget()
			e.TargetPosition = e.Target.Position
			if e.Velocity:Length() > 0.1 then
				s:Play("WalkFull", false)
			else
				s:Play("IdleFull", false)
			end
			if (e.TargetPosition - e.Position):Length() <= 192 and d.atk_cd > 0 then
				e.State = 8
				s:Play("Fire", true)
			end
			if path:HasPathToPos(e.TargetPosition, true) and s:GetAnimation() ~= "Eat" then
				if d.grid_cd == 0 then
					e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
				else
					path:FindGridPath(e.TargetPosition, d.path_spd * 0.5, 1, false)
				end
			end
		end
		
		if e.State == 4 then
			e:MultiplyFriction(0.9)
			if s:GetAnimation() ~= "Eat" then
				if e.Velocity:Length() > 0.1 then
					s:Play("WalkEmpty", false)
				else
					s:Play("IdleEmpty", false)
				end
			else
				if s:IsFinished() == true then
					e.State = 9
				end
			end
			
			local gent = CommRedux.getclosestgrid(e.Position, GridEntityType.GRID_POOP, -1, GridCollisionClass.COLLISION_SOLID)
			local ent
			for i, v in ipairs(stuPoopWhitelist) do
				if not ent then
					ent = CommRedux.getclosestent(e.Position, v, -1, -1)
				end
			end
			
			if ent and d.atk_cd < 1 then
				gent = nil
				e.Target = e:GetPlayerTarget()
				e.TargetPosition = ent.Position
				
				if path:HasPathToPos(e.TargetPosition, true) then
					if d.grid_cd <= 0 then
						if (e.TargetPosition - e.Position):Length() > 40 and s:GetAnimation() ~= "Eat" then
							e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
						end
					else
						path:FindGridPath(e.TargetPosition, d.path_spd * 0.5, 1, false)
					end
					if (e.TargetPosition - e.Position):Length() <= 60 then
						s:Play("Eat", false)
						d.atk_cd = 1
						ent:Kill()
						if e.Position.X > ent.Position.X then
							e.FlipX = true
						else
							e.FlipX = false
						end
					end
				else
					e.Target = e:GetPlayerTarget()
					e.TargetPosition = e.Target.Position
					
					if path:HasPathToPos(e.TargetPosition, true) and s:GetAnimation() ~= "Eat" then
						if d.grid_cd <= 0 then
							e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
						else
							path:FindGridPath(e.TargetPosition, d.path_spd * 0.5, 1, false)
						end
					end
				end
			end
			
			if gent and d.atk_cd < 1 and not ent then
				e.Target = e:GetPlayerTarget()
				e.TargetPosition = gent.Position
				
				if path:HasPathToPos(e.TargetPosition, true) then
					if d.grid_cd <= 0 then
						if (e.TargetPosition - e.Position):Length() > 40 and s:GetAnimation() ~= "Eat" then
							e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
						end
					else
						path:FindGridPath(e.TargetPosition, d.path_spd * 0.5, 1, false)
					end
					if (e.TargetPosition - e.Position):Length() <= 60 then
						s:Play("Eat", false)
						d.atk_cd = 1
						gent:Destroy()
						if e.Position.X > gent.Position.X then
							e.FlipX = true
						else
							e.FlipX = false
						end
					end
				else
					e.Target = e:GetPlayerTarget()
					e.TargetPosition = e.Target.Position
					
					if path:HasPathToPos(e.TargetPosition, true) and s:GetAnimation() ~= "Eat" then
						if d.grid_cd <= 0 then
							e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
						else
							path:FindGridPath(e.TargetPosition, d.path_spd * 0.5, 1, false)
						end
					end
				end
			end
			
			if not gent and not ent then
				e.Target = e:GetPlayerTarget()
				e.TargetPosition = e.Target.Position
				
				if path:HasPathToPos(e.TargetPosition, true) and s:GetAnimation() ~= "Eat" then
					if d.grid_cd <= 0 then
						e.Velocity = e.Velocity - (e.Position - e.TargetPosition):Resized(d.path_spd)
					else
						path:FindGridPath(e.TargetPosition, d.path_spd * 0.5, 1, false)
					end
				end
			end
		end
		
		if e.State == 8 then
			e.Target = e:GetPlayerTarget()
			e.TargetPosition = e.Target.Position
			e:MultiplyFriction(0.9)
			
			if s:IsEventTriggered("Shoot") or s:IsEventTriggered("ShootMain") then
				for i = 0, CommRedux.rand(1, 2, modrng) do
					local prj = Isaac.Spawn(9, ProjectileVariant.PROJECTILE_PUKE, 0, e.Position, (((e.Target.Position + e.Target.Velocity*10) - e.Position):Normalized() * CommRedux.rand_float(8, 10, modrng)):Rotated(CommRedux.rand(-15, 15, modrng)), e):ToProjectile()
					prj.FallingAccel = 1 + CommRedux.rand_float(-0.5, 0.5, e:GetDropRNG())
					prj.FallingSpeed = -8 + CommRedux.rand_float(-4, 4, e:GetDropRNG())
					prj.Scale = 1.25 + CommRedux.rand_float(-0.25, 0.25, modrng)
					prj.CollisionDamage = 2
					if prj.Scale <= 1.25 then prj.CollisionDamage = 1 end
				end
			end
			
			if s:IsEventTriggered("ShootMain") then
				sfx:Play(SoundEffect.SOUND_SHAKEY_KID_ROAR, 1, 0, false, 0.7, 0)
				sfx:Play(SoundEffect.SOUND_MEATHEADSHOOT, 1, 0, false, 1, 0)
			end
			
			if s:GetAnimation() == "Fire" and s:IsFinished() then
				d.atk_cd = CommRedux.rand(10, 10, modrng)
				s:Play("Idle", true)
				d.atk_cd = 0
				e.State = 4
			end
		end
	end
	
	if e.Variant == CREDUX_ENTITYVARIANT_SPIDERDEN then
		local d = e:GetData()
		local s = e:GetSprite()
		e.Target = e:GetPlayerTarget()
		e.TargetPosition = e.Target.Position
		
		e:AddEntityFlags(EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK | EntityFlag.FLAG_NO_KNOCKBACK)
		e:ClearEntityFlags(EntityFlag.FLAG_KNOCKED_BACK)
		e:MultiplyFriction(0)
		
		if e.State == 0 then
			e.State = 1
		end
		
		if e.State == 1 then
			e.State = 3
		end
		
		if e.State == 3 then
			s:Play("Idle")
			if d.atk_cd > 0 then d.atk_cd = d.atk_cd - 1 end
			if (e.TargetPosition - e.Position):Length() < 128 and d.atk_cd <= 0 then
				e.State = 8
			end
		end
		
		if e.State == 8 then
			s:Play("Fire")
			if s:IsEventTriggered("Shoot") then
				sfx:Play(181, 1, 0, false, 1, 0)
				local isbig = CommRedux.rand(0, 7, modrng)
				if isbig == 0 then isbig = true else isbig = false end
				local spider = EntityNPC.ThrowSpider(e.Position, e, e.Position + Vector(CommRedux.rand_float(40, 80, modrng), 0):Rotated(CommRedux.rand(0, 360, modrng)), isbig, -32)
			end
			d.atk_cd = CommRedux.rand(60, 120, modrng)
			if s:IsFinished() then
				e.State = 3
			end
		end
		
		if e:IsDead() then
			for i = 1, CommRedux.rand(2, 3, modrng) do
				local spider = EntityNPC.ThrowSpider(e.Position, e, e.Position + Vector(CommRedux.rand_float(40, 80, modrng), 0):Rotated(CommRedux.rand(0, 360, modrng)), false, -32)
			end
			local prjs = CommRedux.shootSpreadProjectiles(e, e.Position, Vector(1, 0):Resized(8), 360, 6, 0)
			for i, prj in ipairs(prjs) do
				prj.Color = Color(0.5, 0.5, 0.5, 1, 0.5, 0.5, 0.5)
			end
		end
	end
	
	if e.Variant == CREDUX_ENTITYVARIANT_CHEW then
		local d = e:GetData()
		local s = e:GetSprite()
		e.Target = e:GetPlayerTarget()
		e.TargetPosition = e.Target.Position
		
		e:MultiplyFriction(0.8)
		
		if e.State == 0 then
			e.State = 1
		end
		
		if e.State == 1 then
			e.State = 4
		end
		
		if e.State == 4 then
			s:Play("Idle" .. e.SubType + 1)
			if d.atk_cd > 0 then d.atk_cd = d.atk_cd - 1 end
			if (e.TargetPosition - e.Position):Length() < 128 and d.atk_cd <= 0 then
				e.State = 8
			end
			e.Velocity = e.Velocity + (e.TargetPosition - e.Position):Resized(0.5)
		end
		
		if e.State == 8 then
			s:Play("Shoot" .. e.SubType + 1)
			if s:IsEventTriggered("Shoot") then
				sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1, 0)
				local prjs = CommRedux.shootSpreadProjectiles(e, e.Position, ((e.TargetPosition + e.Target.Velocity*12) - e.Position):Resized(7), 66, (e.SubType + 1))
			end
			d.atk_cd = (e.SubType + 1) * 16
			if s:IsFinished() then
				e.State = 4
			end
		end
		
		if e:IsDead() then
			if e.SubType == 3 then
				for i = 0, 1 do
					local spreadDegree = 180
					local numproj = 2
					local child = Isaac.Spawn(703, 3, 1, e.Position, (e.Position - (e.TargetPosition + e.Target.Velocity*12)):Resized(24):Rotated((spreadDegree/numproj)*i), e):ToNPC()
					local degbetween = spreadDegree/numproj
					child:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
					child.Velocity = child.Velocity:Rotated((-degbetween/2) * (numproj-1))
				end
			end
			if e.SubType == 2 then
				for i = 0, 1 do
					local spreadDegree = 180
					local numproj = 2
					local child = Isaac.Spawn(703, 3, 0+i, e.Position, (e.Position - (e.TargetPosition + e.Target.Velocity*12)):Resized(24):Rotated((spreadDegree/numproj)*i), e):ToNPC()
					local degbetween = spreadDegree/numproj
					child:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
					child.Velocity = child.Velocity:Rotated((-degbetween/2) * (numproj-1))
				end
			end
			if e.SubType == 1 then
				for i = 0, 1 do
					local spreadDegree = 180
					local numproj = 2
					local child = Isaac.Spawn(703, 3, 0, e.Position, (e.Position - (e.TargetPosition + e.Target.Velocity*12)):Resized(24):Rotated((spreadDegree/numproj)*i), e):ToNPC()
					local degbetween = spreadDegree/numproj
					child:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
					child.Velocity = child.Velocity:Rotated((-degbetween/2) * (numproj-1))
				end
			end
		end
	end
end, EntityType.ENTITY_CREDUX_ENEMY)

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	if e.Variant == 0 then
		e.Target = e:GetPlayerTarget()
		local room = game:GetRoom()
		local s = e:GetSprite()
		local d = e:GetData()
		
		if e.State == NpcState.STATE_INIT then
			e.State = NpcState.STATE_APPEAR
		end
		if e.State == NpcState.STATE_APPEAR then
			e.State = NpcState.STATE_IDLE
		end
		if e.State == NpcState.STATE_IDLE then
			s:Play("Idle", false)
			e:MultiplyFriction(0.7)
			if (e.Position - e.Target.Position):Length() > 160 then
				e.State = NpcState.STATE_MOVE
			else
				e.State = NpcState.STATE_ATTACK
			end
			
			if not d.jumppos then d.jumppos = e.Position end
			local jumpposlen = (e.Position - d.jumppos):Length()
			if jumpposlen > 8 and s:GetAnimation() == "Idle" then
				--e.Velocity = e.Velocity - (e.Position - d.jumppos):Normalized() * math.ceil(jumpposlen / 16)
			end
		end
		
		if e.State == NpcState.STATE_MOVE then
			s:Play("Hop", false)
			e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
			if s:IsEventTriggered("Jump") then
				d.jumppos = room:FindFreeTilePosition(e.Position - ((e.Position - e.Target.Position) / 2):Rotated(CommRedux.rand(-45, 45, e:GetDropRNG())), 160)
			end
			if s:IsEventTriggered("Land") then
				sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1, 0)
			end
			if s:WasEventTriggered("Land") then
				e:MultiplyFriction(0.7)
				e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
				local jumpposlen = (e.Position - d.jumppos):Length()
				if jumpposlen > 8 then
					e.Velocity = e.Velocity - (e.Position - d.jumppos):Normalized() * math.ceil(jumpposlen / 16)
				end
			else
				e:MultiplyFriction(0.8)
				local jumpposlen = (e.Position - d.jumppos):Length()
				if s:WasEventTriggered("Jump") and not s:WasEventTriggered("Land") then
					e.Velocity = e.Velocity - ((e.Position - d.jumppos):Resized(math.ceil(jumpposlen / 128)) * math.ceil(math.abs(s:GetFrame() - 13) / 2))
				end
			end
			if s:IsFinished() then
				e.State = NpcState.STATE_IDLE
			end
		end
		
		if e.State == NpcState.STATE_ATTACK then
			s:Play("Fire", false)
			e:MultiplyFriction(0.7)
			local jumpposlen = (e.Position - d.jumppos):Length()
			if jumpposlen > 8 and s:GetAnimation() == "Idle" then
				e.Velocity = e.Velocity - (e.Position - d.jumppos):Normalized() * math.ceil(jumpposlen / 16)
			end
			if s:IsEventTriggered("Shoot") then
				local params = ProjectileParams()
				params.HeightModifier = 8
				sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1, 0)
				params.Scale = 1.75
				e:FireProjectiles(e.Position, (e.Target.Position - e.Position):Normalized() * 8, 0, params)
				local fx = Isaac.Spawn(1000, 2, 5, e.Position, Vector.Zero, e):ToEffect()
				fx.RenderZOffset = 2
				fx.SpriteOffset = Vector(0, - 6)
				fx.Color = Color(1, 1, 1, 1, 0, 0, 0)
			end
			if s:IsFinished() then
				e.State = NpcState.STATE_IDLE
			end
		end
	end
	
	if e.Variant == 1 then
		e.Target = e:GetPlayerTarget()
		local room = game:GetRoom()
		local s = e:GetSprite()
		local d = e:GetData()
		
		if e.State == NpcState.STATE_INIT then
			e.State = NpcState.STATE_APPEAR
		end
		if e.State == NpcState.STATE_APPEAR then
			e.State = NpcState.STATE_IDLE
		end
		if e.State == NpcState.STATE_IDLE then
			s:Play("Idle", false)
			e:MultiplyFriction(0.7)
			if (e.Position - e.Target.Position):Length() > 148 then
				e.State = NpcState.STATE_MOVE
			else
				e.State = NpcState.STATE_ATTACK
			end
			
			if not d.jumppos then d.jumppos = e.Position end
			local jumpposlen = (e.Position - d.jumppos):Length()
			if jumpposlen > 8 and s:GetAnimation() == "Idle" then
				--e.Velocity = e.Velocity - (e.Position - d.jumppos):Normalized() * math.ceil(jumpposlen / 16)
			end
		end
		
		if e.State == NpcState.STATE_MOVE then
			s:Play("Hop", false)
			e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
			if s:IsEventTriggered("Jump") then
				d.jumppos = room:FindFreeTilePosition(e.Position - ((e.Position - e.Target.Position) / 2):Rotated(CommRedux.rand(-45, 45, e:GetDropRNG())), 160)
			end
			if s:IsEventTriggered("Land") then
				sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1, 0)
			end
			if not s:WasEventTriggered("Land") then
				e:MultiplyFriction(0.7)
				local jumpposlen = (e.Position - d.jumppos):Length()
				if jumpposlen > 8 then
					e.Velocity = e.Velocity - (e.Position - d.jumppos):Normalized() * math.ceil(jumpposlen / 16)
				end
			else
				e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
				e:MultiplyFriction(0.8)
				local jumpposlen = (e.Position - d.jumppos):Length()
				if s:WasEventTriggered("Jump") and not s:WasEventTriggered("Land") then
					e.Velocity = e.Velocity - ((e.Position - d.jumppos):Resized(math.ceil(jumpposlen / 128)) * math.ceil(math.abs(s:GetFrame() - 13) / 2))
				end
			end
			if s:IsFinished() then
				e.State = NpcState.STATE_IDLE
			end
		end
		
		if e.State == NpcState.STATE_ATTACK then
			s:Play("Leap", false)
			e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
			if s:IsEventTriggered("Jump") then
				d.jumppos = room:FindFreeTilePosition(e.Target.Position, 160)
				e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
			end
			if s:IsEventTriggered("Land") then
				e.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
				sfx:Play(SoundEffect.SOUND_MEAT_IMPACTS, 1, 0, false, 1, 0)
				local prjs = CommRedux.shootSpreadProjectiles(e, e.Position, Vector(1, 0):Resized(10), 360, 6)
			end
			if not s:WasEventTriggered("Land") then
				e:MultiplyFriction(0.7)
				local jumpposlen = (e.Position - d.jumppos):Length()
				if jumpposlen > 8 then
					e.Velocity = e.Velocity - (e.Position - d.jumppos):Normalized() * math.ceil(jumpposlen / 16)
				end
			else
				e.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
				e:MultiplyFriction(0.8)
				local jumpposlen = (e.Position - d.jumppos):Length()
				if s:WasEventTriggered("Jump") and not s:WasEventTriggered("Land") then
					e.Velocity = e.Velocity - ((e.Position - d.jumppos):Resized(math.ceil(jumpposlen / 128)) * math.ceil(math.abs(s:GetFrame() - 13) / 2))
				end
			end
			if s:IsFinished() then
				e.State = NpcState.STATE_IDLE
			end
		end
	end
end, EntityType.ENTITY_TEKTRITE)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == ENTITYVARIANT_CHERUB then
		local s = e:GetSprite()
		e:AddEntityFlags(EntityFlag.FLAG_PERSISTENT | EntityFlag.FLAG_NO_TARGET | EntityFlag.FLAG_NO_STATUS_EFFECTS)
		if not CommRedux.sd.cherubcount then CommRedux.sd.cherubcount = 0 end
		CommRedux.sd.cherubcount = CommRedux.sd.cherubcount + 1
		s:Play("Appear", true)
	end
end, EntityType.ENTITY_CHERUB)

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	if e.Variant == ENTITYVARIANT_CHERUB then
		local room = game:GetRoom()
		e.Target = e:GetPlayerTarget()
		local s = e:GetSprite()
		local d = e:GetData()
		e:MultiplyFriction(0.9)
		if s:IsFinished() and s:GetAnimation() == "Appear" then
			e.State = NpcState.STATE_MOVE
			s:Play("Move")
		end
		if e.State == NpcState.STATE_MOVE or e.State == NpcState.STATE_ATTACK and (e.Position - Isaac.GetFreeNearPosition(e.Target.Position + (e.Target:ToPlayer():GetShootingJoystick():Normalized()*80), 0)):Length() > 32 then
			e.Velocity = e.Velocity - (e.Position - Isaac.GetFreeNearPosition(e.Target.Position + (e.Target:ToPlayer():GetShootingJoystick():Normalized()*80), 0)):Normalized() / 8
		end
		if e.State == NpcState.STATE_MOVE and (e.Position - e.Target.Position):Length() <= 96 then
			e.State = NpcState.STATE_ATTACK
			s:Play("Attack")
		end
		if e.State == NpcState.STATE_ATTACK and s:IsFinished() and s:GetAnimation() == "Attack" then
			e.State = NpcState.STATE_MOVE
			s:Play("Move")
		end
		if s:IsEventTriggered("Shoot") then
			local prj = Isaac.Spawn(9, 0, 0, e.Position, (e.Target.Position - e.Position):Normalized() * 6, e):ToProjectile()
			prj:AddProjectileFlags(ProjectileFlags.HIT_ENEMIES)
			prj.Color = Color(1, 1, 1, 1, 0, 0, 0)
			prj.CollisionDamage = 1
			prj.FallingAccel = 0.5
			prj.Height = -32
			prj.FallingSpeed = -6
			sfx:Play(SoundEffect.SOUND_BLOODSHOOT, 1, 0, false, 1, 0)
			
			local fx = Isaac.Spawn(1000, 2, 5, e.Position, Vector.Zero, e):ToEffect()
			fx.RenderZOffset = 2
			fx.SpriteOffset = Vector(0, - 16)
			fx.Color = Color(1, 1, 1, 1, 0, 0, 0)
		end
	end
end, EntityType.ENTITY_CHERUB)

local function entityNewRoom()
	local room = game:GetRoom()
	for _, e in ipairs(Isaac.GetRoomEntities()) do
		if e.Type == EntityType.ENTITY_CHERUB and e.Variant == ENTITYVARIANT_CHERUB then
			e = e:ToNPC()
			local s = e:GetSprite()
			s:Play("Appear", true)
			e.State = NpcState.STATE_APPEAR
			local pos = room:GetRandomPosition(1)
			while (pos - e.Target.Position):Length() < 128 do
				pos = room:GetRandomPosition(1)
			end
			e.Position = pos
		end
	end
end
CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, entityNewRoom)
CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, entityNewRoom)

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	e:MultiplyFriction(0.8)
end, EntityType.ENTITY_STONEY)



CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	if e.Variant == 0 then
		local d = e:GetData()
		local s = e:GetSprite()
		if d.shopkeeperCRex == nil then d.shopkeeperCRex = false end
		
		if not d.shopkeeperCRseed then
			d.shopkeeperCRseed = CommRedux.rand(1, 19, e:GetDropRNG())
		end
		
		d.shopkeeperCRanim = "Shopkeeper " .. d.shopkeeperCRseed
		if d.shopkeeperCRex == true then d.shopkeeperCRanim = d.shopkeeperCRanim .. " ex" end
		
		if d.shopkeeperCRanim then
			s:Play(d.shopkeeperCRanim)
		end
	end
end, EntityType.ENTITY_SHOPKEEPER)

CommRedux:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, e)
	if e.Variant == 0 then	
		local d = e:GetData()
		local s = e:GetSprite()
		
		d.shopkeeperCRanim = "Shopkeeper " .. d.shopkeeperCRseed
		if d.shopkeeperCRex == true then d.shopkeeperCRanim = d.shopkeeperCRanim .. " ex" end
		
		if d.shopkeeperCRanim then
			s:Play(d.shopkeeperCRanim)
		end
		
		if e:IsDead() and d.shopkeeperCRseed then
			if CommRedux.HasShopkeeperFunc(d.shopkeeperCRseed) then
				local func = CommRedux.GetShopkeeperFunc(d.shopkeeperCRseed)
				local ranfunc = func(e)
				return ranfunc
			else
				return true
			end
		end
	end
end, EntityType.ENTITY_SHOPKEEPER)

CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	if e.Variant == 0 then
		local d = e:GetData()
		local s = e:GetSprite()
	end
end, EntityType.ENTITY_SHOPKEEPER)




CommRedux.AddShopkeeperFunc(12, function(shopkeeper)
	local e = shopkeeper
	
	Isaac.Spawn(5, 40, 1, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), newshopkeeper)
	Isaac.Spawn(5, 40, BombSubType.BOMB_DOUBLEPACK, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	return false
end)

CommRedux.AddShopkeeperFunc(13, function(shopkeeper)
	local e = shopkeeper
	for i = 1, CommRedux.rand(3, 5, shopkeeper:GetDropRNG()) do
		Isaac.Spawn(5, PickupVariant.PICKUP_COIN, 0, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	end
	return false
end)

CommRedux.AddShopkeeperFunc(14, function(shopkeeper)
	local e = shopkeeper
	Isaac.Spawn(5, PickupVariant.PICKUP_HEART, HeartSubType.HEART_SOUL, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	return false
end)

CommRedux.AddShopkeeperFunc(15, function(shopkeeper)
	local e = shopkeeper
	Isaac.Spawn(5, PickupVariant.PICKUP_TAROTCARD, 0, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	return false
end)

CommRedux.AddShopkeeperFunc(16, function(shopkeeper)
	local e = shopkeeper
	Isaac.Spawn(5, 40, BombSubType.BOMB_TROLL, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	return false
end)

CommRedux.AddShopkeeperFunc(17, function(shopkeeper)
	local e = shopkeeper
	for i = 1, CommRedux.rand(1, 2, e:GetDropRNG()) do
		Isaac.Spawn(5, PickupVariant.PICKUP_PILL, 0, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	end
	return false
end)

CommRedux.AddShopkeeperFunc(18, function(shopkeeper)
	local e = shopkeeper
	Isaac.Spawn(5, PickupVariant.PICKUP_HEART, HeartSubType.HEART_ETERNAL, shopkeeper.Position, (Vector(1, 0):Rotated(CommRedux.rand(0, 360, e:GetDropRNG()))):Resized(CommRedux.rand_float(2, 3, e:GetDropRNG())), shopkeeper)
	return false
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_INIT, function(_, e)
	e:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
	Isaac.Spawn(e.Variant, e.SubType, 0, e.Position, e.Velocity, nil)
	e:Remove()
end, 705)


CommRedux:AddCallback(ModCallbacks.MC_PRE_ROOM_ENTITY_SPAWN, function(_, id, variant, subtype, grid, seed)
	local grng = RNG()
	grng:SetSeed(seed, 1)
	if id == 27 then
        if variant < 2 and subtype == 0 then
            if variant == 1 or grng:RandomInt(10) == 0 then
                return {27, 1, 0, grid, seed}
            else
                return {27, 0, 0, grid, seed}
            end
		end
	end
end)