if EID and CommRedux then
-- English stuff
	-- Items
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE, "Using switches between 2 states that affect various stats#Red: Damage up, Range up#White: Tears up, Speed up, Luck up", "The Apple", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_ORIGINALSIN, "Something", "Original Sin", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_COOLBEAN, "Spawns a fart that freezes enemies in its radius", "Cool Bean", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_SHAMPOO, "Gives a small tears up upon use#Tears up resets on new room", "Shampoo", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_BOXOFWIRES, "Gives electric tears that zap nearby enemies", "Box of Wires", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_ECTOPLASM, "Gives haunted tears (todo: write more)", "Ectoplasm", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_GEODE, "Spawns 1 Rune or Soul Stone", "Geode", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_COUNTERFEITDOLLAR, "+99 Coins#Everytime you take damage, you lose between 3-4 coins#You drop some of these coins and you can recover them", "Counterfeit Dollar", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MAXSHEAD, "↑ +15% Damage Multiplier#↑ +0.3 Speed up", "Max's Head", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_CANTRIPBAG, "Spawns 1 Four Souls card after beating a boss or miniboss room", "Cantrip Bag", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_STRAWBERRYMILK, "Gives you shotgun tears that shoot up to 5 tears every time you fire", "Strawberry Milk", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1], "Upon tapping the use item button, you swap between 3 versions of this item#Holding the use item button will give you the pickup associated with the symbol on the item.", "Phlebectomy", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2], "Upon tapping the use item button, you swap between 3 versions of this item#Holding the use item button will give you the pickup associated with the symbol on the item.", "Phlebectomy", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3], "Upon tapping the use item button, you swap between 3 versions of this item#Holding the use item button will give you the pickup associated with the symbol on the item.", "Phlebectomy", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4], "Upon tapping the use item button, you swap between 3 versions of this item#Holding the use item button will give you the pickup associated with the symbol on the item.", "Phlebectomy", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_DECKBUILDER, "Upon holding the use item button, you store the current active card in your pocket into an infinite storage#Tapping the use item button allows you to swap through cards in your storage", "Deck Builder", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_PILLCASE, "Upon holding the use item button, you store the current active pill in your pocket into an infinite storage#Tapping the use item button allows you to swap through pills in your storage", "Mom's pill case", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_D3, "Rerolls all trinkets in the current room", "D3", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_BLUEWAFFLE, "Tears up item that brings you to the tear cap, but tanks your accuracy.", "Blue Waffle", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_CURSED_D12, "Shuffles 'hexed' rooms on the floor.", "Cursed D12", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_FORBIDDENSEED, "Thrown active item, creates a forbidden crop at the location where it drops or collides that slows and slightly damages#Killing enemies on this crop drops crab apples which give you temporary stat upgrades", "Forbidden Seed", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_CRYOBOMBS, "Your bombs now freeze enemies.", "Cryobombs", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_CORDYCEPS, "Touching an enemy gives it a psuedo-permacharm effect that spreads to every other enemy it touches.", "Cordyceps", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MAXSPAW, "Converts red hearts into damage.", "Max's Paw", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MAXSTAIL, "Your damage stat now scales with your speed. the damage boost cannot go lower than 1.", "Max's Tail", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_TAMMYSPAW, "Converts red hearts into a variety of pickups.", "Tammy's Paw", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_TAMMYSTAIL, "Killing enemies grants a decaying tears up", "Tammy's Tail", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_BOWLOBEANS, "Random farts!!!", "Bowl o' Beans", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_BLOODYFEATHER, "Upon taking damage, activates a crack the sky effect.", "Bloody Feather", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_TACO, "TEH EPIC TACO IZ AN ALL STATS UP X3", "Taco", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_HOTBEAN, "Farts and damages all enemies in a small radius and spawns a red candle fire at your position.", "Hot Bean", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MYSTERYMEAT, "Grants 1 red heart and a random stat upgrade.", "Mystery Meat", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MALIGNMUSH, "An 'all stats up' made to mock Magic Mush. It upgrades all stats that Magic Mush doesn't and reduces your size.", "Malign Mush", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_BOOKOFGRIEF, "Gives you a flat +1 tears up and a +5 luck up.", "The Book of Sorrows", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MORTALCOIL, "description pending", "Mortal Coil", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_SNAKEEYES, "Rerolls items into devil deals.", "Snake Eyes", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_LOSTIDOL, "description pending", "Lost Idol", "en_us")
		--thehive
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_POPCOIN, "Converts 7 coins into a heart container.", "Popcoin", "en_us")
		--bhs
		--mr snappy
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_OCCAMSRAZOR, "Upon use, gives 1 broken heart and an all stats up.", "Occam's Razor", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_VOODOOPIN, "When taking damage, has a chance to nullify it, but you still proc your 'on damage' effects.", "Voodoo Pin", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_GUMMYBEAR, "All stats up", "Gummy Bear", "en_us")
		-- white candle
		--conqueror baby
		--ed bathwater
		--lawndart
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_POTTY, "Spawns a few friendly dips.", "Potty", "en_us")
		--fishhook
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MAXSBONE, "As long as you hold it above your head, you gain temporary damage boosts, but youre slow while doing so. Putting it down allows the damage boost to decrease.", "Max's Bone", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_MAXSBALL, "Damage and tears up", "Max's Ball", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX, "Generates pickups upon use, refills upon stage transition", "Lunchbox", "en_us")
		EID:addCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX_EMPTY, "Generates pickups upon use, refills upon stage transition", "Lunchbox", "en_us")
		
		
		
		
		
		-- Pendiente: A partir de The Hive
	-- Trinkets
		EID:addTrinket(TrinketType.TRINKET_CR_TOASTART, "To be determined", "Toastart", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_INSANEPENDANT, "Deal double damage, but also take double damage", "Insane Pendant", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_PUTRIDGLAND, "Creep can damage airborne enemies", "Putrid Gland", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_FIGLEAF, "You can no longer lose devil deal chance.#Damocles cannot be triggered while holding this trinket, but it wont protect you from prior triggers#Perfection cannot be lost", "Fig Leaf", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_PINWORM, "Tears now travel in an arc", "Pin Worm", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_SUPPOSITORY, "Taking pills spawns friendly dips", "Suppository", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_PAINPILLS, "Taking pills heals a full red heart. generates half soul hearts if at full hp.", "Pain Pills", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_SHRUNKENHEAD, "Hexed rooms will generate more frequently on future floors.", "Shrunken Head", "en_us")
		EID:addTrinket(TrinketType.TRINKET_CR_ICHOR, "Regenerates half soul and black hearts upon clearing rooms", "Ichor", "en_us")
	-- Cards
		-- Lost Cards
			EID:addCard(Card.CARD_CR_COLD, "For every enemy in the room, has a 50% chance to freeze", "I - The Cold", "en_us")
			EID:addCard(Card.CARD_CR_SERVANT, "Spawns 4-6 Minisaacs", "II - The Servant", "en_us")
			EID:addCard(Card.CARD_CR_WISDOM, "Teleports you to the Super Secret room", "III - Wisdom", "en_us")
			EID:addCard(Card.CARD_CR_REPENTANCE, "Gives Mom's Knife for the current room", "IV - Repentance", "en_us")
			EID:addCard(Card.CARD_CR_ETERNITY, "Gives you 1 eternal heart and 1 heart container.", "V - Eternity", "en_us")
			EID:addCard(Card.CARD_CR_CORRUPTION, "Teleports you to the I AM ERROR room", "VI - Corruption", "en_us")
			EID:addCard(Card.CARD_CR_IMMOLATION, "Teleports you to the Curse room or the Sacrifice room", "VII - Immolation", "en_us")
			EID:addCard(Card.CARD_CR_WORSHIP, "Has 50/50 chance to either damage you for 2 full hearts or give 	you a random collectible", "VIII - Worship", "en_us")
			EID:addCard(Card.CARD_CR_HEX, "Teleports you to a hexed room#if there are none on the floor, create one where youre standing", "IX - Hex", "en_us")
			EID:addCard(Card.CARD_CR_DISSENSION, "Grants brimstone for 1 room.", "X - Dissension", "en_us")
			EID:addCard(Card.CARD_CR_DAMNED, "Grants 10 bone orbitals.", "XI - The Damned", "en_us")
			EID:addCard(Card.CARD_CR_OCCULT, "Rerolls items into devil deals.", "XII - Occult", "en_us")
			EID:addCard(Card.CARD_CR_SCHOLAR, "Teleports you to the library. if there are no library rooms on the floor, activates a random book.", "XIII - The Scholar", "en_us")
		-- Four souls cards
			EID:addCard(Card.CARD_CR_TREASURE, "Gives you 1 random collectible for the current room", "Treasure Card", "en_us")
			EID:addCard(Card.CARD_CR_LOOT, "Spawns 3 random pickups", "Loot Card", "en_us")
			EID:addCard(Card.CARD_CR_MONSTER, "Spawns a random charmed enemy", "Monster Card", "en_us")
			EID:addCard(Card.CARD_CR_SOUL, "Spawns a lost soul buddy to follow you around", "Bonus Soul", "en_us")
-- English stuff
end

if ClickerAPI and CommRedux then
	ClickerAPI.AddCharacter(PlayerType.PLAYER_CR_ADAM, false, true)
	ClickerAPI.AddCharacter(PlayerType.PLAYER_CR_ABEL, false, true)
end