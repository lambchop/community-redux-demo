local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG
local selfpath = "resources/scripts/commredux/"

local scriptlist = {
	"items/main",
	"players/main",
	"hazards",
	"monsters/main",
	"eid"
}

CBLib.InitializeMod(CommRedux)

CommRedux.ModCallbacks = {}

CommRedux.ModCallbacks.MC_HOLD_ITEM = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_RELEASE = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_TAP = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_POST_HAZARD_REINIT = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_POST_ITEM_GET = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_PLAYER_FIRST_INIT = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_PLAYER_LOAD = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_PLAYER_UPDATE = CBLib.AddCustomCallback(CommRedux)

-- ONLY USE PRE AND POST SPARINGLY. THEY ARE FOR PRIORITY'S SAKE
CommRedux.ModCallbacks.MC_PRE_PLAYER_FIRE_TEAR = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_PLAYER_FIRE_TEAR = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_POST_PLAYER_FIRE_TEAR = CBLib.AddCustomCallback(CommRedux)

CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_RUN_START = CBLib.AddCustomCallback(CommRedux)
CommRedux.ModCallbacks.MC_RUN_CONTINUE = CBLib.AddCustomCallback(CommRedux)

CommRedux.ItemTracker = {}

CollectibleType.COLLECTIBLE_CR_THEAPPLE = Isaac.GetItemIdByName("The Apple")
CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE = Isaac.GetItemIdByName("       The Apple")
CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED = Isaac.GetItemIdByName(" The Apple")
CollectibleType.COLLECTIBLE_CR_ORIGINALSIN = Isaac.GetItemIdByName("Original Sin")
CollectibleType.COLLECTIBLE_CR_COOLBEAN = Isaac.GetItemIdByName("Cool Bean")
CollectibleType.COLLECTIBLE_CR_SHAMPOO = Isaac.GetItemIdByName("Shampoo")
CollectibleType.COLLECTIBLE_CR_BOXOFWIRES = Isaac.GetItemIdByName("Box of Wires")
CollectibleType.COLLECTIBLE_CR_ECTOPLASM = Isaac.GetItemIdByName("Ectoplasm")
CollectibleType.COLLECTIBLE_CR_GEODE = Isaac.GetItemIdByName("Geode")
CollectibleType.COLLECTIBLE_CR_COUNTERFEITDOLLAR = Isaac.GetItemIdByName("Counterfeit Dollar")
CollectibleType.COLLECTIBLE_CR_MAXSHEAD = Isaac.GetItemIdByName("Max's Head")
CollectibleType.COLLECTIBLE_CR_CANTRIPBAG = Isaac.GetItemIdByName("Cantrip Bag")
CollectibleType.COLLECTIBLE_CR_STRAWBERRYMILK = Isaac.GetItemIdByName("Strawberry Milk")
CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY = {
	Isaac.GetItemIdByName("Phlebectomy"),
	Isaac.GetItemIdByName("Phlebectomy "),
	Isaac.GetItemIdByName(" Phlebectomy"),
	Isaac.GetItemIdByName("  Phlebectomy")
}

CollectibleType.COLLECTIBLE_CR_DECKBUILDER = Isaac.GetItemIdByName("Deck Builder")
CollectibleType.COLLECTIBLE_CR_DECKBUILDER_OPEN = Isaac.GetItemIdByName("Open Deck Builder")
CollectibleType.COLLECTIBLE_CR_PILLCASE = Isaac.GetItemIdByName("Mom's Pill Case")
CollectibleType.COLLECTIBLE_CR_D3 = Isaac.GetItemIdByName("D3")
CollectibleType.COLLECTIBLE_CR_BLUEWAFFLE = Isaac.GetItemIdByName("Blue Waffle")
CollectibleType.COLLECTIBLE_CR_CURSED_D12 = Isaac.GetItemIdByName("Cursed D12")
CollectibleType.COLLECTIBLE_CR_FORBIDDENSEED = Isaac.GetItemIdByName("Forbidden Seed")
CollectibleType.COLLECTIBLE_CR_CRYOBOMBS = Isaac.GetItemIdByName("Cryobombs")
CollectibleType.COLLECTIBLE_CR_CORDYCEPS = Isaac.GetItemIdByName("Cordyceps")
CollectibleType.COLLECTIBLE_CR_MAXSPAW = Isaac.GetItemIdByName("Max's Paw")
CollectibleType.COLLECTIBLE_CR_MAXSTAIL = Isaac.GetItemIdByName("Max's Tail")
CollectibleType.COLLECTIBLE_CR_TAMMYSPAW = Isaac.GetItemIdByName("Tammy's Paw")
CollectibleType.COLLECTIBLE_CR_TAMMYSTAIL = Isaac.GetItemIdByName("Tammy's Tail")
CollectibleType.COLLECTIBLE_CR_BOWLOBEANS = Isaac.GetItemIdByName("Bowl o' Beans")
CollectibleType.COLLECTIBLE_CR_BLOODYFEATHER = Isaac.GetItemIdByName("Bloody Feather")
CollectibleType.COLLECTIBLE_CR_TACO = Isaac.GetItemIdByName("Taco")
CollectibleType.COLLECTIBLE_CR_HOTBEAN = Isaac.GetItemIdByName("Hot Bean")
CollectibleType.COLLECTIBLE_CR_MYSTERYMEAT = Isaac.GetItemIdByName("Mystery Meat")
CollectibleType.COLLECTIBLE_CR_MALIGNMUSH = Isaac.GetItemIdByName("Malign Mush")
CollectibleType.COLLECTIBLE_CR_BOOKOFGRIEF = Isaac.GetItemIdByName("The Book of Sorrows")
CollectibleType.COLLECTIBLE_CR_MORTALCOIL = Isaac.GetItemIdByName("Mortal Coil")
CollectibleType.COLLECTIBLE_CR_SNAKEEYES = Isaac.GetItemIdByName("Snake Eyes")
CollectibleType.COLLECTIBLE_CR_LOSTIDOL = Isaac.GetItemIdByName("Lost Idol")

CollectibleType.COLLECTIBLE_CR_THEHIVE = Isaac.GetItemIdByName("The Hive")
CollectibleType.COLLECTIBLE_CR_POPCOIN = Isaac.GetItemIdByName("Popcoin")
CollectibleType.COLLECTIBLE_CR_BHS = Isaac.GetItemIdByName("</3")
CollectibleType.COLLECTIBLE_CR_MRSNAPPY = Isaac.GetItemIdByName("Mr. Snappy")
CollectibleType.COLLECTIBLE_CR_OCCAMSRAZOR = Isaac.GetItemIdByName("Occam's Razor")
CollectibleType.COLLECTIBLE_CR_VOODOOPIN = Isaac.GetItemIdByName("Voodoo Pin")
CollectibleType.COLLECTIBLE_CR_GUMMYBEAR = Isaac.GetItemIdByName("Gummy Bear")
CollectibleType.COLLECTIBLE_CR_WHITECANDLE = Isaac.GetItemIdByName("White Candle")
CollectibleType.COLLECTIBLE_CR_CONQUERORBABY = Isaac.GetItemIdByName("Conqueror Baby")
CollectibleType.COLLECTIBLE_CR_EDBATHWATER = Isaac.GetItemIdByName("Bathwater")
CollectibleType.COLLECTIBLE_CR_LAWNDART = Isaac.GetItemIdByName("Lawn Dart")
CollectibleType.COLLECTIBLE_CR_POTTY = Isaac.GetItemIdByName("Potty")
CollectibleType.COLLECTIBLE_CR_FISHHOOK = Isaac.GetItemIdByName("Fish Hook")
CollectibleType.COLLECTIBLE_CR_MAXSBONE = Isaac.GetItemIdByName("Max's Bone")
CollectibleType.COLLECTIBLE_CR_MAXSBALL = Isaac.GetItemIdByName("Max's Ball")
CollectibleType.COLLECTIBLE_CR_LUNCHBOX = Isaac.GetItemIdByName("Lunchbox")
CollectibleType.COLLECTIBLE_CR_LUNCHBOX_EMPTY = Isaac.GetItemIdByName("Lunchbox ")
CollectibleType.COLLECTIBLE_CR_WATER = Isaac.GetItemIdByName("Glass of Water")
CollectibleType.COLLECTIBLE_CR_COUSINJOEY = Isaac.GetItemIdByName("Cousin Joey")

--transformation metadata
TRANSFORMATION_METADATA = {}

function CommRedux.AddItemToTransformation(transformation, itemid, itemtype)
	if not TRANSFORMATION_METADATA[transformation] then TRANSFORMATION_METADATA[transformation] = {} end
	TRANSFORMATION_METADATA[transformation][#TRANSFORMATION_METADATA[transformation] + 1] = {itemid, itemtype}
	local realitemtype = 0
	if itemtype == "collectible" then realitemtype = 1 end
	if itemtype == "trinket" then realitemtype = 2 end
	if CommRedux.ItemTracker[realitemtype] == nil then CommRedux.ItemTracker[realitemtype] = {} end
	CommRedux.ItemTracker[realitemtype][itemid] = true
end

--transformations
if not Transformation then Transformation = {} end

Transformation.CR_MAX = Isaac.GetItemIdByName("Max!")
CommRedux.AddItemToTransformation(Transformation.CR_MAX, CollectibleType.COLLECTIBLE_CR_MAXSHEAD, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_MAX, CollectibleType.COLLECTIBLE_CR_MAXSPAW, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_MAX, CollectibleType.COLLECTIBLE_CR_MAXSTAIL, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_MAX, CollectibleType.COLLECTIBLE_DOG_TOOTH, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_MAX, CollectibleType.COLLECTIBLE_CR_MAXSBONE, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_MAX, CollectibleType.COLLECTIBLE_CR_MAXSBALL, "collectible")

Transformation.CR_TAMMY = Isaac.GetItemIdByName("Tammy!")
CommRedux.AddItemToTransformation(Transformation.CR_TAMMY, CollectibleType.COLLECTIBLE_TAMMYS_HEAD, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_TAMMY, CollectibleType.COLLECTIBLE_CR_TAMMYSPAW, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_TAMMY, CollectibleType.COLLECTIBLE_CR_TAMMYSTAIL, "collectible")

Transformation.CR_LOKI = Isaac.GetItemIdByName("Trickster!")
CommRedux.AddItemToTransformation(Transformation.CR_LOKI, CollectibleType.COLLECTIBLE_LOKIS_HORNS, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_LOKI, CollectibleType.COLLECTIBLE_MARK, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_LOKI, CollectibleType.COLLECTIBLE_LIL_LOKI, "collectible")
CommRedux.AddItemToTransformation(Transformation.CR_LOKI, TrinketType.TRINKET_NUMBER_MAGNET, "trinket")


if not ShopkeeperSkin then ShopkeeperSkin = {} end
ShopkeeperSkin.SHOPKEEPER_DEFAULT = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
ShopkeeperSkin.SHOPKEEPER_AWESOME = 12
ShopkeeperSkin.SHOPKEEPER_FAT = 13
ShopkeeperSkin.SHOPKEEPER_GHOST = 14
ShopkeeperSkin.SHOPKEEPER_EVIL = 15

if not ShopkeeperSkinDrops then ShopkeeperSkinDrops = {} end

function CommRedux.AddShopkeeperFunc(keeperskin, func)
	ShopkeeperSkinDrops[keeperskin] = func
end

function CommRedux.HasShopkeeperFunc(keeperskin)
	if ShopkeeperSkinDrops[keeperskin] then
		return true
	else
		return false
	end
end

function CommRedux.GetShopkeeperFunc(keeperskin)
	return ShopkeeperSkinDrops[keeperskin]
end

TrinketType.TRINKET_CR_TOASTART = Isaac.GetTrinketIdByName("Toastart")
TrinketType.TRINKET_CR_INSANEPENDANT = Isaac.GetTrinketIdByName("Insane Pendant")
TrinketType.TRINKET_CR_PUTRIDGLAND = Isaac.GetTrinketIdByName("Putrid Gland")
TrinketType.TRINKET_CR_FIGLEAF = Isaac.GetTrinketIdByName("Fig Leaf")
TrinketType.TRINKET_CR_PINWORM = Isaac.GetTrinketIdByName("Pin Worm")
TrinketType.TRINKET_CR_SUPPOSITORY = Isaac.GetTrinketIdByName("Suppository")
TrinketType.TRINKET_CR_PAINPILLS = Isaac.GetTrinketIdByName("Pain Pills")
TrinketType.TRINKET_CR_SHRUNKENHEAD = Isaac.GetTrinketIdByName("Shrunken Head")
TrinketType.TRINKET_CR_ICHOR = Isaac.GetTrinketIdByName("Ichor")

--Lost Cards
Card.CARD_CR_COLD = Isaac.GetCardIdByName("I-TheCold")
Card.CARD_CR_SERVANT = Isaac.GetCardIdByName("II-TheServant")
Card.CARD_CR_WISDOM = Isaac.GetCardIdByName("III-Wisdom")
Card.CARD_CR_REPENTANCE = Isaac.GetCardIdByName("IV-Repentance")
Card.CARD_CR_ETERNITY = Isaac.GetCardIdByName("V-Eternity")
Card.CARD_CR_CORRUPTION = Isaac.GetCardIdByName("VI-Corruption")
Card.CARD_CR_IMMOLATION = Isaac.GetCardIdByName("VII-Immolation")
Card.CARD_CR_WORSHIP = Isaac.GetCardIdByName("VIII-Worship")
Card.CARD_CR_HEX = Isaac.GetCardIdByName("IX-Hex")
Card.CARD_CR_DISSENSION = Isaac.GetCardIdByName("X-Dissension")
Card.CARD_CR_DAMNED = Isaac.GetCardIdByName("XI-TheDamned")
Card.CARD_CR_OCCULT = Isaac.GetCardIdByName("XII-Occult")
Card.CARD_CR_SCHOLAR = Isaac.GetCardIdByName("XIII-TheScholar")

--Four Souls Cards
Card.CARD_CR_TREASURE = Isaac.GetCardIdByName("Treasure")
Card.CARD_CR_LOOT = Isaac.GetCardIdByName("Loot")
Card.CARD_CR_MONSTER = Isaac.GetCardIdByName("Monster")
Card.CARD_CR_SOUL = Isaac.GetCardIdByName("Soul")

AppleFrames = {
	L = Isaac.GetItemIdByName("  The Apple"),
	ML = Isaac.GetItemIdByName("   The Apple"),
	M = Isaac.GetItemIdByName("    The Apple"),
	MR = Isaac.GetItemIdByName("     The Apple"),
	R = Isaac.GetItemIdByName("      The Apple")
}

SoundEffect.SOUND_CR_APPLEGULP = Isaac.GetSoundIdByName("Apple Gulp")
SoundEffect.SOUND_CR_ROTTENAPPLEGULP = Isaac.GetSoundIdByName("Rotten Apple Gulp")
SoundEffect.SOUND_CR_CORKPOP = Isaac.GetSoundIdByName("Cork Pop")
SoundEffect.SOUND_CR_HAUNTDMG = Isaac.GetSoundIdByName("Haunted Damage")
SoundEffect.SOUND_CR_LOSTTAROT = {
	COLD = Isaac.GetSoundIdByName("LCA The Cold"),
	SERVANT = Isaac.GetSoundIdByName("LCA The Servant"),
	WISDOM = Isaac.GetSoundIdByName("LCA Wisdom"),
	REPENTANCE = Isaac.GetSoundIdByName("LCA Repentance"),
	ETERNITY = Isaac.GetSoundIdByName("LCA Eternity"),
	CORRUPTION = Isaac.GetSoundIdByName("LCA Corruption"),
	IMMOLATION = Isaac.GetSoundIdByName("LCA Immolation"),
	WORSHIP = Isaac.GetSoundIdByName("LCA Worship")
}
SoundEffect.SOUND_CR_PILLBOXOPEN = Isaac.GetSoundIdByName("PillBox open")
SoundEffect.SOUND_CR_PILLBOXCLOSE = Isaac.GetSoundIdByName("PillBox close")

SoundEffect.SOUND_CR_TEARFIRE = Isaac.GetSoundIdByName("remix tear fire")
SoundEffect.SOUND_CR_TEARBLOCK = Isaac.GetSoundIdByName("remix tear block")
SoundEffect.SOUND_CR_TEARSPLAT = Isaac.GetSoundIdByName("remix tear splatter")
SoundEffect.SOUND_CR_MINIAPPLEEAT = Isaac.GetSoundIdByName("Mini Apple Chomp")

PlayerType.PLAYER_CR_ADAM = Isaac.GetPlayerTypeByName("Adam", false)
PlayerType.PLAYER_CR_ADAM_B = Isaac.GetPlayerTypeByName("Adam", true)
PlayerType.PLAYER_CR_ABEL = Isaac.GetPlayerTypeByName("Abel", false)
PlayerType.PLAYER_CR_ABEL_B = Isaac.GetPlayerTypeByName("Abel", true)
PlayerType.PLAYER_CR_ABEL_BRAIN = Isaac.GetPlayerTypeByName("Abel Brain", false)

NullItemID.ID_CR_ADAM = Isaac.GetCostumeIdByPath("gfx/characters/character_001cr_adamface.anm2")
NullItemID.ID_CR_ADAM_B = Isaac.GetCostumeIdByPath("gfx/characters/character_001cr_adam_b_hood.anm2")
NullItemID.ID_CR_APPLEBLOOD = Isaac.GetCostumeIdByPath("gfx/characters/costume_appleblood.anm2")
NullItemID.ID_CR_APPLETEARS = Isaac.GetCostumeIdByPath("gfx/characters/costume_appletears.anm2")
NullItemID.ID_CR_ABEL = Isaac.GetCostumeIdByPath("gfx/characters/character_002cr_abelhair.anm2")
NullItemID.ID_CR_ABEL_B = Isaac.GetCostumeIdByPath("gfx/characters/character_002cr_abel_b_hair.anm2")

EffectVariant.EFFECT_CR_PUTRID_BUBBLE = Isaac.GetEntityVariantByName("Putrid Bubble")

EntityType.ENTITY_CAST = Isaac.GetEntityTypeByName("Cast")
EntityType.ENTITY_TEKTRITE = Isaac.GetEntityTypeByName("Tektrite")
EntityType.ENTITY_CHERUB = Isaac.GetEntityTypeByName("Stone Cherub")

EntityType.ENTITY_CREDUX_ENEMY = 703
CREDUX_ENTITYVARIANT_SLAMBONE = Isaac.GetEntityVariantByName("Slambone")
CREDUX_ENTITYVARIANT_STU = Isaac.GetEntityVariantByName("Stu")
CREDUX_ENTITYVARIANT_SPIDERDEN = Isaac.GetEntityVariantByName("Spider Den")
CREDUX_ENTITYVARIANT_CHEW = Isaac.GetEntityVariantByName("Chew")

ENTITYVARIANT_BURROW = Isaac.GetEntityVariantByName("Burrow")
ENTITYVARIANT_FLYORGY = Isaac.GetEntityVariantByName("Fly Orgy")
ENTITYVARIANT_HURL = Isaac.GetEntityVariantByName("Hurl")
ENTITYVARIANT_HOLLOWCAST = Isaac.GetEntityVariantByName("Hollow Cast")
ENTITYVARIANT_MEGAPOOTER = Isaac.GetEntityVariantByName("Mega Pooter")
ENTITYVARIANT_CHERUB = Isaac.GetEntityVariantByName("Cherub")

FamiliarVariant.FAMILIAR_COUSIN_JOEY = Isaac.GetEntityVariantByName("Cousin Joey")
FamiliarVariant.FAMILIAR_CONQUEROR_BABY = Isaac.GetEntityVariantByName("Conqueror Baby")

PickupVariant.PICKUP_FORBIDDENCROP = Isaac.GetEntityVariantByName("Forbidden Crop")
PickupVariant.PICKUP_REDAPPLE = Isaac.GetEntityVariantByName("Small Red Apple")
PickupVariant.PICKUP_WHITEAPPLE = Isaac.GetEntityVariantByName("Small White Apple")

BombVariant.BOMB_ICE = Isaac.GetEntityVariantByName("Ice Bomb")

function CommRedux.AddToItemInit(func, itemid, itemtype)
	local realitemtype = 0
	if itemtype == "collectible" then realitemtype = 1 end
	if itemtype == "trinket" then realitemtype = 2 end
	if CommRedux.ItemTracker[realitemtype] == nil then CommRedux.ItemTracker[realitemtype] = {} end
	CommRedux.ItemTracker[realitemtype][itemid] = true
	CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_ITEM_GET, func)
end
--[[
CommRedux.AddToItemInit(function()
	print("item get")
end, 38, "collectible")
]]
function CommRedux.checkItem(p, id)
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	for i = 1, #CommRedux.sd.items[ps] do
		if CommRedux.sd.items[ps][i] == id then
			return true
		end
	end
end

function CommRedux.checkTrinket(p, id)
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	for i = 1, #CommRedux.sd.trinkets[ps] do
		if CommRedux.sd.trinkets[ps][i] == id then
			return true
		end
	end
end

function CommRedux.addplayer(p, ptype, cidx, isfake)
	if not ptype then ptype = 0 end
	if not p then p = Isaac.GetPlayer() end
	local lastpidx = game:GetNumPlayers() - 1
	
	if lastpidx >= 63 then return nil else
		Isaac.ExecuteCommand('addplayer ' .. ptype .. ' ' .. cidx)
		local _p = Isaac.GetPlayer(lastpidx + 1)
		local d = _p:GetData()
		if isfake then
			_p.Parent = p
			game:GetHUD():AssignPlayerHUDs()
		end
		return _p
	end
end

function CommRedux.getActivePlayers()
	local t = {}
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		if p.Visible then
			t[#t + 1] = p
		end
	end
	return t
end

function CommRedux.shootSpreadProjectiles(e, position, velocity, spreadDegree, numproj, projvar, projsub)
	local t = {}
	for i = 0, numproj - 1 do
		if not projvar then projvar = 0 end
		if not projsub then projsub = 0 end
		local prj = Isaac.Spawn(9, projvar, projsub, position, velocity:Rotated((spreadDegree/numproj)*i), e):ToProjectile()
		local degbetween = spreadDegree/numproj
		prj.Velocity = prj.Velocity:Rotated((-degbetween/2) * (numproj-1))
		t[#t + 1] = prj
	end
	return t
end


function CommRedux.doPlayerColl(e)
	local room = game:GetRoom()
	local level = game:GetLevel()
	local p = e.Parent:ToPlayer()
	local coll = (p.CanFly == true or room:GetType() == RoomType.ROOM_DUNGEON) and EntityGridCollisionClass.GRIDCOLL_WALLS or EntityGridCollisionClass.GRIDCOLL_GROUND
	local neardoor = false
	local dungeoncoll = false
	local xDir, yDir = e.Position.X, e.Position.Y
	
	for i = 0, (room:GetGridSize()) do
		local gent = room:GetGridEntity(i)
		if gent then
			if gent:GetType() == GridEntityType.GRID_DOOR then
				local Door = gent:ToDoor()
				if (e.Position - Door.Position):Length() <= 32 then
					if room:IsClear() then
						Door:TryUnlock(p)
					end
					if Door and (Door:IsOpen() and Door.TargetRoomIndex ~= GridRooms.ROOM_MIRROR_IDX or Door.TargetRoomIndex == GridRooms.ROOM_MIRROR_IDX and Door:GetVariant() ~= DoorVariant.DOOR_UNLOCKED and p:GetEffects():HasNullEffect(NullItemID.ID_LOST_CURSE)) then
						neardoor = true
						local doorLine = {x = nil, y = nil}
						if (Door.Direction == Direction.LEFT or Door.Direction == Direction.RIGHT) then
							if math.abs(e.Position.Y - Door.Position.Y) > 10 then
								yDir = Door.Position.Y + CommRedux.sign(e.Position.Y - Door.Position.Y) * 10
								lockY = CommRedux.sign(e.Position.Y - Door.Position.Y)
							end
							doorLine.x = Door.Position.X
						elseif (Door.Direction == Direction.DOWN or Door.Direction == Direction.UP) then
							if math.abs(e.Position.X - Door.Position.X) > 10 then
								xDir = Door.Position.X + CommRedux.sign(e.Position.X - Door.Position.X) * 10
								lockX = CommRedux.sign(e.Position.X - Door.Position.X)
							end
							doorLine.y = Door.Position.Y
						end
						if doorLine.x and math.abs(doorLine.x - e.Position.X) < 5 or doorLine.y and math.abs(doorLine.y - e.Position.Y) < 5 then
							local targetTransition = Door.TargetRoomIndex
							local roomTransitionAnim = RoomTransitionAnim.WALK
							local dimension = -1
							level.LeaveDoor = Door.Slot
							if Door.TargetRoomIndex == GridRooms.ROOM_MIRROR_IDX then
								dimension = not room:IsMirrorWorld() and 1 or 0
								roomTransitionAnim = RoomTransitionAnim.FADE_MIRROR
								targetTransition = level:GetCurrentRoomIndex()
							end
							if Door.TargetRoomIndex == -101 then
								targetTransition = 162
								dimension = 1
							end
							game:StartRoomTransition(targetTransition, Door.Direction, roomTransitionAnim, p, dimension)
						end
					end
				end
			elseif room:GetType() == RoomType.ROOM_DUNGEON and level:GetStage() ~= LevelStage.STAGE8 and (e.Position - gent.Position):Length() <= 35 then
				if gent:GetType() == GridEntityType.GRID_WALL then
					dungeoncollide = true
				end
				if gent.Position.Y <= 135 and gent:GetType() == 1 and gent:GetVariant() == 10 then
					p.Position = Vector(gent.Position.X,gent.Position.Y-20)
					p.Visible = false
				elseif gent.Position.X >= 595 and gent:GetType() == 19 and gent:GetVariant() == 0 then
					player.Position = Vector(gent.Position.X + 20,gent.Position.Y)
					p.Visible = false
				end
			end
		end
	end

	if neardoor == true then
		coll = GridCollisionClass.COLLISION_NONE
		e.Position = Vector(xDir, yDir)
	end
	if dungeoncoll == true then
		coll = EntityGridCollisionClass.GRIDCOLL_GROUND
	end
	e.GridCollisionClass = coll
end

function CommRedux.AngleToDirection(angleDegrees)
    if math.abs(angleDegrees) < 45 then
        return Direction.RIGHT
    elseif math.abs(angleDegrees) > 135 then
        return Direction.LEFT
    elseif angleDegrees> 0 then
        return Direction.DOWN
    elseif angleDegrees< 0 then
        return Direction.UP
    end
    
    return Direction.NO_DIRECTION
end

function CommRedux.AddTPS(p, n)
    return (30 / ( 30 / (p.MaxFireDelay + 1) + n ) - 1)
end


function CommRedux.GetTPS(p)
    return 30 / (p.MaxFireDelay + 1)
end


function CommRedux.GetDPS(p)
	return CommRedux.GetTPS(p) * p.Damage
end


function CommRedux.round(num, place)
	local n = (num * place - math.floor(num) * place) / place
	if (n*(place) - math.floor(n * (place))) < 0.5 then
		return math.floor(num*place) / place
	else
		return math.ceil(num*place) / place
	end
end


function CommRedux.rand(x, y, rng)
    if not y then
        y = x
        x = 1
    end
    if not rng then
        rng = RNG()
    end
    return (rng:RandomInt(y - x + 1)) + x
end

function CommRedux.rand_float(x, y, rng)
    if not y then
        y = x
        x = 0
    end
    x = x * 1000
    y = y * 1000
    if not rng then
        rng = RNG()
    end
    return math.floor((rng:RandomInt(y - x + 1)) + x) / 1000
end

function CommRedux.shiftdown(t, position)
	for i = position, #t - position do
		t[i] = t[i + 1]
		t[i + 1] = nil
	end
end

function CommRedux.forceBloodTear(tear)
	if tear.Variant == TearVariant.BLUE then
		tear:ChangeVariant(TearVariant.BLOOD)
	elseif tear.Variant == TearVariant.NAIL then
		tear:ChangeVariant(TearVariant.NAIL_BLOOD)
	elseif tear.Variant == TearVariant.GLAUCOMA then
		tear:ChangeVariant(TearVariant.GLAUCOMA_BLOOD)
	elseif tear.Variant == TearVariant.CUPID_BLUE then
		tear:ChangeVariant(TearVariant.CUPID_BLOOD)
	elseif tear.Variant == TearVariant.EYE then
		tear:ChangeVariant(TearVariant.EYE_BLOOD)
	elseif tear.Variant == TearVariant.PUPULA then
		tear:ChangeVariant(TearVariant.PUPULA_BLOOD)
	elseif tear.Variant == TearVariant.GODS_FLESH then
		tear:ChangeVariant(TearVariant.GODS_FLESH_BLOOD)
	end
end


function CommRedux.forceBlueTear(tear)
	if tear.Variant == TearVariant.BLOOD then
		tear:ChangeVariant(TearVariant.BLUE)
	elseif tear.Variant == TearVariant.NAIL_BLOOD then
		tear:ChangeVariant(TearVariant.NAIL)
	elseif tear.Variant == TearVariant.GLAUCOMA_BLOOD then
		tear:ChangeVariant(TearVariant.GLAUCOMA)
	elseif tear.Variant == TearVariant.CUPID_BLOOD then
		tear:ChangeVariant(TearVariant.CUPID_BLUE)
	elseif tear.Variant == TearVariant.EYE_BLOOD then
		tear:ChangeVariant(TearVariant.EYE)
	elseif tear.Variant == TearVariant.PUPULA_BLOOD then
		tear:ChangeVariant(TearVariant.PUPULA)
	elseif tear.Variant == TearVariant.GODS_FLESH_BLOOD then
		tear:ChangeVariant(TearVariant.GODS_FLESH)
	end
end


function CommRedux.getroomidxbytype(roomtype)
	local rooms = Game():GetLevel():GetRooms()
	local t = {}
	for i = 0,#rooms - 1 do
		local room = rooms:Get(i)
		if room.Data.Type == roomtype then
			t[#t + 1] = room
		end
	end
	return t
end

function CommRedux.calcTearVel(vel, spd)
	if vel:Length() < spd then
		vel = vel:Resized(spd)
	end
	return vel
end

function CommRedux.getVecDir(vec)
	if not (vec:Length() > 0) then
		return -1
	else
		local thing = (vec:GetAngleDegrees() / 90) + 2
		
		if thing <= 1.5 and thing > 0.5 then 
			thing = 1
		elseif thing > 3.5 then 
			thing = 0 
		elseif thing <= 3.5 and thing > 3 then 
			thing = 3 
		end
		
		return CommRedux.round(thing, 1)
	end
end

function CommRedux.sign(number)
    return number > 0 and 1 or (number == 0 and 0 or -1)
end

function CommRedux.forceCharacterCostume(player, playertype, costumePath)
	local data = player:GetData()
	if data.hascostume == nil then data.hascostume = {} end
	if data.hascostume[playertype] == nil then data.hascostume[playertype] = false end
	
	if player:GetPlayerType() == playertype then
		if not data.hascostume[playertype] then
			player:AddNullCostume(costumePath)
			data.hascostume[playertype] = true
		end
	end
	
	if player:GetPlayerType() ~= playertype then
		if data.hascostume[playertype] then
			player:TryRemoveNullCostume(costumePath)
			data.hascostume[playertype] = false
		end
	end
end

function CommRedux.dupetear(t)
	local nt = Isaac.Spawn(t.Type, t.Variant, t.SubType, t.Position, t.Velocity, t.SpawnerEntity):ToTear()
	nt.Color = t.Color
	nt.FallingSpeed = t.FallingSpeed
	nt.FallingAcceleration = t.FallingAcceleration
	nt.Height = t.Height
	nt.Scale = t.Scale
	nt.CollisionDamage = t.CollisionDamage
	nt.TearFlags = nt.TearFlags | t.TearFlags
	return nt
end

function CommRedux.destroyneargrid(e, radius, breakmirror)
    local room = game:GetRoom()
    radius = radius or 10
    for i = 0, (room:GetGridSize()) do
		local gent = room:GetGridEntity(i)
        if room:GetGridEntity(i) then
			if (e.Position - gent.Position):Length() <= radius then
				if (gent.Desc.Type ~= 16) then
					gent:Destroy()
				else
					if gent.Desc.Variant ~= 1 or gent.Desc.State ~= 1 then
						gent:Destroy()
					end
					if breakmirror then gent:Destroy() end
				end
            end
        end
    end
end

function CommRedux.getgrid(_type, variant, coll)
    local room = game:GetRoom()
	local t = {}
    for i = 0, (room:GetGridSize()) do
		local gent = room:GetGridEntity(i)
        if room:GetGridEntity(i) then
			local istype = false
			local isvar = false
			local iscoll = false
			
			if gent:GetType() == _type then
				istype = true
			end
			if gent:GetType() == variant then
				isvar = true
			end
			if gent.CollisionClass == coll then
				iscoll = true
			end
			
			if _type == -1 then istype = true end
			if variant == -1 then isvar = true end
			
			if istype and isvar and iscoll then
				t[#t + 1] = gent
			end
        end
    end
	return t
end

function CommRedux.lowest(t) -- returns int
	local v = math.huge
	for i = 1, #t do
		if v > t[i] then v = t[i] end
		if i == #t then
			return v
		end
	end
end

function CommRedux.highest(t) -- returns int
	local v = -math.huge
	for i = 1, #t do
		if v < t[i] then v = t[i] end
		if i == #t then
			return v
		end
	end
end

function CommRedux.getclosestgrid(pos, _type, variant, coll) -- caps are optional
	local t_dist = {}
	local t = {}
	if _type and variant and coll then
		t = CommRedux.getgrid(_type, variant, coll)
	else
		t = CommRedux.getgrid(-1, -1, coll)
	end
	
	if t ~= nil and #t ~= 0 then
		for i, e in ipairs(t) do
			t_dist[i] = (pos):Distance(e.Position)
		end
	end
	
	if t ~= nil and #t ~= 0 then
		for i, e in ipairs(t) do
			local lowest = CommRedux.lowest(t_dist)
			if t_dist[i] and t_dist[i] == lowest then
				return e
			end
		end
	end
end

function CommRedux.getclosestent(pos, TYPE, VARIANT, SUBTYPE) -- caps are optional
	local t_dist = {}
	local t = {}
	if TYPE and VARIANT and SUBTYPE then
		t = Isaac.FindByType(TYPE, VARIANT, SUBTYPE, false, false)
	else
		t = Isaac.GetRoomEntities()
	end
	
	if t ~= nil and #t ~= 0 then
		for i, e in ipairs(t) do
			t_dist[i] = (pos):Distance(e.Position)
		end
	end
	
	if t ~= nil and #t ~= 0 then
		for i, e in ipairs(t) do
			local lowest = CommRedux.lowest(t_dist)
			if t_dist[i] and t_dist[i] == lowest then
				return e
			end
		end
	end
end

function CommRedux.blastdamage(radius, damage, knockback, parent, breakmirror, breakslots, breakstonies)
	local room = game:GetRoom()
	for i, e in pairs(Isaac.GetRoomEntities()) do
		if e.Position:Distance(parent.Position) <= radius then
			if e:IsVulnerableEnemy() and e:IsActiveEnemy() then
				e:TakeDamage(damage, DamageFlag.DAMAGE_EXPLOSION | DamageFlag.DAMAGE_CRUSH, EntityRef(parent), 0)
				e.Velocity = (e.Position - parent.Position):Resized(knockback)
			elseif e.Type == 6 and breakslots then
				e:TakeDamage(damage, DamageFlag.DAMAGE_EXPLOSION | DamageFlag.DAMAGE_CRUSH, EntityRef(parent), 0)
				e.Velocity = (e.Position - parent.Position):Resized(knockback)
			elseif e.Type == EntityType.ENTITY_FIREPLACE and e.Variant ~= 4 then
				e:Die()
			elseif e.Type == EntityType.ENTITY_MOVABLE_TNT then
				e:Die()
			elseif ((e.Type == EntityType.ENTITY_FAMILIAR and e.Variant == 239) or (e.Type == 4)) then
				e.Velocity = (e.Position - parent.Position):Resized(knockback)
			elseif ((e.Type == 17 and e.Variant ~= 3169) or (e.Type == EntityType.ENTITY_STONEY and breakstonies)) then
				e:Kill()
			elseif (e.Type == EntityType.ENTITY_PICKUP) then
				local pi = e:ToPickup()
				if e.Variant == PickupVariant.PICKUP_BOMBCHEST then
					pi:TryOpenChest(nil)
				end
			end
		end
	end
    CommRedux.destroyneargrid(parent, radius, breakmirror)
end

function CommRedux.getAbelBrains(p)
	local t = {}
	for i, b in ipairs(Isaac.FindByType(1, 0, PlayerType.PLAYER_CR_ABEL_BRAIN, false, false)) do
		if b.Parent and GetPtrHash(b.Parent) == GetPtrHash(p) then
			t[#t+ 1] = b
		end
	end
	return t
end

function CommRedux.getMaxCollectibleID()
    return Isaac.GetItemConfig():GetCollectibles().Size -1
end

function CommRedux.FindCollectiblesByTag(tag)
	local itemconfig = Isaac.GetItemConfig()
	local t = {}
	for i = 0, CommRedux.getMaxCollectibleID() do
		if itemconfig:GetCollectible(i) and itemconfig:GetCollectible(i):HasTags(ItemConfig.TAG_BOOK) then
			t[#t+1] = i
		end
	end
	return t
end

function CommRedux.getPtrHashEntity(entity)
	if entity then
		if entity.Entity then
			entity = entity.Entity
		end
		for _, matchEntity in pairs(Isaac.FindByType(entity.Type, entity.Variant, entity.SubType, false, false)) do
			if GetPtrHash(entity) == GetPtrHash(matchEntity) then
				return matchEntity
			end
		end
	end
	return nil
end

function CommRedux.getSpawnData(entity)
	if entity and entity.GetData then
		local data = entity:GetData()
		return data.SpawnData
	end
	return nil
end

function CommRedux.getSpawner(entity)
	if entity and entity.GetData then
		local spawnData = CommRedux.getSpawnData(entity)
		if spawnData and spawnData.SpawnerEntity then
			local spawner = CommRedux.getPtrHashEntity(spawnData.SpawnerEntity)
			return spawner
		end
	end
	return nil
end

function CommRedux.getPlayerFromTear(tear)
	local check = tear.Parent or CommRedux.getSpawner(tear) or tear.SpawnerEntity
	if check then
		if check.Type == EntityType.ENTITY_PLAYER then
			return CommRedux.getPtrHashEntity(check):ToPlayer()
		elseif check.Type == EntityType.ENTITY_FAMILIAR and check.Variant == FamiliarVariant.INCUBUS then
			local data = tear:GetData()
			data.IsIncubusTear = true
			return check:ToFamiliar().Player:ToPlayer()
		end
	end
	return nil
end

function CommRedux.getPlayerFromCreep(creep)
	local check = creep.Parent or CommRedux.getSpawner(creep) or creep.SpawnerEntity
	if check then
		if check.Type == EntityType.ENTITY_PLAYER then
			return CommRedux.getPtrHashEntity(check):ToPlayer()
		elseif check.Type == EntityType.ENTITY_FAMILIAR then
			local data = creep:GetData()
			data.IsFamiliarCreep = true
			return check:ToFamiliar().Player:ToPlayer()
		end
	end
	return nil
end

function CommRedux.GetCreepScale(effect)
	local variant = effect.Variant
	local scaleMultiplier = 1
	if variant == EffectVariant.PLAYER_CREEP_LEMON_MISHAP or variant == EffectVariant.PLAYER_CREEP_HOLYWATER then
		scaleMultiplier = 2
	elseif variant == EffectVariant.PLAYER_CREEP_LEMON_PARTY then
		scaleMultiplier = 10
	elseif variant == EffectVariant.PENTAGRAM_BLACKPOWDER then
		scaleMultiplier = 0.05
	end
	
	local range = (effect.Scale * 10) * scaleMultiplier
	return range
end

function CommRedux.SpawnPutridBubbles(effect)
	local creepData = effect:GetData()
	
	creepData.bubbleSpawnCounter = creepData.bubbleSpawnCounter or CommRedux.rand(0, 98, effect:GetDropRNG())
	creepData.bubbleSpawnCounter = creepData.bubbleSpawnCounter + 1
	
	if creepData.bubbleSpawnCounter >= 100 then
		creepData.bubbleSpawnCounter = CommRedux.rand(0, 80, effect:GetDropRNG())
		
		local scale = CommRedux.GetCreepScale(effect)
		scale = scale - 2
		if scale < 2 then
			scale = 2
		end
		
		local posX = effect.Position.X + CommRedux.rand(scale * -1, scale, effect:GetDropRNG())
		local posY = effect.Position.Y + CommRedux.rand(scale * -1, scale, effect:GetDropRNG())
		local pos = Vector(posX, posY)
		
		local gridEntity = game:GetRoom():GetGridEntityFromPos(pos)
		local spawnBubble = true
		if gridEntity ~= nil then
			local collisionClass = gridEntity.CollisionClass
			if collisionClass ~= GridCollisionClass.COLLISION_NONE then
				spawnBubble = false
			end
		end
		
		if spawnBubble then
			local bubble = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.EFFECT_CR_PUTRID_BUBBLE, 0, pos, Vector.Zero, effect)
			local bubbleSprite = bubble:GetSprite()
			
			local variant = effect.Variant
			local color = effect:GetColor()
			if variant == EffectVariant.PLAYER_CREEP_LEMON_MISHAP or variant == EffectVariant.PLAYER_CREEP_LEMON_PARTY or variant == EffectVariant.CREEP_YELLOW then
				bubbleSprite:Load("gfx/effects/creep/bubble (lemon).anm2", true)
			elseif variant == EffectVariant.PLAYER_CREEP_HOLYWATER or variant == EffectVariant.PLAYER_CREEP_HOLYWATER_TRAIL then
				bubbleSprite:Load("gfx/effects/creep/bubble (holy water).anm2", true)
			elseif variant == EffectVariant.PLAYER_CREEP_WHITE or variant == EffectVariant.CREEP_WHITE then
				bubbleSprite:Load("gfx/effects/creep/bubble (white).anm2", true)
			elseif variant == EffectVariant.PLAYER_CREEP_BLACK or variant == EffectVariant.CREEP_BLACK then
				bubbleSprite:Load("gfx/effects/creep/bubble (black).anm2", true)
			elseif variant == EffectVariant.PLAYER_CREEP_GREEN or variant == EffectVariant.CREEP_GREEN then
				bubbleSprite:Load("gfx/effects/creep/bubble (green).anm2", true)
			else
				bubble:SetColor(color, 300, 1, false, false)
			end
			bubbleSprite:Play("Bubble", true)
			
			bubble:Die()
		end
	end
end

function CommRedux.getBombRadiusFromDamage(damage)
    if 175.0 <= damage then
        return 105.0
    else
        if damage <= 140.0 then
            return 75.0
        else
            return 90.0
        end
    end
end

for i, v in ipairs(scriptlist) do
	print('[Community Remix] loading script ' .. v .. '...')
	include(selfpath .. v)
end
