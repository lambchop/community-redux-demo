local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(self, p, flag)
	local bofc = p:GetEffects():GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_BOX_OF_FRIENDS)
	local d = p:GetData()
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_CONQUERORBABY) then
		if flag == CacheFlag.CACHE_FAMILIARS then
			if not d.updfam then d.updfam = true end
			p:CheckFamiliar(
				FamiliarVariant.FAMILIAR_CONQUEROR_BABY, 
				p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_CONQUERORBABY, false) + bofc, 
				modrng, 
				Isaac.GetItemConfig():GetCollectible(CollectibleType.COLLECTIBLE_CR_CONQUERORBABY)
			)
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, function(self, f)
	f:AddToFollowers()
	local d = f:GetData()
	local s = f:GetSprite()
	s:Play("Idle")
end, FamiliarVariant.FAMILIAR_CONQUEROR_BABY)

CommRedux:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(self, f)
	local p = f.Player
	local pd = p:GetData()
	if pd.updfam then
		f.Parent = p
		pd.updfam = nil
	end
	f:FollowParent()
end, FamiliarVariant.FAMILIAR_CONQUEROR_BABY)