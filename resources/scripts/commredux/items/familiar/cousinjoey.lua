local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(self, p, flag)
	local bofc = p:GetEffects():GetCollectibleEffectNum(CollectibleType.COLLECTIBLE_BOX_OF_FRIENDS)
	local d = p:GetData()
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_COUSINJOEY) then
		if flag == CacheFlag.CACHE_FAMILIARS then
			p:CheckFamiliar(
				FamiliarVariant.FAMILIAR_COUSIN_JOEY, 
				p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_COUSINJOEY, false) + bofc, 
				modrng, 
				Isaac.GetItemConfig():GetCollectible(CollectibleType.COLLECTIBLE_CR_COUSINJOEY)
			)
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_FAMILIAR_INIT, function(self, f)
	f:AddToFollowers()
	local d = f:GetData()
	local s = f:GetSprite()
	s:Play("FloatDown")
end, FamiliarVariant.FAMILIAR_COUSIN_JOEY)

CommRedux:AddCallback(ModCallbacks.MC_FAMILIAR_UPDATE, function(self, f)
	f:AddToFollowers()
	f:FollowParent()
	local p = f.Player
	local s = f:GetSprite()
	local FireDir = p:GetFireDirection()
	local d = f:GetData()
	d.MaxFCD = 6
	local dirtab = {
		"Down",
		"Side",
		"Up",
		"Side",
		"Down"
	}
	
	local iterator = 1
	if p:HasTrinket(TrinketType.TRINKET_FORGOTTEN_LULLABY) then
		iterator = iterator * 2
	end
	
	local isfinfireanim = false
	
	for i = 1, #dirtab do
		if s:GetAnimation() == "FloatShoot" .. dirtab[i] then
			if s:GetFrame() >= 15 then
				isfinfireanim = true
			end
		end
	end
	
	if f.FireCooldown <= 0 and FireDir ~= -1 then
		if FireDir == 0 then
			f.FlipX = true
		else
			f.FlipX = false
		end
		
		local tearVel = Vector.Zero
		local dirveltab = {
			Vector(-1, 0),
			Vector(0, -1),
			Vector(1, 0),
			Vector(0, 1)
		}
		
		local t = f:FireProjectile(dirveltab[FireDir + 1])
		t.Color = Color(1, 1, 0, 1, 0.18, 0.05, 0)
		t.FallingSpeed = 1.5
		t.CollisionDamage = t.CollisionDamage * 0.5
		
		s:Play("FloatShoot" .. dirtab[FireDir + 2], false)
		
		f.FireCooldown = d.MaxFCD
	end
	
	if f.FireCooldown > 0 and FireDir ~= -1 then
		if FireDir == 0 then
			f.FlipX = true
		else
			f.FlipX = false
		end
		if f.FireCooldown < d.MaxFCD / 2 then
			if d.MaxFCD > 8 * iterator then
				s:Play("Float" .. dirtab[FireDir + 2], false)
			end
		else
			s:Play("FloatShoot" .. dirtab[FireDir + 2], false)
		end
	end
	
	if f.FireCooldown < d.MaxFCD / 2 and FireDir == -1 and isfinfireanim then
		f.FlipX = false
		s:Play("FloatDown")
	end
	
	if f.FireCooldown > 0 then f.FireCooldown = f.FireCooldown - iterator end
	
end, FamiliarVariant.FAMILIAR_COUSIN_JOEY)