local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

function CommRedux:AppleUse(id, rng, p, flags, slot)
	local d = p:GetData()
	local hud = game:GetHUD()
	if d.CommRedux == nil then d.CommRedux = {} end
	if flags & UseFlag.USE_OWNED > 0 then
		if id == CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE then
			d.CommRedux.AppleMode = "Damage"
			p:AddNullCostume(NullItemID.ID_CR_APPLEBLOOD)
			p:TryRemoveNullCostume(NullItemID.ID_CR_APPLETEARS)
			if p:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES) and #Isaac.FindByType(3, 206, CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE) < 2 then
				for i = 1, 2 do
					p:AddWisp(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE, p.Position, true, false)
				end
				for _, w in ipairs(Isaac.FindByType(3, 206, CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED)) do
					w:Die()
				end
			end
		elseif id == CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED or id == CollectibleType.COLLECTIBLE_CR_THEAPPLE then
			d.CommRedux.AppleMode = "Tears"
			p:AddNullCostume(NullItemID.ID_CR_APPLETEARS)
			p:TryRemoveNullCostume(NullItemID.ID_CR_APPLEBLOOD)
			if p:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES) and #Isaac.FindByType(3, 206, CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED) < 2 then
				for i = 1, 2 do
					p:AddWisp(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED, p.Position, true, false)
				end
				for _, w in ipairs(Isaac.FindByType(3, 206, CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE)) do
					w:Die()
				end
			end
		end
		d.appleslot = slot
		sfx:Play(SoundEffect.SOUND_CR_APPLEGULP, 1, 0, false, 1, 0)
		p:AddCacheFlags(CacheFlag.CACHE_ALL)
		p:EvaluateItems()
		if CommRedux.sd.showstreak == 2 then
			local dps_str
			if CommRedux.sd.showdps == 2 then
				dps_str = "DPS:     " .. CommRedux.round(CommRedux.GetDPS(p), 100)
			else
				dps_str = ""
			end
			hud:ShowItemText(d.CommRedux.AppleMode .. " Mode", dps_str, false)
		end
		if id == CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE then
			p:RemoveCollectible(id, true, slot, true)
			p:AddCollectible(AppleFrames.R, 0, true, slot, 0)
			p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED, "UseItem", "PlayerPickupSparkle")
			d.dir = -1
			d.cd = 2
		elseif id == CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED then
			p:RemoveCollectible(id, true, slot, true)
			p:AddCollectible(AppleFrames.L, 0, true, slot, 0)
			p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE, "UseItem", "PlayerPickupSparkle")
			d.dir = 1
			d.cd = 2
		elseif id == CollectibleType.COLLECTIBLE_CR_THEAPPLE then
			p:RemoveCollectible(id, true, slot, true)
			p:AddCollectible(AppleFrames.M, 0, true, slot, 0)
			p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE, "UseItem", "PlayerPickupSparkle")
			d.dir = 1
			d.cd = 2
		end
		return false
	else
		if d.CommRedux.AppleMode == "Tears" then
			d.CommRedux.AppleMode = "Damage"
			p:AddNullCostume(NullItemID.ID_CR_APPLEBLOOD)
			p:TryRemoveNullCostume(NullItemID.ID_CR_APPLETEARS)
		else
			d.CommRedux.AppleMode = "Tears"
			p:AddNullCostume(NullItemID.ID_CR_APPLETEARS)
			p:TryRemoveNullCostume(NullItemID.ID_CR_APPLEBLOOD)
		end
		sfx:Play(SoundEffect.SOUND_CR_APPLEGULP, 1, 0, false, 1, 0)
		p:AddCacheFlags(CacheFlag.CACHE_ALL)
		p:EvaluateItems()
		if CommRedux.sd.showstreak == 2 then
			local dps_str
			if CommRedux.sd.showdps == 2 then
				dps_str = "DPS:     " .. CommRedux.round(CommRedux.GetDPS(p), 100)
			else
				dps_str = ""
			end
			hud:ShowItemText(d.CommRedux.AppleMode .. " Up", dps_str, false)
		end
		return false
	end
end
CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, CommRedux.AppleUse, CollectibleType.COLLECTIBLE_CR_THEAPPLE)
CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, CommRedux.AppleUse, CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED)
CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, CommRedux.AppleUse, CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE)

CommRedux:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, p)
	local d = p:GetData()
	local pfx = p:GetEffects()
	
	local hasapple = (
		p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED) or
		p:HasCollectible(AppleFrames.L) or
		p:HasCollectible(AppleFrames.ML) or
		p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE) or
		p:HasCollectible(AppleFrames.M) or
		p:HasCollectible(AppleFrames.MR) or
		p:HasCollectible(AppleFrames.R) or
		p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE)
	)
	
	local hasappleframes = (
		p:HasCollectible(AppleFrames.L) or
		p:HasCollectible(AppleFrames.ML) or
		p:HasCollectible(AppleFrames.M) or
		p:HasCollectible(AppleFrames.MR) or
		p:HasCollectible(AppleFrames.R)
	)
	
	if d.cd and d.cd > 0 then d.cd = d.cd - 1 end
	
	if d.dir and d.appleslot and d.cd == 0 then
		if p:HasCollectible(AppleFrames.L) then
			if d.dir == 1 then
				p:RemoveCollectible(AppleFrames.L, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.ML, 0, true, d.appleslot, 0)
				d.cd = 2
			else
				p:RemoveCollectible(AppleFrames.L, true, d.appleslot, true)
				p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED, 0, true, d.appleslot, 0)
				d.dir = nil
				d.cd = nil
			end
		elseif p:HasCollectible(AppleFrames.R) then
			if d.dir == -1 then
				p:RemoveCollectible(AppleFrames.R, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.MR, 0, true, d.appleslot, 0)
				d.cd = 2
			else
				p:RemoveCollectible(AppleFrames.R, true, d.appleslot, true)
				p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE, 0, true, d.appleslot, 0)
				d.dir = nil
				d.cd = nil
			end
		elseif p:HasCollectible(AppleFrames.M) then
			if d.dir == -1 then
				p:RemoveCollectible(AppleFrames.M, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.ML, 0, true, d.appleslot, 0)
				d.cd = 2
			else
				p:RemoveCollectible(AppleFrames.M, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.MR, 0, true, d.appleslot, 0)
				d.cd = 2
			end
		elseif p:HasCollectible(AppleFrames.ML) then
			if d.dir == -1 then
				p:RemoveCollectible(AppleFrames.ML, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.L, 0, true, d.appleslot, 0)
				d.cd = 2
			else
				p:RemoveCollectible(AppleFrames.ML, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.M, 0, true, d.appleslot, 0)
				d.cd = 2
			end
		elseif p:HasCollectible(AppleFrames.MR) then
			if d.dir == -1 then
				p:RemoveCollectible(AppleFrames.MR, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.M, 0, true, d.appleslot, 0)
				d.cd = 2
			else
				p:RemoveCollectible(AppleFrames.MR, true, d.appleslot, true)
				p:AddCollectible(AppleFrames.R, 0, true, d.appleslot, 0)
				d.cd = 2
			end
		end
	end
	
	if d.CommRedux.AppleMode and not hasapple then
		d.CommRedux.AppleMode = nil
		p:TryRemoveNullCostume(NullItemID.ID_CR_APPLETEARS)
		p:TryRemoveNullCostume(NullItemID.ID_CR_APPLEBLOOD)
		p:AddCacheFlags(CacheFlag.CACHE_ALL)
		p:EvaluateItems()
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if CommRedux.sd.applemode and CommRedux.sd.applemode[tostring(playerseed)] then
		d.CommRedux.AppleMode = CommRedux.sd.applemode[tostring(playerseed)] 
		p:AddCacheFlags(CacheFlag.CACHE_ALL)
		p:EvaluateItems()
	end
	
	if CommRedux.sd.appleslot and CommRedux.sd.appleslot[tostring(playerseed)] then
		d.appleslot = CommRedux.sd.appleslot[tostring(playerseed)]
	end
	
	local hasappleframes = (
		p:HasCollectible(AppleFrames.L) or
		p:HasCollectible(AppleFrames.ML) or
		p:HasCollectible(AppleFrames.M) or
		p:HasCollectible(AppleFrames.MR) or
		p:HasCollectible(AppleFrames.R)
	)
	
	if hasappleframes then
		p:RemoveCollectible(p:GetActiveItem(d.appleslot), true, d.appleslot, true)
		if d.CommRedux.AppleMode == "Damage" then
			p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED, 0, true, d.appleslot, 0)
		elseif d.CommRedux.AppleMode == "Tears" then
			p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE, 0, true, d.appleslot, 0)
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if d.CommRedux.AppleMode then
		CommRedux.sd.applemode[tostring(playerseed)] = d.CommRedux.AppleMode
	end
	if d.appleslot then
		CommRedux.sd.appleslot[tostring(playerseed)] = d.appleslot
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end 
	local numcarbattery = p:GetCollectibleNum(356, false)
	
	if d.CommRedux.AppleMode then
		if d.CommRedux.AppleMode == "Damage" then
			if flag == CacheFlag.CACHE_DAMAGE then
				local dmult = 0
				if (p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM) then
					dmult = dmult + 0.5
				end
				dmult = dmult + (numcarbattery / 2)
				
				p.Damage = (p.Damage * (1.5 + dmult))
				
				if p.Damage < 3.5 then
					p.Damage = 3.5
				end
			elseif flag == CacheFlag.CACHE_LUCK then
				if not (p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM) then
					p.Luck = p.Luck - 2
					if p.Luck > 0 then p.Luck = 0 end
				end
			elseif flag == CacheFlag.CACHE_FIREDELAY then
				if not (p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM) then
					if p.MaxFireDelay < 10 then
						p.MaxFireDelay = (p.MaxFireDelay + 2.5) / 1.25
					end
				end
			elseif flag == CacheFlag.CACHE_SPEED then
				if not (p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM) then
					p.MoveSpeed = p.MoveSpeed - 0.16
				end
				if p.MoveSpeed > 1 then p.MoveSpeed = 1 end
			elseif flag == CacheFlag.CACHE_SHOTSPEED then
				p.ShotSpeed = p.ShotSpeed + 0.16
			elseif flag == CacheFlag.CACHE_RANGE then
				p.TearRange = p.TearRange - 80
			end
		elseif d.CommRedux.AppleMode == "Tears" then
			if flag == CacheFlag.CACHE_FIREDELAY then
				local tpsadd = 0
				tpsadd = tpsadd + numcarbattery / 2
				
				if (p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM) then
					tpsadd = tpsadd + 1
				end
				
				tears_up.div = tears_up.div / 1 - (tpsadd / 4)
				
			elseif flag == CacheFlag.CACHE_DAMAGE then
				if not (p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) and p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM) then
					p.Damage = p.Damage * (0.75)
				end
			elseif flag == CacheFlag.CACHE_LUCK then
				p.Luck = p.Luck + 1 + (1/3)
				p.Luck = p.Luck + math.abs(p.Luck / 2)
				if p.Luck < 0 then p.Luck = 0 end
			elseif flag == CacheFlag.CACHE_SPEED then
				p.MoveSpeed = p.MoveSpeed + 0.32
				if p.MoveSpeed < 1 then p.MoveSpeed = 1 end
			elseif flag == CacheFlag.CACHE_SHOTSPEED then
				p.ShotSpeed = p.ShotSpeed - 0.16
			elseif flag == CacheFlag.CACHE_RANGE then
				p.TearRange = p.TearRange + 40
			end
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE, function(p, flag)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end 
	
	if d.CommRedux.AppleMode then
		if d.CommRedux.AppleMode == "Damage" then
		elseif d.CommRedux.AppleMode == "Tears" then
			if flag == CacheFlag.CACHE_FIREDELAY then
				p.MaxFireDelay = CommRedux.AddTPS(p, 1)
			end
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_PLAYER_FIRE_TEAR, function(p, t)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	if d.CommRedux.AppleMode and d.CommRedux.AppleMode == "Damage" then
		CommRedux.forceBloodTear(t)
	elseif d.CommRedux.AppleMode and d.CommRedux.AppleMode == "Tears" then
		CommRedux.forceBlueTear(t)
	end
end)