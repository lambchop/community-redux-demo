local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p, f, slot)
	local t = {
		CollectibleType.COLLECTIBLE_BEAN,
		CollectibleType.COLLECTIBLE_BUTTER_BEAN,
		CollectibleType.COLLECTIBLE_WAIT_WHAT,
		CollectibleType.COLLECTIBLE_MEGA_BEAN,
		CollectibleType.COLLECTIBLE_KIDNEY_BEAN,
		CollectibleType.COLLECTIBLE_CR_COOLBEAN,
		CollectibleType.COLLECTIBLE_CR_HOTBEAN
	}
	p:UseActiveItem(t[CommRedux.rand(1, #t, rng)], false, false, false, false, slot)
end, CollectibleType.COLLECTIBLE_CR_BOWLOBEANS)