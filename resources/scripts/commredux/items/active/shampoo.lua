local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local d = p:GetData()
	
	if p:HasCollectible(CollectibleType.COLLECTIBLE_BAR_OF_SOAP) then
		d.CommRedux.whiteappleboost = d.CommRedux.whiteappleboost + 1.5
	else
		d.CommRedux.whiteappleboost = d.CommRedux.whiteappleboost + 0.75
	end
	
	sfx:Play(SoundEffect.SOUND_CR_CORKPOP, 1, 0, false, 1, 0)
	
	p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
	p:EvaluateItems()
end, CollectibleType.COLLECTIBLE_CR_SHAMPOO)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE, function(p, flag)
	local d = p:GetData()
	
	if d.CommRedux == nil then d.CommRedux = {} end
	
	if d.CommRedux.shampooboost == nil then d.CommRedux.shampooboost = {} end
	if d.CommRedux.shampooboost.tears == nil then d.CommRedux.shampooboost.tears = 0 end
	
	if flag == CacheFlag.CACHE_FIREDELAY and d.CommRedux.shampooboost then
		p.MaxFireDelay = CommRedux.AddTPS(p, CommRedux.GetTPS(p) * d.CommRedux.shampooboost.tears)
	end
end)