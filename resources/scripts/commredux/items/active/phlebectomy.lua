local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_TAP, function(id, p, slot)
	if id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1] then
		p:RemoveCollectible(id, true, slot, true)
		p:AddCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2], 0, true, slot, 0)
	elseif id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2] then
		p:RemoveCollectible(id, true, slot, true)
		p:AddCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3], 0, true, slot, 0)
	elseif id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3] then
		p:RemoveCollectible(id, true, slot, true)
		p:AddCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4], 0, true, slot, 0)
	elseif id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4] then
		p:RemoveCollectible(id, true, slot, true)
		p:AddCollectible(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2], 0, true, slot, 0)
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_HOLD_ITEM, function(id, p, slot, frames)
	if 
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1] or
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2] or
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3] or
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4]
	then
		if frames == 6 then
			p:AnimateCollectible(id, "LiftItem", "PlayerPickup")
		end
		if frames > 6 then
			local r = p.Color.R - (frames + 1)/50
			if r < 0.5 then r = 0.5 end
			
			local g = p.Color.G - (frames + 1)/50
			if g < 0.5 then g = 0.5 end
			
			local b = p.Color.B - (frames + 1)/50
			if b < 0.5 then b = 0.5 end
			
			local ro = p.Color.RO + (frames + 1)/50
			if ro > 0.5 then ro = 0.5 end
			p:SetColor(Color(r, g, b, p.Color.A, ro, p.Color.GO, p.Color.BO), 2, 999, false, true)
		end
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_RELEASE, function(id, p, slot, frames)
	if 
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1] or
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2] or
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3] or
		id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4]
	then
		local room = game:GetRoom()
		p:AnimateCollectible(id, "HideItem", "PlayerPickup")
		if frames >= 24 then
			if id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[2] then
				p:TakeDamage(2, DamageFlag.DAMAGE_INVINCIBLE | DamageFlag.DAMAGE_NO_PENALTIES | DamageFlag.DAMAGE_NO_MODIFIERS | DamageFlag.DAMAGE_RED_HEARTS, EntityRef(p), 0)
				p:SetMinDamageCooldown(10)
				local pi = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_BOMB, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
				pi:SetColor(Color(0, 0, 0, 1, 0.75, 0, 0), 16, 999, true, true)
				if p:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES) then
					p:AddWisp(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1], p.Position, true, false)
				end
			elseif id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[3] then
				p:TakeDamage(2, DamageFlag.DAMAGE_INVINCIBLE | DamageFlag.DAMAGE_NO_PENALTIES | DamageFlag.DAMAGE_NO_MODIFIERS | DamageFlag.DAMAGE_RED_HEARTS, EntityRef(p), 0)
				p:SetMinDamageCooldown(10)
				local pi = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_KEY, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
				pi:SetColor(Color(0, 0, 0, 1, 0.75, 0, 0), 16, 999, true, true)
				if p:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES) then
					p:AddWisp(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1], p.Position, true, false)
				end
			elseif id == CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[4] then
				p:TakeDamage(2, DamageFlag.DAMAGE_INVINCIBLE | DamageFlag.DAMAGE_NO_PENALTIES | DamageFlag.DAMAGE_NO_MODIFIERS | DamageFlag.DAMAGE_RED_HEARTS, EntityRef(p), 0)
				p:SetMinDamageCooldown(10)
				for i = 1, CommRedux.rand(3, 5, modrng) do
					local pi = Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
					pi:SetColor(Color(0, 0, 0, 1, 0.75, 0, 0), 16, 999, true, true)
				end
				if p:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES) then
					p:AddWisp(CollectibleType.COLLECTIBLE_CR_PHLEBECTOMY[1], p.Position, true, false)
				end
			end
		end
	end
end, CommRedux)
