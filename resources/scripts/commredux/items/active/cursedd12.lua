local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	CommRedux.sd.cursedroomidx = {}
	local level = game:GetLevel()
	local normalroomidx = CommRedux.getroomidxbytype(1)
	local didgen = false
	for i = 1, #normalroomidx do
	
		local roomDescriptor = normalroomidx[i]
		local roomConfigRoom = roomDescriptor.Data
		local spawnList = roomConfigRoom.Spawns
		
		local isvalid = false
		
		for i = 1, #spawnList do
			if spawnList:Get(i - 1):PickEntry(0).Type > 9 and spawnList:Get(i - 1):PickEntry(0).Type < 960 then
				isvalid = true
			end
		end
		
		if isvalid then
			while didgen == false do
				if CommRedux.rand(1, 8, rng) == 1 then
					CommRedux.sd.cursedroomidx[#CommRedux.sd.cursedroomidx + 1] = normalroomidx[i].GridIndex
					didgen = true
				end
			end
		end
	end
	if didgen then
		game:ShakeScreen(16)
		sfx:Play(SoundEffect.SOUND_BLACK_POOF, 1, 0, false, 0.5, 0)
		local poof = Isaac.Spawn(1000, 16, 1, p.Position, Vector.Zero, p)
		poof:GetSprite().Color = Color(0.65, 0.25, 0.65, 1, 0, 0, 0)
	else
		sfx:Play(SoundEffect.SOUND_BOSS2INTRO_ERRORBUZZ, 1, 0, false, 1, 0)
	end
	return true
end, CollectibleType.COLLECTIBLE_CR_CURSED_D12)