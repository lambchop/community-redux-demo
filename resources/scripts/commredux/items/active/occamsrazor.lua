local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p, f, slot)
	local d = p:GetData()
	if d.CommRedux.occamboost then
		d.CommRedux.occamboost = d.CommRedux.occamboost + 0.25
	end
	p:AddBrokenHearts(1)
	p:UseActiveItem(CollectibleType.COLLECTIBLE_DULL_RAZOR, false, false, false, false, slot)
	p:AddCacheFlags(CacheFlag.CACHE_ALL)
	p:EvaluateItems()
	if p:GetPlayerType() == PlayerType.PLAYER_THELOST or p:GetPlayerType() == PlayerType.PLAYER_THELOST_B then
		p:Die()
	end
	return {ShowAnim = true}
end, CollectibleType.COLLECTIBLE_CR_OCCAMSRAZOR)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if CommRedux.sd.occamboost and CommRedux.sd.occamboost[tostring(playerseed)] then
		d.CommRedux.occamboost = CommRedux.sd.occamboost[tostring(playerseed)] 
		p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
		p:EvaluateItems()
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if d.CommRedux.occamboost then
		CommRedux.sd.occamboost[tostring(playerseed)] = d.CommRedux.occamboost
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	local d = p:GetData()
	
	if d.CommRedux == nil then d.CommRedux = {} end
	
	if d.CommRedux.occamboost == nil then d.CommRedux.occamboost = 0 end
	
	if flag == CacheFlag.CACHE_DAMAGE then
		p.Damage = p.Damage * (d.CommRedux.occamboost + 1)
	end
	
	if flag == CacheFlag.CACHE_FIREDELAY then
		tears_up.div = tears_up.div / (1 - (0.2 * d.CommRedux.occamboost))
	end
	
	if flag == CacheFlag.CACHE_SHOTSPEED then
		p.ShotSpeed = p.ShotSpeed + (0.08 * d.CommRedux.occamboost)
	end
	
	if flag == CacheFlag.CACHE_SPEED then
		p.MoveSpeed = p.MoveSpeed * (1 + (0.16 * d.CommRedux.occamboost))
	end
	
	if flag == CacheFlag.CACHE_LUCK then
		p.Luck = p.Luck + (2 * d.CommRedux.occamboost)
	end
	
	if flag == CacheFlag.CACHE_RANGE then
		p.TearRange = p.TearRange + (d.CommRedux.occamboost * 2)*40
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	if p:GetBrokenHearts() >= 6 and not CommRedux.sd.unlocks.occamsrazor then
		miniAch.AddToQueue("occamsrazor.png")
		CommRedux.sd.unlocks.occamsrazor = true
	end
end)
