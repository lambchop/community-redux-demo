local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	sfx:Play(SoundEffect.SOUND_BOOK_PAGE_TURN_12, 1, 0, false, 1, 0)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	if d.CommRedux.bookofdespair == nil then d.CommRedux.bookofdespair = 0 end
	d.CommRedux.bookofdespair = d.CommRedux.bookofdespair + 1
	p:GetEffects():AddCollectibleEffect(CollectibleType.COLLECTIBLE_TORN_PHOTO, true, 1)
	p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY | CacheFlag.CACHE_SHOTSPEED | CacheFlag.CACHE_LUCK)
	p:EvaluateItems()
	return true
end, CollectibleType.COLLECTIBLE_CR_BOOKOFGRIEF)

CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function(_)
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		if d.CommRedux == nil then d.CommRedux = {} end
		if d.CommRedux.bookofdespair and d.CommRedux.bookofdespair > 0 then
			d.CommRedux.bookofdespair = 0
			p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY | CacheFlag.CACHE_SHOTSPEED | CacheFlag.CACHE_LUCK)
			p:EvaluateItems()
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE, function(p, flag)
	local d = p:GetData()
	
	if d.CommRedux == nil then d.CommRedux = {} end
	if d.CommRedux.bookofdespair == nil then d.CommRedux.bookofdespair = 0 end
	
	if flag == CacheFlag.CACHE_FIREDELAY then
		if d.CommRedux.bookofdespair > 0 then
			p.MaxFireDelay = CommRedux.AddTPS(p, d.CommRedux.bookofdespair)
		end
	end
	
	if flag == CacheFlag.CACHE_SHOTSPEED then
		if d.CommRedux.bookofdespair > 0 then
			p.ShotSpeed = p.ShotSpeed + (0.16 * CommRedux.sign(d.CommRedux.bookofdespair))
		end
	end
	
	if flag == CacheFlag.CACHE_LUCK then
		if d.CommRedux.bookofdespair > 0 then
			p.Luck = p.Luck + (5 * ((d.CommRedux.bookofdespair + 1)/2))
		end
	end
end)