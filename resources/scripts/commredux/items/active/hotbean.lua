local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local fart = Isaac.Spawn(1000, 34, 0, p.Position, p.Velocity*0, p):ToEffect()
	fart.Color = Color(1, 0.1, 0.1, 0.75, 1, 0.25, 0)
	
	local radius = 96
	local damage = 20
	local fartpitch
	if p:HasTrinket(TrinketType.TRINKET_GIGANTE_BEAN) then
		radius = radius * 2
		damage = damage * 2
		sfx:Stop(SoundEffect.SOUND_FART)
		sfx:Play(SoundEffect.SOUND_FART, 1, 0, false, 0.5, 0)
		fart.SpriteScale = fart.SpriteScale * 2
	end
	
	for i, e in pairs(Isaac.GetRoomEntities()) do
		if e.Position:Distance(p.Position) <= radius then
			if e:IsVulnerableEnemy() and e:IsActiveEnemy() then
				e:AddBurn(EntityRef(p), 30 * 4, 1)
				e:TakeDamage(damage, 0, EntityRef(p), 0)
			end
		end
	end
	local fire = Isaac.Spawn(1000, 51, 0, p.Position, Vector.Zero, p):ToEffect()
	fire.Scale = 1.5
	fire:SetDamageSource(p.Type)
	return false
end, CollectibleType.COLLECTIBLE_CR_HOTBEAN)