local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	
	local canuse
	if p:GetMaxHearts() > 2 then
		canuse = true
	else
		if (p:GetSoulHearts() > 0 or p:GetBoneHearts() > 0) and p:GetMaxHearts() > 0 then
			canuse = true
		else
			canuse = false
		end
	end
	
	if canuse then
		p:AddMaxHearts(-2)
		for i = 1, CommRedux.rand(4, 6, rng) do
			local pivar = CommRedux.rand(1, 8, rng)*10
			if pivar == 50 then pivar = 70 end
			if pivar == 60 then pivar = 90 end
			if pivar == 70 then pivar = 300 end
			if pivar == 80 then pivar = 350 end
			Isaac.Spawn(5, pivar, 0, p.Position, Vector(CommRedux.rand_float(2.5, 5, rng), 0):Rotated(CommRedux.rand(0, 360, rng)), p)
		end
		sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
		return true
	end
end, CollectibleType.COLLECTIBLE_CR_TAMMYSPAW)