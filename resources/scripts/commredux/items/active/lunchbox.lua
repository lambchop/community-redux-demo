local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p, flags, slot)
	for i = 1, CommRedux.rand(1, 8, rng) do
		local pivar = CommRedux.rand(1, 8, rng)*10
		if pivar == 50 then pivar = 70 end
		if pivar == 60 then pivar = 90 end
		if pivar == 70 then pivar = 300 end
		if pivar == 80 then pivar = 350 end
		Isaac.Spawn(5, pivar, 0, p.Position, Vector(CommRedux.rand_float(5, 7, rng), 0):Rotated(CommRedux.rand(0, 360, rng)), p)
	end
	sfx:Play(SoundEffect.SOUND_CHEST_OPEN, 1, 0, false, 1.5, 0)
	p:AddCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX_EMPTY, 0, true, slot, 0)
	p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX_EMPTY, "UseItem", "PlayerPickupSparkle")
	p:RemoveCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX)
	return {ShowAnim = false}
end, CollectibleType.COLLECTIBLE_CR_LUNCHBOX)

CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function(self)
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		for i2 = 0, 3 do
			if p:GetActiveItem(i2) == CollectibleType.COLLECTIBLE_CR_LUNCHBOX_EMPTY then
				p:RemoveCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX_EMPTY)
				p:AddCollectible(CollectibleType.COLLECTIBLE_CR_LUNCHBOX, 0, true, i2, 0)
			end
		end
	end
end)