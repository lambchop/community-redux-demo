local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local seed = rng:Next()
	local runeToSpawn = game:GetItemPool():GetCard(seed, false, true, true)
	p:AddCard(runeToSpawn)
	p:AnimateCard(runeToSpawn, "UseItem")
	sfx:Play(SoundEffect.SOUND_STONE_IMPACT, 1, 0, false, 1.5, 0)
end, CollectibleType.COLLECTIBLE_CR_GEODE)