local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p, flags, slot)
	for i = 1, CommRedux.rand(2, 4, rng) do
		local dipvel = Vector(CommRedux.rand_float(50, 70, rng), 0):Rotated(CommRedux.rand(0, 360, rng))
		p:ThrowFriendlyDip(0, p.Position, p.Position + dipvel)
	end
	sfx:Play(SoundEffect.SOUND_MEATHEADSHOOT, 1, 0, false, 1, 0)
	return {ShowAnim = true}
end, CollectibleType.COLLECTIBLE_CR_POTTY)