local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local entities = Isaac:GetRoomEntities()
	
	for i=1, #entities do
		if entities[i].Type == EntityType.ENTITY_PICKUP then
			if entities[i].Variant == PickupVariant.PICKUP_TRINKET then
				if entities[i].SubType then
					local itemPool = Game():GetItemPool()
					entities[i]:ToPickup():Morph(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, itemPool:GetTrinket(), true)
					Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.POOF01, -1, entities[i].Position, entities[i].Velocity, p)
				end
			end
		end
	end
	
	return true
end, CollectibleType.COLLECTIBLE_CR_D3)