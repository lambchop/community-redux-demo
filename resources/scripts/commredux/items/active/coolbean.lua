local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local fart = Isaac.Spawn(1000, 34, 0, p.Position, p.Velocity*0, p):ToEffect()
	fart.Color = Color(1, 1, 1, 0.75, 0.05, 0.25, 0.8)
	
	local radius = 96
	local damage = 7
	if p:HasTrinket(TrinketType.TRINKET_GIGANTE_BEAN) then
		radius = radius * 2
		damage = damage * 2
		sfx:Stop(SoundEffect.SOUND_FART)
		sfx:Play(SoundEffect.SOUND_FART, 1, 0, false, 0.5, 0)
		fart.SpriteScale = fart.SpriteScale * 2
	end
	
	for i, e in pairs(Isaac.GetRoomEntities()) do
		if e.Position:Distance(p.Position) <= radius then
			if e:IsVulnerableEnemy() and e:IsActiveEnemy() then
				if not e:IsBoss() then
					e:AddEntityFlags(EntityFlag.FLAG_ICE)
					e:AddEntityFlags(EntityFlag.FLAG_FREEZE)
					e:TakeDamage(e.HitPoints + 1, DamageFlag.DAMAGE_IGNORE_ARMOR, EntityRef(p), 0)
				else
					e:AddSlowing(EntityRef(p), 60, 1, Color(e.Color.R, e.Color.G, e.Color.B, e.Color.A, e.Color.RO + 0.2, e.Color.GO + 0.2, e.Color.BO + 0.2))
					e:TakeDamage(damage, 0, EntityRef(p), 0)
				end
			end
		end
	end
	return false
end, CollectibleType.COLLECTIBLE_CR_COOLBEAN)