local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_TAP, function(id, p, slot)
	if id == CollectibleType.COLLECTIBLE_CR_PILLCASE then
		local d = p:GetData()
		if d.pillcase == nil then d.pillcase = {} end
		local pos = 1
		local pi = d.pillcase[pos]
		local currpi = p:GetPill(0)
		if currpi > 0 and #d.pillcase > 0 then
			d.pillcase[#d.pillcase + 1] = currpi
			p:SetPill(0, 0)
			sfx:Play(SoundEffect.SOUND_CR_PILLBOXOPEN, 1, 0, false, 1, 0)
		end
		
		if pi ~= nil then
			p:AddPill(pi)
			p:AnimatePill(pi, "UseItem")
			sfx:Play(SoundEffect.SOUND_CR_PILLBOXOPEN, 1, 0, false, 1, 0)
			table.remove(d.pillcase, pos)
		end
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_HOLD_ITEM, function(id, p, slot, frames)
	if id == CollectibleType.COLLECTIBLE_CR_PILLCASE then
		if frames == 6 then
			local pi = p:GetPill(0)
			local d = p:GetData()
			if d.pillcase == nil then d.pillcase = {} end
			if pi > 0 then
				p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_PILLCASE, "UseItem", "PlayerPickupSparkle")
				d.pillcase[#d.pillcase + 1] = pi
				p:SetPill(0, 0)
				sfx:Play(SoundEffect.SOUND_CR_PILLBOXCLOSE, 1, 0, false, 1, 0)
			end
		end
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, function(p)
	local d = p:GetData()
	if CommRedux.sd.pillcase and CommRedux.sd.pillcase[tostring(playerseed)] then
		d.pillcase = CommRedux.sd.pillcase[tostring(playerseed)]
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	if d.pillcase then
		CommRedux.sd.pillcase[tostring(playerseed)] = d.pillcase
	end
end)