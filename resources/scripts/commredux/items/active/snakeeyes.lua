local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local itempool = game:GetItemPool()
	local itemconfig = Isaac.GetItemConfig()
	local didpoof = false
	for i, pi in ipairs(Isaac.FindByType(5, 100, -1, false, false)) do
		local pi = pi:ToPickup()
		if pi:GetSprite():GetAnimation() ~= "Empty" then
			local newid = itempool:GetCollectible(ItemPoolType.POOL_DEVIL, true, modrng:GetSeed(), CollectibleType.COLLECTIBLE_NULL)
			local cfg = itemconfig:GetCollectible(newid)
			pi:Morph(5, 100, newid, true, true, false)
			pi.AutoUpdatePrice = false
			pi.Price = -cfg.DevilPrice
			pi.ShopItemId = -1
			pi:GetData().isDevil = true
			pi:GetData().targetPlayer = p
			Isaac.Spawn(1000, 15, 0, pi.Position, Vector.Zero, pi)
			didpoof = true
		end
	end
	
	if didpoof == true then
		game:ShakeScreen(10)
		--sfx:Play(SoundEffect.SOUND_DEATH_CARD, 1, 0, false, 3, 0)
	end
	return true
end, CollectibleType.COLLECTIBLE_CR_SNAKEEYES)

CommRedux:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pi)
	if pi.Variant == 100 then
		local d = pi:GetData()
		if d.isDevil and d.targetPlayer then
			local p = d.targetPlayer:ToPlayer()
			local itemconfig = Isaac.GetItemConfig()
			local cfg = itemconfig:GetCollectible(pi.SubType)
			if pi.Price == PickupPrice.PRICE_TWO_HEARTS and p:GetEffectiveMaxHearts() < 4 then
				pi.Price = PickupPrice.PRICE_ONE_HEART_AND_TWO_SOULHEARTS
			end
			if (pi.Price == PickupPrice.PRICE_TWO_HEARTS or pi.Price == PickupPrice.PRICE_ONE_HEART_AND_TWO_SOULHEARTS or pi.Price == PickupPrice.PRICE_ONE_HEART) and p:GetEffectiveMaxHearts() < 2 then
				pi.Price = PickupPrice.PRICE_THREE_SOULHEARTS
			end
		end
	end
end)