local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p, flags, slot)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	if not d.CommRedux.isholdingseed then
		p:AnimateCollectible(id, "LiftItem", "PlayerPickup")
		d.CommRedux.isholdingseed = true
		d.CommRedux.seedslot = slot
	else
		p:AnimateCollectible(id, "HideItem", "PlayerPickup")
		d.CommRedux.isholdingseed = false
		d.CommRedux.seedslot = nil
	end
	return {Discharge = false}
end, CollectibleType.COLLECTIBLE_CR_FORBIDDENSEED)

CommRedux:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, p)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	
	if d.CommRedux.isholdingseed and d.CommRedux.seedslot then
		local isseed = (p:GetActiveItem(d.CommRedux.seedslot) == CollectibleType.COLLECTIBLE_CR_FORBIDDENSEED)
		if not isseed then
			p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_FORBIDDENSEED, "HideItem", "PlayerPickup")
			d.CommRedux.isholdingseed = false
			d.CommRedux.seedslot = nil
		end
	end
	
	if d.CommRedux.isholdingseed == true and p:GetFireDirection() ~= -1 then
		if p:GetShootingInput():Length() > 0.1 then
			local tspd = 15
			local t = Isaac.Spawn(EntityType.ENTITY_TEAR, 0, 0, p.Position, (p:GetShootingJoystick():Normalized() * tspd) + p.Velocity, p):ToTear()
			t.Scale = 1.1
			t.CollisionDamage = 7
			t.FallingAcceleration = 1.1
			t.FallingSpeed = -4
			t.Height = -50
			t.Velocity = CommRedux.calcTearVel(t.Velocity, tspd)
			p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_FORBIDDENSEED, "HideItem", "PlayerPickup")
			d.CommRedux.isholdingseed = false
			t:GetData().isseedtear = true
			t:ChangeVariant(TearVariant.STONE)
			sfx:Play(SoundEffect.SOUND_SCAMPER, 1, 0, false, 1, 0)
			local s = t:GetSprite()
			local anim = s:GetAnimation()
			s:Load("gfx/tear_appleseed.anm2", true)
			s:Play(anim)
			p:DischargeActiveItem(d.CommRedux.seedslot)
			d.CommRedux.seedslot = nil
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_TEAR_UPDATE, function(_, t)
	local d = t:GetData()
	if d.pastheight == nil then d.pastheight = t.Height end
	local heightdiff = 0
	if d.pastheight ~= t.Height then
		heightdiff = (t.Height - d.pastheight)
		d.pastheight = t.Height
	end
	if d.isseedtear == true then
		t.Velocity = t.Velocity / 1.025
		if t:IsDead() then
			for i = 1, CommRedux.rand(6, 8, modrng) do
				local fx = Isaac.Spawn(1000, 35, 0, t.Position, (t.Velocity / 10) + (Vector(CommRedux.rand_float(-1, 1, modrng), CommRedux.rand_float(-1, 1, modrng))) * 2, t):ToEffect()
				fx.m_Height = t.Height
				if CommRedux.rand(1, 2, modrng) == 1 then
					fx:GetSprite().Color = Color(0.5, 0.25, 0.25, 1, 0, 0, 0)
				else
					fx:GetSprite().Color = Color(0.75, 0.65, 0.75, 1, 0, 0, 0)
				end
			end
			local fx = Isaac.Spawn(1000, 97, 0, t.Position, Vector.Zero, t):ToEffect()
			fx.Scale = t.Scale/2
			fx.SpriteOffset = Vector(0, t.Height) * (2/3)
			sfx:Play(SoundEffect.SOUND_MUSHROOM_POOF_2, 0.75, 0, false, 2, 0)
			Isaac.Spawn(5, PickupVariant.PICKUP_FORBIDDENCROP, 3333, t.Position, Vector.Zero, t)
		end
		t:GetSprite().Rotation = Vector(t.Velocity.X, t.Velocity.Y + heightdiff):GetAngleDegrees()
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_PRE_TEAR_COLLISION, function(_, t, collider, low)
	local d = t:GetData()
	if d.isseedtear == true then
		for i = 1, CommRedux.rand(6, 8, modrng) do
			local fx = Isaac.Spawn(1000, 35, 0, t.Position, (t.Velocity / 10) + (Vector(CommRedux.rand_float(-1, 1, modrng), CommRedux.rand_float(-1, 1, modrng))) * 2, t):ToEffect()
			fx.m_Height = t.Height
			if CommRedux.rand(1, 2, modrng) == 1 then
				fx:GetSprite().Color = Color(0.5, 0.25, 0.25, 1, 0, 0, 0)
			else
				fx:GetSprite().Color = Color(0.75, 0.65, 0.75, 1, 0, 0, 0)
			end
		end
		local fx = Isaac.Spawn(1000, 97, 0, t.Position, Vector.Zero, t):ToEffect()
		fx.Scale = t.Scale/2
		fx.SpriteOffset = Vector(0, t.Height) * (2/3)
		sfx:Play(SoundEffect.SOUND_MUSHROOM_POOF_2, 0.75, 0, false, 2, 0)
		Isaac.Spawn(5, PickupVariant.PICKUP_FORBIDDENCROP, 3333, t.Position, Vector.Zero, t)
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function(_)
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		if d.CommRedux == nil then d.CommRedux = {} end
		d.CommRedux.isholdingseed = nil
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_PICKUP_UPDATE, function(_, pi)
	local d = pi:GetData()
	if pi.Variant == PickupVariant.PICKUP_FORBIDDENCROP then
		pi:MultiplyFriction(0)
		pi:AddEntityFlags(EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK | EntityFlag.FLAG_NO_KNOCKBACK)
		pi.DepthOffset = -1000000
		for i, e in ipairs(Isaac.GetRoomEntities()) do
			if e:IsEnemy() and not e:IsFlying() then
				if (e.Position - pi.Position):Length() <= 48 then
					e:AddSlowing(EntityRef(pi), 1, 1, Color(0.75, 0.75, 0.75, 1, 0.25, 0.25, 0.25))
					e.Velocity = e.Velocity / 2
					if e:IsDead() and not e:IsBoss() then
						local chance = CommRedux.rand(1, 4, modrng)
						if chance == 1 then
							Isaac.Spawn(5, PickupVariant.PICKUP_REDAPPLE, 3333, e.Position, Vector.Zero, pi)
						elseif chance == 2 then
							Isaac.Spawn(5, PickupVariant.PICKUP_WHITEAPPLE, 3333, e.Position, Vector.Zero, pi)
						end
					end
				end
			end
		end
		if d.cooldown == nil then d.cooldown = 2 end
		if d.cooldown <= 0 then
			d.cooldown = 15
				for i, e in ipairs(Isaac.GetRoomEntities()) do
				if e:IsEnemy() and not e:IsFlying() then
					if (e.Position - pi.Position):Length() <= 48 then
						e:TakeDamage(0.5, 0, EntityRef(pi), 2)
					end
				end
			end
		end
		if d.cooldown > 0 then d.cooldown = d.cooldown - 1 end
	end
	
	if pi.Variant == PickupVariant.PICKUP_REDAPPLE or PickupVariant.PICKUP_WHITEAPPLE then
		local s = pi:GetSprite()
		if s:GetAnimation() == "Collect" and s:IsFinished() then
			pi:Remove()
		elseif s:GetAnimation() == "Collect" and not s:IsFinished() then
			pi:MultiplyFriction(0)
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pi, collider, low)
	if pi.Variant == PickupVariant.PICKUP_FORBIDDENCROP then
		return true
	end
	
	if collider.Type == 1 and (pi.Variant == PickupVariant.PICKUP_REDAPPLE or pi.Variant == PickupVariant.PICKUP_WHITEAPPLE) then
		local p = collider:ToPlayer()
		local d = p:GetData()
		if d.CommRedux == nil then d.CommRedux = {} end
		local s = pi:GetSprite()
		
		if s:GetAnimation() ~= "Collect" then
			s:Play("Collect")
			sfx:Play(SoundEffect.SOUND_CR_MINIAPPLEEAT, 1, 0, false, 1, 0)
			if pi.Variant == PickupVariant.PICKUP_REDAPPLE then
				d.CommRedux.redappleboost = d.CommRedux.redappleboost + 0.5
				p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
				p:EvaluateItems()
			elseif pi.Variant == PickupVariant.PICKUP_WHITEAPPLE then
				d.CommRedux.whiteappleboost = d.CommRedux.whiteappleboost + 0.5
				p:AddCacheFlags(CacheFlag.CACHE_FIREDELAY)
				p:EvaluateItems()
			end
			return true
		else
			return true
		end
	end
end)