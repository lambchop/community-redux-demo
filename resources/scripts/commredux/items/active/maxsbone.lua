local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_HOLD_ITEM, function(id, p, slot, frames)
	if id == CollectibleType.COLLECTIBLE_CR_MAXSBONE then
		if frames == 1 then
			p:AnimateCollectible(id, "LiftItem", "PlayerPickup")
		end
		local d = p:GetData()
		if d.CommRedux.redappleboost and d.CommRedux.tempstatcountdown == 14 then
			d.CommRedux.redappleboost = d.CommRedux.redappleboost + (0.2)
			d.CommRedux.tempstatcountdown = 14 + (15)
			p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
			p:EvaluateItems()
		end
		p:MultiplyFriction(0.84)
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_RELEASE, function(id, p, slot, frames)
	if id == CollectibleType.COLLECTIBLE_CR_MAXSBONE then
		local room = game:GetRoom()
		p:AnimateCollectible(id, "HideItem", "PlayerPickup")
	end
end, CommRedux)
