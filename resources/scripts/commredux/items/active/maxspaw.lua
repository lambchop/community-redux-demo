local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	
	local canuse
	if p:GetMaxHearts() > 2 then
		canuse = true
	else
		if (p:GetSoulHearts() > 0 or p:GetBoneHearts() > 0) and p:GetMaxHearts() > 0 then
			canuse = true
		else
			canuse = false
		end
	end
	
	if canuse then
		p:AddMaxHearts(-2)
		d.CommRedux.maxpawboost = d.CommRedux.maxpawboost + 0.25
		p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
		p:EvaluateItems()
		sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
		return true
	end
end, CollectibleType.COLLECTIBLE_CR_MAXSPAW)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if CommRedux.sd.maxpawboost and CommRedux.sd.maxpawboost[tostring(playerseed)] then
		d.CommRedux.maxpawboost = CommRedux.sd.maxpawboost[tostring(playerseed)]
		p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
		p:EvaluateItems()
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if d.CommRedux.maxpawboost then
		CommRedux.sd.maxpawboost[tostring(playerseed)] = d.CommRedux.maxpawboost
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	local d = p:GetData()
	
	if d.CommRedux == nil then d.CommRedux = {} end
	
	if d.CommRedux.maxpawboost == nil then d.CommRedux.maxpawboost = 0 end
	
	if flag == CacheFlag.CACHE_DAMAGE then
		p.Damage = p.Damage * (1 + d.CommRedux.maxpawboost)
	end
end)