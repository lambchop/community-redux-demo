local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_TAP, function(id, p, slot)
	if id == CollectibleType.COLLECTIBLE_CR_DECKBUILDER then
		local d = p:GetData()
		if d.deckbuilder == nil then d.deckbuilder = {} end
		local pos = 1
		local k = d.deckbuilder[pos]
		
		local currk = p:GetCard(0)
		if currk > 0 and #d.deckbuilder > 0 then
			d.deckbuilder[#d.deckbuilder + 1] = currk
			p:SetCard(0, 0)
			sfx:Play(SoundEffect.SOUND_PAPER_OUT, 1, 0, false, 1.5, 0)
		end
		
		if k ~= nil then
			p:AddCard(k)
			p:AnimateCard(k, "UseItem")
			sfx:Play(SoundEffect.SOUND_PAPER_OUT, 1, 0, false, 1.5, 0)
			table.remove(d.deckbuilder, pos)
		end
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_HOLD_ITEM, function(id, p, slot, frames)
	if id == CollectibleType.COLLECTIBLE_CR_DECKBUILDER then
		if frames == 6 then
			local k = p:GetCard(0)
			local d = p:GetData()
			if d.deckbuilder == nil then d.deckbuilder = {} end
			if k > 0 then
				p:AnimateCollectible(CollectibleType.COLLECTIBLE_CR_DECKBUILDER_OPEN, "UseItem", "PlayerPickupSparkle")
				d.deckbuilder[#d.deckbuilder + 1] = k
				p:SetCard(0, 0)
				sfx:Play(SoundEffect.SOUND_PAPER_IN, 1, 0, false, 1.5, 0)
			end
		end
	end
end, CommRedux)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if CommRedux.sd.deckbuilder and CommRedux.sd.deckbuilder[tostring(playerseed)] then
		d.deckbuilder = CommRedux.sd.deckbuilder[tostring(playerseed)]
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if d.deckbuilder then
		CommRedux.sd.deckbuilder[tostring(playerseed)] = d.deckbuilder
	end
end)