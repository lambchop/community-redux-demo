local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, id, rng, p, f, slot)
	local coins = p:GetNumCoins()
	local toremove = 7
	if coins >= toremove then
		p:AddMaxHearts(2)
		p:AddHearts(2)
		p:AddCoins(-toremove)
		sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
		local fx = Isaac.Spawn(1000, EffectVariant.HEART, 0, p.Position, p.Velocity*0, p):ToEffect()
		fx.RenderZOffset = 64
		fx.SpriteOffset = Vector (0, -24)
		return {ShowAnim = true}
	end
end, CollectibleType.COLLECTIBLE_CR_POPCOIN)