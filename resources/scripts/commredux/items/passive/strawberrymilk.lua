local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_STRAWBERRYMILK) then
		local cnum = p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_STRAWBERRYMILK, true)
		if flag == CacheFlag.CACHE_FIREDELAY then
			p.MaxFireDelay = p.MaxFireDelay * (2.25 * cnum)
		end
		
		if flag == CacheFlag.CACHE_DAMAGE then
			p.Damage = p.Damage * (0.8 / cnum)
		end
		
		if flag == CacheFlag.CACHE_TEARCOLOR then
			p.TearColor = Color(0.8, 0.9, 0.9, 1, 0.5, 0, 0.25)
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_PLAYER_FIRE_TEAR, function(p, t)
	local d = p:GetData()

	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_STRAWBERRYMILK) then
		local cnum = p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_STRAWBERRYMILK, true)
		for i = 1, CommRedux.rand(3, 4, t:GetDropRNG()) + (cnum*8) - 8 do
			local nt = CommRedux.dupetear(t)
			nt.Velocity = nt.Velocity:Rotated(CommRedux.rand(-1000, 1000, t:GetDropRNG())/50)
			nt.Velocity = nt.Velocity * CommRedux.rand(750, 1250, t:GetDropRNG()) /1000
			
			local tearmod = CommRedux.rand(9, 11, t:GetDropRNG())/10
			nt.CollisionDamage = nt.CollisionDamage * (tearmod^2)
			nt.Scale = nt.Scale * (tearmod)
			nt.FallingAcceleration = nt.FallingAcceleration * (tearmod^2)
			nt.FallingSpeed = nt.FallingSpeed * (tearmod^2)
			nt.Height = nt.Height * (tearmod^2)
			nt.Parent = p
			local td = t:GetData()
			local ntd = nt:GetData()
			if td.isEcto then ntd.isEcto = true end
			if td.isWire then ntd.isWire = true end
		end
	end
end)