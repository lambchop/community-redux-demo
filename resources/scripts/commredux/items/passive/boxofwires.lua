local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_PLAYER_FIRE_TEAR, function(p, t)
	local d = p:GetData()

	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_BOXOFWIRES) then
		local luck
		local col = Color(t.Color.R + 0.5, t.Color.G, t.Color.B - 0.5, t.Color.A, t.Color.RO + 0.5, t.Color.GO + 0.25, t.Color.BO)
		if p.Luck > 10 then luck = 10 else luck = p.Luck end
		if luck > 0 then
			if CommRedux.rand_float(0, 10, t:GetDropRNG()) > math.abs(luck - 10) then
				t.Color = col
				t:GetData().isWire = true
			end
		else
			if CommRedux.rand_float(0, 10, t:GetDropRNG()) <= 1 then
				t.Color = col
				t:GetData().isWire = true
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_TEAR_UPDATE, function(_, t)
	local p = CommRedux.getPlayerFromTear(t)
	local td = t:GetData()
	
	if p then
		local d = p:GetData()
		if td.isWire then
			for _, v in ipairs(Isaac.GetRoomEntities()) do
				if v:IsActiveEnemy() and v:IsVulnerableEnemy() and (v.Position - t.Position):Length() < math.ceil(128 * t.Scale) then
					local e = v:ToNPC()
					if CommRedux.rand(1, 10, t:GetDropRNG()) == 1 then
						local pos = -(t.Position - e.Position):Normalized()
						local l = p:FireTechLaser(t.Position, 6, pos, false, true, t, 0.5):ToLaser()
						l.Color = Color(0, 0, 0, t.Color.A, (t.Color.R + t.Color.RO)/2 + 0.25, (t.Color.G + t.Color.GO)/2 + 0.25, (t.Color.B + t.Color.BO)/2 + 0.25)
						l:SetMaxDistance((e.Position - t.Position):Length())
						l.ParentOffset = Vector(t.Position.X, t.Position.Y + t.Height)
						for _, v2 in ipairs(Isaac.GetRoomEntities()) do
							if v2:IsActiveEnemy() and v2:IsVulnerableEnemy() and (v2.Position - e.Position):Length() < 128 and v2.Index ~= e.Index then
								local e2 = v2:ToNPC()
								if CommRedux.rand(1, 10, t:GetDropRNG()) == 1 then
									local pos = -(e.Position - e2.Position):Normalized()
									local l = p:FireTechLaser(e.Position, 6, pos, false, true, t, 0.25):ToLaser()
									l.Color = Color(0, 0, 0, t.Color.A, (t.Color.R + t.Color.RO)/2 + 0.25, (t.Color.G + t.Color.GO)/2 + 0.25, (t.Color.B + t.Color.BO)/2 + 0.25)
									l:SetMaxDistance((e2.Position - e.Position):Length())
								end
							end
						end
					end
				end
			end
		end
	end
end)