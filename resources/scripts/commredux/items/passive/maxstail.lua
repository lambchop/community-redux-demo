local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_MAXSTAIL) then
		for i = 1, p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_MAXSTAIL) do
			if flag == CacheFlag.CACHE_DAMAGE then
				p.Damage = p.Damage + 1
			end
			if flag == CacheFlag.CACHE_SPEED then
				if p.MoveSpeed >= 1 then
					p.Damage = p.Damage * ((9 + p.MoveSpeed) / 10)
				end
			end
		end
	end
end)