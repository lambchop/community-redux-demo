local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_MALIGNMUSH) then
		for i = 1, p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_MALIGNMUSH) do
			if flag == CacheFlag.CACHE_SHOTSPEED then
				p.ShotSpeed = p.ShotSpeed + 0.16
			end
			if flag == CacheFlag.CACHE_LUCK then
				p.Luck = p.Luck + 1
			end
			if flag == CacheFlag.CACHE_FIREDELAY then
				tears_up.div = tears_up.div * (4/5)
			end
			if flag == CacheFlag.CACHE_SIZE then
				p.SpriteScale = p.SpriteScale * 0.8
			end
		end
	end
end)