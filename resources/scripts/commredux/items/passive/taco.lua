local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_TACO) then
		for i = 1, p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_TACO) do
			if flag == CacheFlag.CACHE_SPEED then
				p.MoveSpeed = p.MoveSpeed + 0.16
			end
			if flag == CacheFlag.CACHE_RANGE then
				p.TearRange = p.TearRange + 1.5*40
			end
			if flag == CacheFlag.CACHE_DAMAGE then
				p.Damage = p.Damage * 1.25
			end
			if flag == CacheFlag.CACHE_SHOTSPEED then
				p.ShotSpeed = p.ShotSpeed + 0.16
			end
			if flag == CacheFlag.CACHE_LUCK then
				p.Luck = p.Luck + 1
			end
			if flag == CacheFlag.CACHE_FIREDELAY then
				tears_up.div = tears_up.div * (4/5)
			end
		end
	end
end)