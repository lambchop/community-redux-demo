local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end 
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_MYSTERYMEAT) then
		if CommRedux.sd.mysterymeat[ps] == nil then CommRedux.sd.mysterymeat[ps] = CommRedux.rand(1, 5, p:GetDropRNG()) end
		for i = 1, p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_MYSTERYMEAT) do
			if flag == CacheFlag.CACHE_SPEED then
				if CommRedux.sd.mysterymeat[ps] == 1 then
					p.MoveSpeed = p.MoveSpeed + 0.32
				end
			end
			if flag == CacheFlag.CACHE_RANGE then
				if CommRedux.sd.mysterymeat[ps] == 2 then
					p.TearRange = p.TearRange + 2*40
				end
			end
			if flag == CacheFlag.CACHE_DAMAGE then
				if CommRedux.sd.mysterymeat[ps] == 3 then
					p.Damage = p.Damage * 1.5
				end
			end
			if flag == CacheFlag.CACHE_LUCK then
				if CommRedux.sd.mysterymeat[ps] == 4 then
					p.Luck = p.Luck + 2
				end
			end
			if flag == CacheFlag.CACHE_FIREDELAY then
				if CommRedux.sd.mysterymeat[ps] == 5 then
					tears_up.div = tears_up.div * 1.25
				end
			end
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_LOAD, function(p)
	local d = p:GetData()
	local playerseed = p:GetCollectibleRNG(0):GetSeed()
	local ps = tostring(playerseed)
	if CommRedux.sd.mysterymeat and CommRedux.sd.mysterymeat[tostring(playerseed)] then
		p:AddCacheFlags(CacheFlag.CACHE_ALL)
		p:EvaluateItems()
	end
end)