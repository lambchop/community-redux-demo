local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_PLAYER_FIRE_TEAR, function(p, t)
	local d = p:GetData()
	
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_ECTOPLASM) then
		local luck
		local col = Color(t.Color.R - 0.9, t.Color.G, t.Color.R - 0.75, t.Color.A, t.Color.RO, t.Color.GO + 0.25, t.Color.BO + 0.5)
		if p.Luck > 10 then luck = 10 else luck = p.Luck end
		if luck > 0 then
			if CommRedux.rand_float(0, 10, t:GetDropRNG()) > math.abs(luck - 10) then
				t.Color = col
				t:GetData().isEcto = true
			end
		else
			if CommRedux.rand_float(0, 10, t:GetDropRNG()) <= 1 then
				t.Color = col
				t:GetData().isEcto = true
			end
		end
	end
end)


CommRedux:AddCallback(ModCallbacks.MC_PRE_TEAR_COLLISION, function(_, t, col, low)
	local p = CommRedux.getPlayerFromTear(t)
	
	if t:GetData().isEcto then
		if col:IsVulnerableEnemy() then
			if col:GetData().ectoCharges == nil then col:GetData().ectoCharges = 0 end
			if col:GetData().ectoCharges < 6 then
				col:GetData().ectoCharges = col:GetData().ectoCharges + 1
			end
			col:GetData().ectoTime = 0
			col:GetData().ectoParent = p
		end
	end
end)


CommRedux:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, e)
	local d = e:GetData()
	if d.ectoCharges then
		d.ectoTime = d.ectoTime + 1
		if d.ectoTime >= 30 + math.ceil(d.ectoParent.MaxFireDelay) and e:IsVulnerableEnemy() then
			sfx:Play(SoundEffect.SOUND_CR_HAUNTDMG, 1, 0, false, 1, 0)
			local p = d.ectoParent:ToPlayer()
			e:TakeDamage((p.Damage * 2) * d.ectoCharges, 0, EntityRef(p), 0)
			d.ectoCharges = nil
			d.ectoTime = nil
		end
		if d.ectoCharges ~= nil then
			e:SetColor(Color(1, 1, 1, 1, 0, 0.1 + (0.05*d.ectoCharges), 0.075 + (0.075*d.ectoCharges)), 2, 999, false, true)
		end
	end
end)