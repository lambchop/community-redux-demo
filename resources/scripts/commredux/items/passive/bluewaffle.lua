local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end 
	
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_BLUEWAFFLE) then
		for i = 1, p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_BLUEWAFFLE) do
			if flag == CacheFlag.CACHE_FIREDELAY then
				tears_up.div = tears_up.div * 0.5
			end
		end
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_FIRE_TEAR, function(p, t)
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_BLUEWAFFLE) then
		t.Velocity = t.Velocity:Rotated(CommRedux.rand(-23, 23, t:GetDropRNG()))
	end
end)