local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PRE_EVALUATE_CACHE, function(p, flag, tears_up)
	if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_MAXSBALL) then
		for i = 1, p:GetCollectibleNum(CollectibleType.COLLECTIBLE_CR_MAXSBALL) do
			if flag == CacheFlag.CACHE_DAMAGE then
				p.Damage = p.Damage * 1.2
			end
			if flag == CacheFlag.CACHE_FIREDELAY then
				tears_up.div = tears_up.div * (4/5)
			end
		end
	end
end)