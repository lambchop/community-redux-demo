local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.COLD, 1.5, 0, false, 1, 0)
	end
	for _, e in pairs(Isaac.GetRoomEntities()) do
		if e:IsVulnerableEnemy() and e:IsActiveEnemy() then
			if CommRedux.rand(1, 2, modrng) == 1 then
				if not e:IsBoss() then
					e:AddEntityFlags(EntityFlag.FLAG_ICE)
					e:AddEntityFlags(EntityFlag.FLAG_FREEZE)
					e:TakeDamage(e.HitPoints + 1, 0, EntityRef(p), 0)
				else
					e:TakeDamage(40, 0, EntityRef(p), 0)
					e:AddSlowing(EntityRef(p), 60, 1, Color(e.Color.R, e.Color.G, e.Color.B, e.Color.A, e.Color.RO + 0.2, e.Color.GO + 0.2, e.Color.BO + 0.2))
				end
			else
				e:TakeDamage(10, 0, EntityRef(p), 0)
				e:AddSlowing(EntityRef(p), 60, 1, Color(e.Color.R, e.Color.G, e.Color.B, e.Color.A, e.Color.RO + 0.2, e.Color.GO + 0.2, e.Color.BO + 0.2))
			end
		end
	end
end, Card.CARD_CR_COLD)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.SERVANT, 1.5, 0, false, 1, 0)
	end
	local amount = CommRedux.rand(4, 6, modrng)
	for i = 1, amount do
		p:AddMinisaac(p.Position)
	end
end, Card.CARD_CR_SERVANT)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.WISDOM, 1.5, 0, false, 1, 0)
	end
	game:StartRoomTransition(game:GetLevel():QueryRoomTypeIndex(RoomType.ROOM_SUPERSECRET, false, modrng), Direction.NO_DIRECTION ,3)
end, Card.CARD_CR_WISDOM)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.REPENTANCE, 1.5, 0, false, 1, 0)
	end
	local tempEffects = p:GetEffects()
	tempEffects:AddCollectibleEffect(CollectibleType.COLLECTIBLE_MOMS_KNIFE)
end, Card.CARD_CR_REPENTANCE)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.ETERNITY, 1.5, 0, false, 1, 0)
	end
	p:AddEternalHearts(1)
	p:AddMaxHearts(2)
	p:AddHearts(4)
end, Card.CARD_CR_ETERNITY)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.CORRUPTION, 1.5, 0, false, 1, 0)
	end
	local stage = game:GetLevel():GetStage()
	local stageType = game:GetLevel():GetStageType()
	
	if (stage == LevelStage.STAGE4_3) or (stage == LevelStage.STAGE6) or (stage == LevelStage.STAGE7) or (stage == LevelStage.STAGE8) or (stage == LevelStage.STAGE4_2) and (stageType == StageType.STAGETYPE_REPENTANCE) then
		p:AnimateSad()	
	else
		game:StartRoomTransition(game:GetLevel():QueryRoomTypeIndex(RoomType.ROOM_ERROR, false, modrng), Direction.NO_DIRECTION ,3)
	end
end, Card.CARD_CR_CORRUPTION)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.IMMOLATION, 1.5, 0, false, 1, 0)
	end
	local level = game:GetLevel()
	local rooms = level:GetRooms()
	for i = 0, rooms.Size - 1 do
		local room = rooms:Get(i)
		local roomConfig = room.Data
		if roomConfig.Type == RoomType.ROOM_CURSE then
			game:StartRoomTransition(game:GetLevel():QueryRoomTypeIndex(RoomType.ROOM_CURSE, false, modrng), Direction.NO_DIRECTION ,3)
			break
		elseif roomConfig.Type == RoomType.ROOM_SACRIFICE then
			game:StartRoomTransition(game:GetLevel():QueryRoomTypeIndex(RoomType.ROOM_SACRIFICE, false, modrng), Direction.NO_DIRECTION ,3)
			break
		end
	end
end, Card.CARD_CR_IMMOLATION)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
		sfx:Play(SoundEffect.SOUND_CR_LOSTTAROT.WORSHIP, 1.5, 0, false, 1, 0)
	end
	local room = game:GetRoom()
	if CommRedux.rand(1, 2, modrng) == 1 then
		p:TakeDamage(4, DamageFlag.DAMAGE_NOKILL, EntityRef(p), 0)
	else
		Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, game:GetItemPool():GetCollectible(game:GetItemPool():GetLastPool()), room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
		p:AnimateHappy()
	end
end, Card.CARD_CR_WORSHIP)


CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
	end
	local cursedroomidx = CommRedux.sd.cursedroomidx[CommRedux.rand(1, #CommRedux.sd.cursedroomidx, modrng)]
	if cursedroomidx == nil then
		CommRedux.sd.cursedroomidx[#CommRedux.sd.cursedroomidx + 1] = game:GetLevel():GetCurrentRoomDesc().GridIndex
		game:ShakeScreen(16)
		sfx:Play(SoundEffect.SOUND_BLACK_POOF, 1, 0, false, 0.5, 0)
		local poof = Isaac.Spawn(1000, 16, 1, p.Position, Vector.Zero, p)
		poof:GetSprite().Color = Color(0.65, 0.25, 0.65, 1, 0, 0, 0)
	else
		game:StartRoomTransition(cursedroomidx, Direction.NO_DIRECTION ,3)
	end
end, Card.CARD_CR_HEX)


CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
	end
	p:UseActiveItem(CollectibleType.COLLECTIBLE_SULFUR, UseFlag.USE_NOANIM, ActiveSlot.SLOT_PRIMARY)
	p:AddBlackHearts(2)
end, Card.CARD_CR_DISSENSION)


CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
	end
	for i = 1, 10 do
		local pool = {
			FamiliarVariant.BONE_ORBITAL
		}
		local f = Isaac.Spawn(3, pool[CommRedux.rand(1, #pool, modrng)], 0, p.Position + Vector(1, 0):Resized(CommRedux.rand(40, 80, modrng)):Rotated(CommRedux.rand(0, 360, modrng)), Vector.Zero, p):ToFamiliar()
		f.Player = p
		f:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
	end
end, Card.CARD_CR_DAMNED)


CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
	end
	local itempool = game:GetItemPool()
	local itemconfig = Isaac.GetItemConfig()
	local didpoof = false
	for i, pi in ipairs(Isaac.FindByType(5, 100, -1, false, false)) do
		local pi = pi:ToPickup()
		if pi:GetSprite():GetAnimation() ~= "Empty" then
			local newid = itempool:GetCollectible(ItemPoolType.POOL_DEVIL, true, modrng:GetSeed(), CollectibleType.COLLECTIBLE_NULL)
			local cfg = itemconfig:GetCollectible(newid)
			pi:Morph(5, 100, newid, true, true, false)
			pi.AutoUpdatePrice = false
			pi.Price = -cfg.DevilPrice
			pi.ShopItemId = -1
			pi:GetData().isDevil = true
			pi:GetData().targetPlayer = p
			Isaac.Spawn(1000, 15, 0, pi.Position, Vector.Zero, pi)
			didpoof = true
		end
	end
	
	if didpoof == true then
		game:ShakeScreen(10)
	end
end, Card.CARD_CR_OCCULT)


CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	if flags & UseFlag.USE_NOANNOUNCER < 1 and CommRedux.sd.losttarotannouncer == 2 then
	end
	
	local stage = game:GetLevel():GetStage()
	local stageType = game:GetLevel():GetStageType()
	
	local level = game:GetLevel()
	local rooms = level:GetRooms()
	
	local foundlibrary = false
	
	for i = 0, rooms.Size - 1 do
		local room = rooms:Get(i)
		local roomConfig = room.Data
		if roomConfig.Type == RoomType.ROOM_LIBRARY then
			game:StartRoomTransition(game:GetLevel():QueryRoomTypeIndex(RoomType.ROOM_CURSE, false, modrng), Direction.NO_DIRECTION ,3)
			foundlibrary = true
			break
		end
	end
	if not foundlibrary then
		local t = CommRedux.FindCollectiblesByTag(ItemConfig.TAG_BOOK, ItemConfig.TAG_BOOK)
		local itemconfig = Isaac.GetItemConfig()
		local touse = t[CommRedux.rand(1, #t, modrng)]
		while touse == CollectibleType.COLLECTIBLE_BIBLE or touse == CollectibleType.COLLECTIBLE_BOOK_OF_VIRTUES do
			touse = t[CommRedux.rand(1, #t, modrng)]
		end
		p:UseActiveItem(touse, UseFlag.USE_NOANIM, ActiveSlot.SLOT_PRIMARY)
		game:GetHUD():ShowItemText(p, itemconfig:GetCollectible(touse))
	end
end, Card.CARD_CR_SCHOLAR)