local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	p:UseActiveItem(CollectibleType.COLLECTIBLE_METRONOME, UseFlag.USE_NOANIM)
end, Card.CARD_CR_TREASURE)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	local room = game:GetRoom()
	for i=1, 3 do
		Isaac.Spawn(EntityType.ENTITY_PICKUP, 0, -1, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
	end
end, Card.CARD_CR_LOOT)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	local room = game:GetRoom()
	local ran = CommRedux.rand(1,6,modrng)
	local enemy = nil
	if ran == 1 then
		enemy = Isaac.Spawn(EntityType.ENTITY_ATTACKFLY, 0, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, nil)
	elseif ran == 2 then
		enemy = Isaac.Spawn(EntityType.ENTITY_FAT_BAT, 0, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, nil)
	elseif ran == 3 then
		enemy = Isaac.Spawn(EntityType.ENTITY_GAPER, 0, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, nil)
	elseif ran == 4 then
		enemy = Isaac.Spawn(EntityType.ENTITY_ONE_TOOTH, 0, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, nil)
	elseif ran == 5 then
		enemy = Isaac.Spawn(EntityType.ENTITY_POOTER, 0, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, nil)
	elseif ran == 6 then
		enemy = Isaac.Spawn(EntityType.ENTITY_VIS, 0, 0, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, nil)
	end
	enemy:AddCharmed(EntityRef(p), -1)
end, Card.CARD_CR_MONSTER)

CommRedux:AddCallback(ModCallbacks.MC_USE_CARD, function(_, id, p, flags)
	local room = game:GetRoom()
	Isaac.Spawn(EntityType.ENTITY_FAMILIAR, FamiliarVariant.LOST_SOUL, -1, room:FindFreePickupSpawnPosition(p.Position, 0, true), Vector.Zero, p)
end, Card.CARD_CR_SOUL)
