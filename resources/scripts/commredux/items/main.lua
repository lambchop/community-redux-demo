local game = CommRedux.GAME

local selfpath = "resources/scripts/commredux/items/"

local scriptlist = {
	"active/bookofsorrows",
	"active/bowlofbeans",
	"active/coolbean",
	"active/cursedd12",
	"active/d3",
	"active/deckbuilder",
	"active/forbiddenseed",
	"active/geode",
	"active/hotbean",
	"active/lunchbox",
	"active/maxsbone",
	"active/maxspaw",
	"active/occamsrazor",
	"active/phlebectomy",
	"active/pillcase",
	"active/popcoin",
	"active/potty",
	"active/shampoo",
	"active/snakeeyes",
	"active/tammyspaw",
	"active/theapple",
	
	"familiar/conquerorbaby",
	"familiar/cousinjoey",
	
	--passive
	"passive/bluewaffle",
	"passive/boxofwires",
	"passive/ectoplasm",
	"passive/gummybear",
	"passive/malignmush",
	"passive/maxsball",
	"passive/maxshead",
	"passive/maxstail",
	"passive/mysterymeat",
	"passive/strawberrymilk",
	"passive/taco",
	
	--pickup
	
	"pocket/cantripbagcards",
	"pocket/losttarot",
	
	--transformations
	
	"trinket/painpills",
	"trinket/suppository",
}

CommRedux:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, p)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	local cidx = p.ControllerIndex
	local held
	if d.CommRedux.holditemframes == nil then d.CommRedux.holditemframes = 0 end
	if d.CommRedux.activeSlot then
		local id = p:GetActiveItem(d.CommRedux.activeSlot)
		if d.CommRedux.activeSlot == ActiveSlot.SLOT_PRIMARY then
			held = Input.IsActionPressed(ButtonAction.ACTION_ITEM, cidx)
		elseif d.CommRedux.activeSlot == ActiveSlot.SLOT_POCKET then
			held = Input.IsActionPressed(ButtonAction.ACTION_PILLCARD, cidx)
		end
		if held then
			d.CommRedux.holditemframes = d.CommRedux.holditemframes + 1
			CBLib.RunCallback(CommRedux.ModCallbacks.MC_HOLD_ITEM, CommRedux, id, p, d.CommRedux.activeSlot, d.CommRedux.holditemframes)
		end
		if not held and d.CommRedux.holditemframes > 0 then
			if d.CommRedux.holditemframes < 6 then
				CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_TAP, CommRedux, id, p, d.CommRedux.activeSlot)
				d.CommRedux.holditemframes = 0
			else
				CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_HOLD_ITEM_RELEASE, CommRedux, id, p, d.CommRedux.activeSlot, d.CommRedux.holditemframes)
				d.CommRedux.holditemframes = 0
			end
		end
	end
end)

local function resethelditem()
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		if d.CommRedux == nil then d.CommRedux = {} end
		if d.CommRedux.holditemframes then d.CommRedux.holditemframes = 0 end
	end
end
CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, resethelditem)
CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, resethelditem)

for i, v in ipairs(scriptlist) do
	include(selfpath .. v)
end

local function useActive(_, id, rng, p, flags, slot)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	d.CommRedux.activeSlot = slot
end
CommRedux:AddCallback(ModCallbacks.MC_USE_ITEM, useActive)