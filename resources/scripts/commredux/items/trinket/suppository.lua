local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_PILL, function(_, id, p, flags)
	if p:GetTrinketMultiplier(TrinketType.TRINKET_CR_SUPPOSITORY) > 0 then
		for i = 1, CommRedux.rand(2, 4, modrng) * p:GetTrinketMultiplier(TrinketType.TRINKET_CR_SUPPOSITORY) do
			local dipvel = Vector(CommRedux.rand_float(50, 70, modrng), 0):Rotated(CommRedux.rand(0, 360, modrng))
			p:ThrowFriendlyDip(0, p.Position, p.Position + dipvel)
		end
		sfx:Play(SoundEffect.SOUND_MEATHEADSHOOT, 1, 0, false, 1, 0)
	end
end)
