local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CommRedux:AddCallback(ModCallbacks.MC_USE_PILL, function(_, id, p, flags)
	if p:GetTrinketMultiplier(TrinketType.TRINKET_CR_PAINPILLS) > 0 then
		for i = 1, p:GetTrinketMultiplier(TrinketType.TRINKET_CR_PAINPILLS) do
			if p:HasFullHearts() then
				p:AddSoulHearts(1)
			else
				p:AddHearts(2)
			end
		end
		sfx:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1, 0)
	end
end)
