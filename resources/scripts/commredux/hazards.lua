local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG
local curseShaderFloat = 0

CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function(self)
	CommRedux.sd.cursedroomidx = {}
	curseShaderFloat = 0
	local level = game:GetLevel()
	local normalroomidx = CommRedux.getroomidxbytype(1)
	local cursemodifier = 1
	
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		cursemodifier = cursemodifier + p:GetTrinketMultiplier(TrinketType.TRINKET_CR_SHRUNKENHEAD) * 3
	end
	
	for i = 1, #normalroomidx do
	
		local roomDescriptor = normalroomidx[i]
		local roomConfigRoom = roomDescriptor.Data
		local spawnList = roomConfigRoom.Spawns
		
		local isvalid = false
		
		for i = 1, #spawnList do
			if spawnList:Get(i - 1):PickEntry(0).Type > 9 and spawnList:Get(i - 1):PickEntry(0).Type < 960 then
				isvalid = true
			end
		end
		
		if normalroomidx[i].Data.Difficulty > 9 and isvalid then
			if CommRedux.rand(1, math.ceil(24/cursemodifier), modrng) == 1 then
				CommRedux.sd.cursedroomidx[#CommRedux.sd.cursedroomidx + 1] = normalroomidx[i].GridIndex
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function(self)
	local idx = tostring(Game():GetLevel():GetCurrentRoomDesc().SafeGridIndex)
	local room = game:GetRoom()
	local level = game:GetLevel()
	local roomidx = level:GetCurrentRoomDesc().GridIndex
	
	if CommRedux.sd.roomdata and CommRedux.sd.roomdata[idx] and not room:IsFirstVisit() then
		for _, v in pairs(CommRedux.sd.roomdata[idx]) do
			local e = Isaac.Spawn(v.Type, v.Variant, v.SubType, Vector(v.X, v.Y), Vector(0,0), nil):ToNPC()
			local edata = e:GetData()
			if v.Data then
				for i, data in pairs(v.Data) do
					edata[i] = v.Data[i]
				end
			end
			CBLib.RunCallback(CommRedux.ModCallbacks.MC_POST_HAZARD_REINIT, CommRedux, e)
		end
		CommRedux.sd.roomdata[idx] = {}
	end
	
	local iscursed = false
	
	if CommRedux.sd.cursedroomidx then
		for i = 1, #CommRedux.sd.cursedroomidx do
			if CommRedux.sd.cursedroomidx[i] == roomidx then
				if level:GetCurrentRoomDesc().ClearCount >= 3 then
					CommRedux.sd.cursedroomidx[i] = nil
				else
					iscursed = true
				end
			end
		end
	end
	
	local willrespawn = true
	
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		if p:GetEffects():HasNullEffect(NullItemID.ID_LOST_CURSE) then
			willrespawn = false
		end
	end
	
	if iscursed and willrespawn then
		game:ShakeScreen(16)
		sfx:Play(SoundEffect.SOUND_BLACK_POOF, 1, 0, false, 0.5, 0)
		room:RespawnEnemies()
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_UPDATE, function(self)
	if curseShaderFloat > 0.01 then
		curseShaderFloat = curseShaderFloat / 1.25
	else
		curseShaderFloat = 0
	end
	
	local room = game:GetRoom()
	local level = game:GetLevel()
	
	local roomidx = level:GetCurrentRoomDesc().GridIndex
	
	local iscursed = false
	
	for i = 1, #CommRedux.sd.cursedroomidx do
		if CommRedux.sd.cursedroomidx[i] == roomidx then
			iscursed = true
		end
	end
	
	if iscursed then
		if level:GetCurrentRoomDesc().ClearCount < 3 then
			MusicManager():PitchSlide(0.75)
			
			if curseShaderFloat < 1 then
				curseShaderFloat = curseShaderFloat + 0.05
			end
			
			for i, e in ipairs(Isaac.GetRoomEntities()) do
				local d = e:GetData()
				if e.Type == 9 then
					if e.CollisionDamage == 1 then
						e.CollisionDamage = 2
					end
				end
				if e:IsEnemy() then
					if e.CollisionDamage == 1 then
						e.CollisionDamage = 2
					end
					d.CursedArmor = level:GetCurrentRoomDesc().ClearCount
				end
			end
		else
			for i = 1, #CommRedux.sd.cursedroomidx do
				if CommRedux.sd.cursedroomidx[i] == roomidx then
					--print("cleansed")
					CommRedux.sd.cursedroomidx[i] = nil
				end
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(self, e, amount, flags, source, countdown)
	if e:IsEnemy() then
		local d = e:GetData()
		if d.CursedArmor and (flags & DamageFlag.DAMAGE_IGNORE_ARMOR < 1) and (flags & DamageFlag.DAMAGE_FAKE < 1) then
			
			e:TakeDamage(amount / (1 + d.CursedArmor), flags | DamageFlag.DAMAGE_FAKE, source, countdown)
			return false
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_GET_SHADER_PARAMS, function(_, name)
	if name == "CurseShader" then
        return {Toggle = curseShaderFloat}
    end
end)
