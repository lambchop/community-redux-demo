local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	
	if p:GetPlayerType() == PlayerType.PLAYER_CR_ABEL then
		if d.CommRedux.AbelCountDown == nil then d.CommRedux.AbelCountDown = 15 end
		if d.CommRedux.AbelDMGBoost == nil then d.CommRedux.AbelDMGBoost = 0 end
		if d.CommRedux.AbelCountDown <= 0 then
			if d.CommRedux.AbelDMGBoost and d.CommRedux.AbelDMGBoost > 0 then
				d.CommRedux.AbelDMGBoost = d.CommRedux.AbelDMGBoost - 0.01
				if d.CommRedux.AbelDMGBoost < 0 then d.CommRedux.AbelDMGBoost = 0 end
				p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
				p:EvaluateItems()
			end
			d.CommRedux.AbelCountDown = 10
		end
		
		if d.CommRedux.AbelDMGBoost > 0 and d.CommRedux.AbelCountDown > 0 then
			d.CommRedux.AbelCountDown = d.CommRedux.AbelCountDown - 1
		elseif d.CommRedux.AbelDMGBoost <= 0 then
			d.CommRedux.AbelCountDown = 60
		end
		
		if d.CommRedux.AbelDMGBoost < 0 then d.CommRedux.AbelDMGBoost = 0 end
		if not p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) then
			if d.CommRedux.AbelDMGBoost > 1 then 
				d.CommRedux.AbelDMGBoost = 1
				p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
				p:EvaluateItems()
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, p)
	if p:GetPlayerType() == PlayerType.PLAYER_CR_ABEL then
		p:AddCollectible(CollectibleType.COLLECTIBLE_KIDNEY_BEAN, 1, true, ActiveSlot.SLOT_PRIMARY, 0)
		game:GetItemPool():RemoveCollectible(CollectibleType.COLLECTIBLE_KIDNEY_BEAN)
		p:SetCard(0, Card.CARD_CR_SERVANT)
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE, function(p, flag)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end 
	
	CommRedux.forceCharacterCostume(p, PlayerType.PLAYER_CR_ABEL, NullItemID.ID_CR_ABEL)
	
	if p:GetPlayerType() == PlayerType.PLAYER_CR_ABEL then
		if flag == CacheFlag.CACHE_RANGE then
			p.TearRange = p.TearRange - 1.5*40
		end
		if flag == CacheFlag.CACHE_SPEED then
			p.MoveSpeed = p.MoveSpeed + 0.16
		end
		if d.CommRedux.AbelDMGBoost == nil then d.CommRedux.AbelDMGBoost = 0 end
		if flag == CacheFlag.CACHE_DAMAGE then
			p.Damage = (p.Damage * 0.75) * (1 + d.CommRedux.AbelDMGBoost)
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_NPC_DEATH, function(_, e)
	for i = 0, game:GetNumPlayers() - 1 do
		local p = Isaac.GetPlayer(i)
		local d = p:GetData()
		if p:GetPlayerType() == PlayerType.PLAYER_CR_ABEL then
			if e:HasEntityFlags(EntityFlag.FLAG_CHARM) then
				d.CommRedux.AbelDMGBoost = d.CommRedux.AbelDMGBoost + 0.25
				d.CommRedux.AbelCountDown = 60
				p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
				p:EvaluateItems()
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, e, amount, flags, source, countdown)
	if e.Type == EntityType.ENTITY_PLAYER then
		local p = e:ToPlayer()
		local d = p:GetData()
		if flags & DamageFlag.DAMAGE_NO_PENALTIES < 1 then
			if p:GetPlayerType() == PlayerType.PLAYER_CR_ABEL then
				if d.CommRedux.AbelDMGBoost == nil then d.CommRedux.AbelDMGBoost = 0 end
				if d.CommRedux.AbelDMGBoost > 0 then 
					d.CommRedux.AbelDMGBoost = 0
					p:AddCacheFlags(CacheFlag.CACHE_DAMAGE)
					p:EvaluateItems()
				end
			end
		end
		
		if p:GetPlayerType() == PlayerType.PLAYER_CR_ABEL then
			p:UseActiveItem(CollectibleType.COLLECTIBLE_KIDNEY_BEAN, UseFlag.USE_OWNED + UseFlag.USE_NOANIM, ActiveSlot.SLOT_POCKET2)
			if p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) then
				game:ShakeScreen(10)
				for i, npc in ipairs(Isaac.GetRoomEntities()) do
					if (npc.Position - p.Position):Length() <= 128 then
						if npc:IsActiveEnemy() and npc:IsVulnerableEnemy() and npc:IsEnemy() then
							npc:AddCharmed(EntityRef(p), 30 * 16)
							npc:TakeDamage(math.ceil(math.abs((npc.Position - p.Position):Length() - 128) / 4), 0, EntityRef(p), 0)
						end
						local poof = Isaac.Spawn(1000, 16, 1, p.Position, Vector.Zero, p)
						poof:GetSprite().Color = Color(0.75, 0.25, 0.8, 1, 0, 0, 0)
						sfx:Play(SoundEffect.SOUND_BLACK_POOF, 1, 0, false, 1, 0)
					else
						if npc:IsActiveEnemy() and npc:IsVulnerableEnemy() and npc:IsEnemy() then
							npc:AddCharmed(EntityRef(p), 30 * 8)
						end
					end
				end
			end
		end
	end
end)