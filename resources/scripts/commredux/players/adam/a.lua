local game = CommRedux.GAME
local sfx = CommRedux.SFX
local modrng = CommRedux.RNG


CBLib.AddToCallback(CommRedux.ModCallbacks.MC_POST_EVALUATE_CACHE, function(p, flag)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end 
	
	CommRedux.forceCharacterCostume(p, PlayerType.PLAYER_CR_ADAM, NullItemID.ID_CR_ADAM)
	
	if not d.CommRedux.AppleMode then
		if p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM then
			if d.CommRedux.AdamAppleMode then
				if d.CommRedux.AdamAppleMode == "Damage" then
					if flag == CacheFlag.CACHE_DAMAGE then
						p.Damage = p.Damage * 1.2
					end
					if flag == CacheFlag.CACHE_LUCK then
						p.Luck = p.Luck - 1
					end
					if flag == CacheFlag.CACHE_SHOTSPEED then
						p.ShotSpeed = p.ShotSpeed + 0.16
					end
					if flag == CacheFlag.CACHE_RANGE then
						p.TearRange = p.TearRange - 20
					end
				end
				if d.CommRedux.AdamAppleMode == "Tears" then
					if flag == CacheFlag.CACHE_DAMAGE then
						p.Damage = p.Damage * 0.75
					end
					if flag == CacheFlag.CACHE_FIREDELAY then
						p.MaxFireDelay = CommRedux.AddTPS(p, 0.5)
					end
					if flag == CacheFlag.CACHE_LUCK then
						p.Luck = p.Luck + 1
					end
					if flag == CacheFlag.CACHE_SPEED then
						p.MoveSpeed = p.MoveSpeed + 0.16
					end
					if flag == CacheFlag.CACHE_RANGE then
						p.TearRange = p.TearRange + 20
					end
				end
			end
		end
	else
		if p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM then
			d.CommRedux.AdamAppleMode = d.CommRedux.AppleMode
			if flag == CacheFlag.CACHE_SHOTSPEED then
				p.ShotSpeed = p.ShotSpeed + 0.16
			end
		end
	end
end)

CommRedux:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, p)
	local d = p:GetData()
	
	if p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM then
		if not d.CommRedux then d.CommRedux = {} end
		if not d.CommRedux.AppleMode then
			if CommRedux.rand(1, 2, p:GetDropRNG()) == 1 then
				d.CommRedux.AppleMode = "Tears"
				p:AddNullCostume(NullItemID.ID_CR_APPLETEARS)
				p:AddCacheFlags(CacheFlag.CACHE_ALL)
				p:EvaluateItems()
				p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE, 0, true, ActiveSlot.SLOT_PRIMARY, 0)
			else
				d.CommRedux.AppleMode = "Damage"
				p:AddNullCostume(NullItemID.ID_CR_APPLEBLOOD)
				p:AddCacheFlags(CacheFlag.CACHE_ALL)
				p:EvaluateItems()
				p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED, 0, true, ActiveSlot.SLOT_PRIMARY, 0)
			end
		end
		p:AddEternalHearts(1)
		game:GetItemPool():RemoveCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE)
	end
end)

CBLib.AddToCallback(CommRedux.ModCallbacks.MC_PLAYER_UPDATE, function(p)
	local d = p:GetData()
	if d.CommRedux == nil then d.CommRedux = {} end
	if p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM and p:HasCollectible(CollectibleType.COLLECTIBLE_BIRTHRIGHT) then
		if not d.birthright then
			p:RemoveCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE)
			p:RemoveCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE)
			p:RemoveCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED)
			p:RemoveCollectible(AppleFrames.L)
			p:RemoveCollectible(AppleFrames.M)
			p:RemoveCollectible(AppleFrames.R)
			p:SetPocketActiveItem(CollectibleType.COLLECTIBLE_CR_THEAPPLE)
			d.birthright = true
		end
	end
	
	if p:GetPlayerType() == PlayerType.PLAYER_CR_ADAM then
		if p:HasCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE) then
			if d.CommRedux.AppleMode == "Tears" then
				if d.birthright then
					p:SetPocketActiveItem(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE)
				else
					p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_WHITE, 0, true, ActiveSlot.SLOT_PRIMARY, 0)
				end
			elseif d.CommRedux.AppleMode == "Damage" then
				if d.birthright then
					p:SetPocketActiveItem(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED)
				else
					p:AddCollectible(CollectibleType.COLLECTIBLE_CR_THEAPPLE_RED, 0, true, ActiveSlot.SLOT_PRIMARY, 0)
				end
			end
		end
	end
end)