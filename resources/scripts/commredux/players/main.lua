
local selfpath = "resources/scripts/commredux/players/"

local scriptlist = {
	"adam/a",
	"abel/a",
}

for i, v in ipairs(scriptlist) do
	include(selfpath .. v)
end