local thisversion = 1.0

local function loadscript()
	CBLib = RegisterMod("Callback Library", 1)
	CBLib.Version = thisversion
	CBLib.Loaded = false
	print("[".. CBLib.Name .."]", "loading script V" .. CBLib.Version .. "...")
	-- LIBRARY BY LAMBCHOP_IS_OK

	CustomModCallbacks = {}
	
	

	CBLib.ModCallbacks = {} -- used for enumeration
	-- carries on from 71 to act like an extension of ModCallbacks

	function CBLib.InitializeMod(mod)
		for i = 1, #CustomModCallbacks do
			if CustomModCallbacks[i] and CustomModCallbacks[i][1] == mod.Name then
				CustomModCallbacks[i] = {}
			end
		end
	end

	function CBLib.AddCustomCallback(mod)
		local cb = #CustomModCallbacks + 71
		table.insert(CustomModCallbacks, {mod.Name, {}})
		return cb
	end

	function CBLib.AddToCallback(callbacks, func, mod, _1, _2, _3, _4, _5, _6)
		local CMC = CustomModCallbacks
		if type(callbacks) == "table" then
			for v = 1, #callbacks do
				if callbacks[v] < 71 then	
					mod:AddCallback(callbacks[v], func, _1, _2, _3, _4, _5, _6)
				else
					CMC[callbacks - 70][2][#CMC[callbacks[v - 70]][2] + 1] = func
				end
			end
		elseif type(callbacks) == "number" then
			if callbacks < 71 then
				mod:AddCallback(callbacks, func, _1, _2, _3, _4, _5, _6)
			else
				CMC[callbacks - 70][2][#CMC[callbacks - 70][2] + 1] = func
			end
		end
	end

	function CBLib.RunCallback(callback, mod, _1, _2, _3, _4, _5, _6)
		local callback = callback - 70
		for i = 1, #CustomModCallbacks[callback][2] do
			CustomModCallbacks[callback][2][i](_1, _2, _3, _4, _5, _6)
		end
	end
	CBLib.Loaded = true
	print("[".. CBLib.Name .."]", "loaded successfully")
end

if CBLib then
	if CBLib.Version < thisversion then
		print("[".. CBLib.Name .."]", "found old script V" .. CBLib.Version .. ", found new script V" .. thisversion .. ". replacing...")
		CBLib = nil
		loadscript()
		print("[".. CBLib.Name .."]", "replaced with V" .. CBLib.Version)
	end
elseif not CBLib then
	loadscript()
end

--[[ 
	Example of cblib being used:
	
	mod = RegisterMod("my mod", 1)
	mod.ModCallbacks = {}
	
	mod.ModCallbacks.MC_CALLBACK = CBLib.AddCustomCallback(mod)	
	
	mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, p)
		if p:GetMaxHearts() == 6 then
			mod.RunCallback(mod.ModCallbacks.MC_CALLBACK, mod, p) -- will run all functions attached to it
		end
	end)
	
	CBLib.AddToCallback(mod.ModCallbacks.MC_CALLBACK, function(p)
		p:Die() -- this kills the player
	end)
]]